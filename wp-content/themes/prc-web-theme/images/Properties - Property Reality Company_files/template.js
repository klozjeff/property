/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.2
 */

(function($)
{
	$(document).ready(function()
	{
		$('*[rel=tooltip]').tooltip()

		// Turn radios into btn-group
		$('.radio.btn-group label').addClass('btn');
		$(".btn-group label:not(.active)").click(function()
		{
			var label = $(this);
			var input = $('#' + label.attr('for'));

			if (!input.prop('checked')) {
				label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
				if (input.val() == '') {
					label.addClass('active btn-primary');
				} else if (input.val() == 0) {
					label.addClass('active btn-danger');
				} else {
					label.addClass('active btn-success');
				}
				input.prop('checked', true);
			}
		});
		$(".btn-group input[checked=checked]").each(function()
		{
			if ($(this).val() == '') {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-primary');
			} else if ($(this).val() == 0) {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-danger');
			} else {
				$("label[for=" + $(this).attr('id') + "]").addClass('active btn-success');
			}
		});

    jQuery("#owl-demo").owlCarousel({
     
    //autoPlay: 3000, Set AutoPlay to 3 seconds
    navigation : true, // Show next and prev buttons
     
    items : 4,
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [979,3]
     
    });
    // Custom Navigation Events

    $(".next").click(function(){
        owl.trigger('owl.next');
    })
    $(".prev").click(function(){
        owl.trigger('owl.prev');
    })


    jQuery("#owl-testi").owlCarousel({
    navigation : false,
    pagination : false,
    singleItem : true,
    loop:true,
    paginationSpeed:3000,
    });

    window.setInterval(function(){
            jQuery("#owl-testi").trigger('owl.next');
        },5000);


    jQuery("#owl-offer").owlCarousel({
     
    autoPlay: 3000, //Set AutoPlay to 3 seconds
     
    items : 1,
    itemsDesktop : [1199,1],
    itemsDesktopSmall : [979,1]
     
    });
    jQuery("#owl-videos").owlCarousel({
     
    //autoPlay: 3000, Set AutoPlay to 3 seconds
     
    items : 1,
    itemsDesktop : [1199,1],
    itemsDesktopSmall : [979,1]
     
    });
    // Custom Navigation Events
    $(".next").click(function(){
        owl.trigger('owl.next');
    })
    $(".prev").click(function(){
        owl.trigger('owl.prev');
    })
	})
})(jQuery);



