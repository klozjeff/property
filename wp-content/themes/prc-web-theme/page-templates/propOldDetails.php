<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 7/8/2017
 * Time: 12:14 PM
 */
?>

<div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">


            <div class="container">

                <div class="row">

                    <div class="col-xs-12 site-main-content">

                        <main id="main" class="site-main">

                            <div class="white-box user-profile-wrapper">
                                <form id="prcweb-save-items" action="<?php echo admin_url('admin-ajax.php'); ?>" class="submit-form" enctype="multipart/form-data" method="post">

                                        <table class="table table-bordered table-striped" cellpadding="10" cellspacing="1">

                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Property Name</th>
                                                <th>Status</th>
                                                <th>Amount</th>
                                                <th>Balance</th>
                                                <th>Plot No.</th>
                                                <th>Next Installment Date</th>
                                                <th>Resell</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $t=0;
                                            foreach ($property as $item){
                                                $t+=1;
                                                ?>
                                                <tr>
                                                    <td><?php echo $t; ?></td>
                                                    <td><?php echo $item->project_id; ?></td>
                                                    <td><?php echo $item->status; ?></td>
                                                    <td><?php echo $item->amount; ?></td>
                                                    <td><?php echo $item->balance; ?></td>
                                                    <td><?php echo $item->plot_no; ?></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>

                                                </tr>
                                                <?php

                                            }
                                            ?>


</tbody>
</table>


</form>
</div>
<!-- .user-profile-wrapper -->


</main>
<!-- .site-main -->

</div>
<!-- .site-main-content -->

</div>
<!-- .row -->

</div>
<!-- .container -->

</div>
<!-- .site-content -->

</div><!-- .site-content-wrapper -->







///Full width Row
<div class="property property-row shadow-hover">
    <a href="#" class="property-img">
        <div class="img-fade"></div>
        <div class="property-tag button status">For Sale</div>
        <div class="property-price">$150,000</div>
        <div class="property-color-bar"></div>
        <img src="images/property-img1.jpg" alt="">
    </a>
    <div class="property-content">
        <div class="property-title">
            <h4><a href="#">Modern Family Home</a></h4>
            <p class="property-address"><i class="fa fa-map-marker icon"></i>123 Smith Dr, Annapolis, MD</p>
            <div class="clear"></div>
            <p class="property-text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet...</p>
        </div>
        <table class="property-details">
            <tbody><tr>
                <td><i class="fa fa-bed"></i> 3 Beds</td>
                <td><i class="fa fa-tint"></i> 2 Baths</td>
                <td><i class="fa fa-expand"></i> 25,000 Sq Ft</td>
            </tr>
            </tbody></table>
    </div>
    <div class="property-footer">
        <span class="left"><i class="fa fa-calendar-o icon"></i> 5 days ago</span>
        <span class="right">
          <a href="#"><i class="fa fa-heart-o icon"></i></a>
          <a href="#"><i class="fa fa-share-alt"></i></a>
          <a href="#" class="button button-icon"><i class="fa fa-angle-right"></i>Details</a>
        </span>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>