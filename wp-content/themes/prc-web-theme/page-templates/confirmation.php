<?php
/**
 * Template Name:Confirmation
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/19/2017
 * Time: 12:33 PM
 */
global $current_user;
$current_user_meta = get_user_meta($current_user->ID);
get_header();
get_template_part( 'partials/header/banner' );
?>

<div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">
            <div class="desk-progress-bar">
                <div class="progress-bar-inner">
                   <!-- <div class="navbar-header">
                        <a class="navbar-brand" href="../index.html">
                            <img src="<?=get_template_directory_uri();?>/images/nakuru_greens.png"
                                 onerror=""
                                 alt="Nukuru Greens | Everdine UK">
                        </a>
                    </div>-->
                    <ul>
                        <li>
                            <span>1<i>. Property Details</i></span>
                        </li>
                        <li>
                            <span >2<i>. Customer Information</i></span>
                        </li>
                        <li>
                            <span >3<i>. Payment</i></span>
                        </li>
                        <li>
                            <span class="active">4<i>. Confirmation</i></span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 site-main-content">

                        <main id="main" class="site-main">

                            <div class="white-box user-profile-wrapper">

                                <div id="#billing-intialize" class="billing needsclick dz-clickable">
                                    <div class="dz-message needsclick">
                                      Dear <?php echo esc_attr( $current_user_meta['last_name'][0].' '.$current_user_meta['first_name'][0] );?>, your Payment of Kshs 2,000 for Order No:#001 has been received & confirmed<br>
                                        <span class="dz-note needsclick">
                                            Kindly proceed to your <a style="color:#00acac;text-transform: uppercase;font-size: 16px" href="http://localhost/property/account">ACCOUNT</a> for more info
                                        </span>
                                    </div>
                                </div>

                            </div>
                            <!-- .user-profile-wrapper -->


                        </main>
                        <!-- .site-main -->

                    </div>
                    <!-- .site-main-content -->

                </div>
                <!-- .row -->

            </div>
            <!-- .container -->

        </div>
        <!-- .site-content -->

    </div><!-- .site-content-wrapper -->

<?php
/*
 * Footer
 */
get_footer();
?>