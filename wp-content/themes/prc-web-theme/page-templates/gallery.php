<?php
/**
 * Template Name:PRC Gallery
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/23/2017
 * Time: 2:59 PM
 */

get_header();
/*
 * Homepage Slider or Banner or Google Map
 */
?>
<div class="page-head add-padding-top"
     style="background:url(<?php echo esc_url(get_template_directory_uri() . '/images/banner_gallery.jpg'); ?>) #494c53 no-repeat center top; background-size:cover">

</div>

<div class="container">
    <div class="col-md-12">
        <h2></h2>
        <?php echo do_shortcode("[huge_it_portfolio id='2']"); ?>
    </div>
</div>
<?php
get_footer();
?>
