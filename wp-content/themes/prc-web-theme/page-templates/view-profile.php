<?php
/**
 * Template Name:My Profile
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/27/2017
 * Time: 9:35 PM
 */
if ( !is_user_logged_in() ) {
    wp_redirect(  home_url() );
}
global $current_user, $post,$prcweb_options;

wp_get_current_user();
$userID         = $current_user->ID;
$user_login     = $current_user->user_login;
get_header();

get_template_part( 'partials/header/banner' );
$details = new Prc_Custom_Query( 'prc_property_order_items' );
$property= $details->showMyProperties($userID);
//var_dump($properties);
//die();
?>
    <div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">


            <div class="container">

                <div class="row">

                    <div class="col-xs-12 site-main-content">

                        <main id="main" class="site-main">
                            <?php
                            if( is_user_logged_in() ) {
                                // get user information
                               // $current_user = wp_get_current_user();
                                $current_user_meta = get_user_meta($current_user->ID);

                                ?>
                                <div class="white-box user-profile-wrapper" style="margin-bottom: 10px;padding-bottom: 0px !important;">
                                    <div class="col-md-2">
                                        <div class="form-option user-profile-img-wrapper clearfix">
                                            <div id="user-profile-img">
                                                <div class="profile-thumb">
                                                    <img class="img-circle"
                                                         src="<?= get_template_directory_uri(); ?>/images/upload-sample-image.jpg"
                                                         alt="User Image">
                                                    <input class="profile-image-id" name="profile-image-id" value="4018"
                                                           type="hidden">
                                                    <span id="select-profile-image" class="label"><i
                                                            class="fa fa-edit"></i></span>
                                                    <div id="errors-log"></div>
                                                    <div id="plupload-container"></div>
                                                </div>
                                            </div>

                                            <!-- #user-profile-img -->

                                            <!-- .profile-img-controls -->
                                        </div>

                                    </div>

                                    <div class="col-md-10" style="margin-left: -60px;">
                                        <div class="personal-info">
                                            <div class="col-md-12">

                                                <div class="name"><?php echo esc_attr( $current_user_meta['last_name'][0].' '.$current_user_meta['first_name'][0] );?></div>

                                            </div>

                                                <div class="col-sm-6 small-details">

                                                    <span class="listItem"><i class="fa fa-envelope-o"></i> <?php echo $current_user->user_email;?></span>
                                                       <span class="listItem"><i class="fa fa-phone"></i> <?php echo esc_attr( $current_user_meta['mobile_number'][0]);?></span>
                                                    <!--<span class="listItem"><i class="fa fa-file"></i> <?php echo esc_attr( $current_user_meta['id_number'][0]);?></span>
-->
                                                </div>
                                                <div class="col-sm-6 other-details pull-right">
                                                    <span class="listItem"><i class="fa fa-linkedin"></i></span>
                                                    <span class="listItem"><i class="fa fa-facebook"></i> </span>
                                                    <span class="listItem"><i class="fa fa-google-plus"></i> </span>
                                                    <span class="listItem"><i class="fa fa-twitter"></i> </span>

                                                </div>

                                            <div style="margin:0px!important;padding: 0px !important;"
                                                 class="col-md-12">
                                                <hr/>
                                            </div>
                                            <div class="col-md-8 left-details">

                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-6 col-xs-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="card-block">

                                                                    <div class="media">
                                                                        <div class="media-body text-xs-left">
                                                                            <h3 class="pink"><?php echo $details->totalMyProperties($userID);?></h3>
                                                                            <span>No. of Properties</span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-2 col-lg-6 col-xs-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="card-block">
                                                                    <div class="media">
                                                                        <div class="media-body text-xs-left">
                                                                            <h3 class="teal">0</h3>
                                                                            <span>Scheduled Site Visits</span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xl-3 col-lg-6 col-xs-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="card-block">
                                                                    <div class="media">
                                                                        <div class="media-body text-xs-left">
                                                                            <h3 class="deep-orange">64.89 %</h3>
                                                                            <span>Credit Score</span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-3 col-lg-6 col-xs-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="card-block">
                                                                    <div class="media">
                                                                        <div class="media-body text-xs-left">
                                                                            <h3 class="cyan">0</h3>
                                                                            <span>Enquiries/Tickets</span>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-4">


                                           <span class="btn btn-action">VIEW FULL PROFILE</span>

                                            </div>
                                        </div>
                                    </div>

                                    <ul class="nav nav-tabs">
                                        <li class="tab-title active" data-tab="property_details"><a href="#my_properties"> MY PROPERTIES</a></li>
                                        <li class="tab-title" data-tab="property_video"><a href="#statements">STATEMENTS</a></li>
                                        <li class="tab-title" id="mapTab" data-tab="property_map"><a href="#reports">REPORTS</a></li>

                                    </ul>
                                </div>
                                <div class="row match-height">
                                    <?php
                                    foreach ($property as $item):?>
                                    <div class="col-xl-3 col-md-3 col-sm-12">
                                        <div class="card" style="height: auto;">
                                            <div class="card-body">
                                                <img class="card-img-top img-fluid" src="<?= get_template_directory_uri(); ?>/images/property1.jpg" alt="Card image cap">
                                                <div class="card-block">
                                                    <h4 class="card-title"><?php echo $item->project_id; ?></h4>
                                                    <p class="card-text">Plot No: <?php echo $item->plot_no; ?>
                                                    </p>
                                                    <p class="card-text">Price: <?php echo $item->amount; ?></p>
                                                    <a href="#" class="btn btn-outline-teal">View Details</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <?php endforeach;?>


                                </div>

                                <?php
                            }
                                ?>
                            <!-- .user-profile-wrapper -->


                        </main>
                        <!-- .site-main -->

                    </div>
                    <!-- .site-main-content -->

                </div>
                <!-- .row -->

            </div>
            <!-- .container -->

        </div>
        <!-- .site-content -->

    </div><!-- .site-content-wrapper -->


<?php
get_footer();
?>
<script type="text/javascript">
    jQuery(function ($) {
        $(document).ready(function () {

            $('ul.nav-tabs li').click(function () {
                var tab_id = $(this).attr('data-tab');


                $('ul.nav-tabs li').removeClass('active');
                $('.vc_tta-panel').removeClass('active');

                $(this).addClass('active');
                $("#" + tab_id).addClass('active');

            })

            $('.info_link').click(function(){
                alert($(this).attr('href'));
                // or alert($(this).hash();
            });

        });
    });
</script>
