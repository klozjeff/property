<?php
/**
 * Template Name:User Dashboard
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 7/8/2017
 * Time: 10:55 AM
 */
if ( !is_user_logged_in() ) {
    wp_redirect(  home_url() );
}
global $current_user, $post,$prcweb_options;

wp_get_current_user();
$userID         = $current_user->ID;
$user_login     = $current_user->user_login;
get_header();

get_template_part( 'partials/header/banner' );
$details = new Prc_Custom_Query( 'prc_customers_information' );
$personalInfo= $details->get_by(array('wpID' => $userID),'=',true);

?>
<div id="content-wrapper" class="site-content-wrapper site-pages">

    <div id="content" class="site-content layout-boxed">
        <div class="container">
            <?php
            if( is_user_logged_in() ):
            // get user information
            // $current_user = wp_get_current_user();
            $current_user_meta = get_user_meta($current_user->ID);

            ?>
            <div class="row user-profile">
                <div class="col-lg-3 col-md-3 sidebar-left">
                    <div class="widget member-card">
                        <div class="member-card-header">
                            <a href="#" class="member-card-avatar"><img src="<?= get_template_directory_uri(); ?>/images/upload-sample-image.jpg" alt=""></a>
                            <h3><?php echo esc_attr( $current_user_meta['last_name'][0].' '.$current_user_meta['first_name'][0] );?></h3>
                           <!-- <p><i class="fa fa-envelope icon"></i>jdoe@gmail.com</p>-->
                        </div>
                        <div class="member-card-content">
                            <!--<img class="hex" src="images/hexagon.png" alt="">-->
                            <ul>

                                <li><a href="account"><i class="fa fa-user icon"></i>Profile</a></li>
                                <li><a href="my-properties"><i class="fa fa-map-marker icon"></i>My Properties</a></li>
                                <li><a href="statements"><i class="fa fa-file-excel-o icon"></i>Statements</a></li>
                                <li><a href="#"><i class="fa fa-reply icon"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 col-md-9">
                    <form id="inspiry-edit-user" action="<?php echo admin_url('admin-ajax.php'); ?>"  enctype="multipart/form-data" method="post">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="edit-avatar">
                                    <img class="profile-avatar" src="<?= get_template_directory_uri(); ?>/images/upload-sample-image.jpg" alt="">
                                    <a href="#" class="button small">Change Avatar</a>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <h4>Account Details</h4>
                                <div class="divider"></div>
                                <div class="form-block col-md-12">
                                    <label class="col-sm-3 control-label" >Account No</label>
                                    <div class="col-sm-6">
                                        <input type="text" readonly name="account_no" value="<?php echo $personalInfo->crm_account_no;?>"  class="form-control border">
                                    </div>

                                         </div>
                                <div class="form-block col-md-6">
                                    <label>Surname</label>
                                    <input class="form-control border" name="first-name" id="first-name" value="<?php echo $personalInfo->surname;?>" type="text">
                                </div>
                                <div class="form-block col-md-6">
                                    <label>Other Names</label>
                                    <input class="form-control border" name="last-name" id="last-name" value="<?php echo $personalInfo->other_names;?>" type="text">
                                </div>
                                <div class="form-block col-md-6">
                                    <label>Email</label>
                                    <input class="form-control border" name="email"  id="email" value=" <?php echo $current_user->user_email;?>" type="text">
                                </div>
                                <div class="form-block col-md-6">
                                    <label>Phone</label>
                                    <input class="form-control border" name="mobile-number"  id="mobile-number" value="<?php echo $personalInfo->mobile_number;?>" type="text">
                                </div>
                                <div class="form-block col-md-6">
                                    <label>ID No</label>
                                    <input class="form-control border" name="id_number" id="id_number" value="<?php echo $personalInfo->id_number;?>" type="text">
                                </div>
                                <div class="form-block col-md-6">
                                    <label>Gender</label>
                                    <select class="form-control" name="gender" id="gender"  type="text">
                                        <option value=""></option>
                                        <option value="Male"<?php if($personalInfo->gender=='Male') { echo 'Selected'; } ?>>Male</option>
                                        <option value="Female"<?php if($personalInfo->gender=='Female') { echo 'Selected'; } ?>>Female</option>
                                        <option value="Couple"<?php if($personalInfo->gender=='Couple') { echo 'Selected'; } ?>>Couple</option>
                                        <option value="Group"<?php if($personalInfo->gender=='Group') { echo 'Selected'; } ?>>Group</option>
                                    </select>
                                </div>
                                <div class="form-block col-md-6">
                                    <label>Date of Birth</label>
                                    <input class="form-control border" name="DateOfBirth" id="DateOfBirth" value="<?php echo $personalInfo->dob;?>" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <h4>Address Details</h4>
                                <div class="divider"></div>
                                <div class="form-block">
                                    <label>P.O BOX</label>
                                    <input class="form-control border" name="box_number" id="box_number" value="<?php echo $personalInfo->postal_address;?>" type="text">
                                </div>

                                <div class="form-block">
                                    <label>Postal Code</label>
                                    <input class="form-control border" name="postal_code"   id="postal_code" value="<?php echo $personalInfo->zip_code;?>" type="text">
                                </div>

                                <div class="form-block">
                                    <label>City</label>
                                    <input class="form-control border" name="town" id="town" value="<?php echo $personalInfo->town;?>" type="text">
                                </div>
                                <div class="form-block">
                                    <label>Residence</label>
                                    <input class="form-control border" name="physical_add" id="physical_add" value="<?php echo $personalInfo->physical_address;?>" type="text">
                                </div>
                                <div class="form-block">
                                    <label>Country</label>
                                    <input class="form-control border" name="country"  id="country"value="<?php echo $personalInfo->country;?>" type="text">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h4>Next of Kin Details</h4>
                                <div class="divider"></div>
                                <div class="form-block">
                                    <label>Next of Kin</label>
                                    <input class="form-control border" value="<?php echo $personalInfo->kin_name;?>" name="kin_name" id="kin_name" type="text">
                                </div>

                                <div class="form-block">
                                    <label>Relationship</label>
                                    <input class="form-control border" value="<?php echo $personalInfo->relationship;?>" name="relationship" id="relationship" type="text">
                                </div>

                                <div class="form-block">
                                    <label>Mobile No</label>
                                    <input class="form-control border" value="<?php echo $personalInfo->kin_mobile;?>" name="kin_mobile" id="kin_mobile" type="text">
                                </div>
                                <div class="form-block">
                                    <label>Kin ID No.</label>
                                    <input class="form-control border" value="<?php echo $personalInfo->kin_id;?>" name="kin_id" id="kin_id" type="text">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-option">
                                    <?php
                                    //action hook for plugin and extra fields
                                    do_action( 'edit_user_profile', $current_user );

                                    // WordPress Nonce for Security Check
                                    wp_nonce_field( 'update_user', 'user_profile_nonce' );
                                    ?>
                                    <input type="hidden" name="action" value="prcweb_update_profile" />
                                    <input type="hidden" name="redirect_to" value="<?php echo esc_url( home_url( '/account/' ) ); ?>" />
                                </div>
                            </div>
                        </div><!-- end row -->

                        <div class="form-block">
                            <button type="submit" id="update-user"  name="update-user"  class="button button-icon pull-right"><i class="fa fa-check"></i>Update Profile</button>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader-2.gif" id="ajax-loader" alt="Loading...">
                        </div>

                        <p id="form-message"></p>
                        <ul id="form-errors"></ul>
                    </form>

                </div><!-- end col -->
            </div>
        </div>
        <?php
        endif;
        ?>
    </div>
</div>
<?php
get_footer();
?>
