<?php
/**
 * Template Name:Company
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 8/22/2017
 * Time: 9:48 AM
 */
get_header();
global $prcweb_options;
global $pageName; //$pageName from body.php
$pagename = get_query_var('pagename');

//Governance Query
$number_of_team = 4;
$team = array(
    'post_type' => 'team',
    'posts_per_page' => $number_of_team,
    'orderby' => array(
        'ID DESC'
    )
);
wp_reset_query();

$governance = new WP_Query($team);

//Press Query
$number_of_press = 10;
$press = array(
    'post_type' => 'press',
    'posts_per_page' => $number_of_press,
    'orderby' => array(
        'ID DESC'
    )
);
wp_reset_query();

$pressRelease = new WP_Query($press);

?>
<div class="page-head add-padding-top" style="background:url(<?php echo esc_url(get_template_directory_uri() . '/images/banner_about.jpg'); ?>) #494c53 no-repeat center top;background-size:cover">



</div>
<section class="sub-nav" id="sub-nav">
    <div class="container">
        <div class="col-md-12">
            <div class="actions">
                <a class="primary" href=""><span>Visit Our Blog</span></a></div>
            <a class="prompt-link" href="#">About Us</a>
            <div class="destinations"><a href="<?php echo esc_url(home_url('/about-us')); ?>"
                                         <?php if ($pagename == 'about-us'): ?>class="active"<?php endif; ?>>About
                    Us</a>
                <a href="<?php echo esc_url(home_url('/governance')); ?>"
                   <?php if ($pagename == 'governance'): ?>class="active"<?php endif; ?>>Governance</a>
                <a href="<?php echo esc_url(home_url('/careers')); ?>"
                   <?php if ($pagename == 'careers'): ?>class="active"<?php endif; ?>>Careers</a>
                <a href="<?php echo esc_url(home_url('/faq')); ?>"
                   <?php if ($pagename == 'faq'): ?>class="active"<?php endif; ?>>FAQs</a>
                <a href="<?php echo esc_url(home_url('/press')); ?>">Press</a>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="col-md-9">
        <?php if ($pagename == 'about-us'): ?>
            <h2 class="text-heading font-size-26 padding-T33">About PRC</h2>
            <p class="para size-full bp768-size-9of10"><?php while (have_posts()) : the_post();


                    the_content();


                endwhile; // End of the loop. ?></p>

            <hr class="section-divider">
            <div class="text-align-center padding-B20 padding-V40">
                <div class="site-container padding-V33">
                    <div class="col-md-8 center"><h2 class="text-heading-inner font-size-20 bp768-font-size-26"
                                                     data-reactid="282">We're hiring</h2>
                        <p class="font-size-15 color-dark-gray margin-V9">Help us continue making property ownership
                            affordable and genuine</p>
                        <a class="btn btn-hollow margin-T20" href="">View our open positions

                        </a></div>
                </div>
            </div>
        <?php elseif ($pagename == 'governance'): ?>

            <h2 class="text-heading font-size-26 padding-T33">Governance</h2>
            <p class="para size-full bp768-size-9of10">The Board is responsible for the overall conduct of the Company’s
                business. The Chairman is responsible for the operation, leadership and governance of the Board,
                ensuring its effectiveness and setting its agenda. The Chief Executive is responsible for the management
                of the Company’s business and the implementation of Board strategy and policy.:</p>
            <h2 class="text-heading-inner font-size-12">Meet the Board</h2>

            <div class="row">
                <div class="content-team">
                    <ul>
                        <?php if ($governance->have_posts()) :

                            while ($governance->have_posts()) :
                                $governance->the_post();

                                ?>
                                <li href="" class="">
                                    <div class="pane-list col-md-4 js-bd-pane">
                                        <div class="tema-list-item pane-block" style="height: 310px;">
                                            <div class="content">
                                                <div class="pane-block-image">
                                                    <div class="image-container">
                                                        <?php the_post_thumbnail(); ?>
                                                    </div>
                                                </div>
                                                <div class="item-block-meta">
                                                    <div class="meta-content">
                                                        <h3>
                                                            <span class="text-render"><?php the_title(); ?></span>
                                                        </h3>
                                                        <p>
                                                            <span class="text-render"><?php echo get_post_meta($post->ID, 'PRC_WEB_team_title', true) ?></span>
                                                        </p>
                                                        <div class="meta-content-cta hide-item js-bio-cta"
                                                             style="display: none;">
                                                            <a class="btn btn-g-f" title="" onclick="PeopleDetails(6)"
                                                               href="javascript:void(0);">Read Bio</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                                <?php
                            endwhile;endif;
                        ?>


                    </ul>
                </div>
            </div>


            <?php
        elseif ($pagename == 'press'):?>
            <h3 class="text-heading-inner font-size-20 margin-B12 padding-T33"><span>In the Press</span></h3>
            <?php if ($pressRelease->have_posts()) :

                while ($pressRelease->have_posts()) :
                    $pressRelease->the_post();

                    ?>
               <div class="col-md-12 margin-B20">
                   <span class="col-md-3 color-medium-gray text-align-right pull-end">01.06.17</span>
                   <span class="col-md-3 color-dark-gray">
                       <img src="<?php echo get_template_directory_uri().'/images/press/the-star-logo.png';?>">
                       <p><?php echo get_post_meta($post->ID, 'PRC_WEB_press_source', true) ?></p>

                   </span>
                   <span class="col-md-6 article-title"><a target="_blank" href="<?php echo get_post_meta($post->ID, 'PRC_WEB_source_link', true) ?>"><?php the_title(); ?></a>
                    <p class="para size-full bp768-size-9of10"><?php prcweb_excerpt( 20 ); ?></p>
                   </span>
               </div>
                    <?php
                endwhile;endif;
            ?>




        <?php endif; ?>
    </div>
    <div class="col-md-3 margin-B40">
        <div class="text-align-left">
            <h2 class="text-heading-small font-size-18 padding-T45">Our Values</h2>
            <div class="bp768-flex bp768-justify-content-space-between bp768-flex-direction-row flex-wrap-wrap margin-B40">
                <div class="align-self-center display-inline-block size-1of2 margin-B27">Customer Focus</div>
                <div class="align-self-center display-inline-block size-1of2 margin-B27">Integrity</div>
                <div class="align-self-center display-inline-block size-1of2 margin-B27">Reliability</div>
                <div class="align-self-center display-inline-block size-1of2 margin-B27">Innovation</div>

            </div>
        </div>
        <h3 class="font-size-20 margin-B12"><span>Contact</span></h3>
        <p class="font-size-15 color-dark-gray margin-T40">For any press enquiries, please
            contact <a href="mailto:press@gocardless.com">the PRC PR Team</a>
            , or view our comprehensive <a
                    href="">press pack</a>
        </p>
        <h3 class="font-size-20 margin-B12"><span>Resources</span></h3>
        <p class="font-size-15 color-dark-gray margin-T40">For any press enquiries, please
            contact <a href="mailto:press@gocardless.com">the PRC PR Team</a>
            , or view our comprehensive <a
                    href="">press pack</a>
        </p>

    </div>

</div>
<?php
get_footer();
?>
