<?php
/**
 * Template Name:PRC Land
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/23/2017
 * Time: 2:46 PM
 */
get_header('brands');

/*
 * Homepage Slider or Banner or Google Map
 */
global $prcweb_options;

// For Demo Purposes
if ( isset( $_GET['module_below_header'] ) ) {
    $prcweb_options[ 'prcweb_home_module_below_header' ] = $_GET['module_below_header'];
    if ( isset( $_GET['module_below_header'] ) ) {
        $prcweb_options[ 'prcweb_slider_type' ] = $_GET['slider_type'];
    }
}
if ( $prcweb_options[ 'prcweb_home_module_below_header' ] == 'slider' ) {

    /*
     * Types of slider
     */
    if ( $prcweb_options[ 'prcweb_slider_type' ] == 'revolution-slider' ) {

        $revolution_slider_alias = "housing-slider-v1";
        if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
            if ( $prcweb_options[ 'prcweb_header_variation' ] == '1' ) {
                // Image Banner should be displayed with header variation one to keep things in order
                get_template_part( 'partials/header/banner' );
            }
            echo '<div class="prcweb-revolution-slider">';
            putRevSlider( $revolution_slider_alias );
            echo '</div>';
        } else {
            get_template_part( 'partials/header/banner' );
        }

    } elseif ( $prcweb_options[ 'prcweb_slider_type' ] == 'properties-slider-two' ) {
        get_template_part ( 'partials/home/slider-two');
    } elseif ( $prcweb_options[ 'prcweb_slider_type' ] == 'properties-slider-three' ) {
        get_template_part ( 'partials/home/slider-three' );
    } else {
        get_template_part( 'partials/home/slider' );
    }

} else if ( $prcweb_options[ 'prcweb_home_module_below_header' ] == 'google-map' ) {
    // Google Map
    get_template_part( 'partials/header/map' );
} else {
    // Image Banner
    get_template_part( 'partials/header/banner' );
}

/*
 * Home section width
 */
global $prcweb_home_sections_width;
$prcweb_home_sections_width = $prcweb_options[ 'prcweb_home_sections_width' ];
if ( !in_array( $prcweb_home_sections_width, array( 'boxed', 'wide' ) ) ) {
    $prcweb_home_sections_width = 'wide';
}
if ( $prcweb_options[ 'prcweb_home_search' ] && $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
    get_template_part( 'partials/land/search' );
}

// Homepage Layout Manager
$enabled_sections = $prcweb_options['prcweb_brand_sections']['enabled'];

if ( $enabled_sections ) {
    foreach ($enabled_sections as $key => $val  ) {
        switch( $key ) {
            /*
         * How it works
         */
            case 'sub-nav':
                get_template_part( 'partials/brand/sub-nav' );
                break;
            case 'featured':
                get_template_part( 'partials/brand/brand-featured' );
                break;
            case 'testimonials':
                get_template_part('partials/brand/brand-testimonials');
                break;
            case 'press-release':
                get_template_part('partials/land/brand-press');
                break;

        }

    }
}
get_footer();
?>
