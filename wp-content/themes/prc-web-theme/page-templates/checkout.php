<?php
/**
 * Template Name: Checkout
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/13/2017
 * Time: 4:51 PM
 */
get_header();

get_template_part( 'partials/header/banner' );
//echo cartItems('plot', 'none!');
global $post;
$propertyID=$_SESSION["property-id"];

?>
    <div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">
            <div class="desk-progress-bar">
                <div class="progress-bar-inner">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="../index.html">

                    </a>
                    </div>
                    <ul>
                        <li>
                            <span class="active">1<i>. Property Details</i></span>
                        </li>
                        <li>
                            <span >2<i>. Customer Information</i></span>
                        </li>
                        <li>
                            <span>3<i>. Payment</i></span>
                        </li>
                        <li>
                            <span >4<i>. Confirmation</i></span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 site-main-content">

                        <main id="main" class="site-main">

                            <div class="white-box user-profile-wrapper">
                                <form id="prcweb-save-items" action="<?php echo admin_url('admin-ajax.php'); ?>" class="submit-form" enctype="multipart/form-data" method="post">
                                <?php
                                if(isset($_SESSION["cart-items"])){
                                    $item_total = 0;
                                 //   error_reporting(0);
                                    ?>
                                <table class="table table-bordered table-striped" cellpadding="10" cellspacing="1">

                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Property</th>
                                        <th>Plot No</th>
                                        <th>Price (KES)</th>
                                        <th>Downpayment Per Plot</th>
                                        <th>Payment Plan</th>
                                        <th>Monthly Installment Amount</th>
                                        <th>Interest</th>
                                        <th>New Price</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $t=0;
                                    foreach ($_SESSION["cart-items"] as $item){
                                        $t+=1;
                                        ?>
                                        <tr>
                                            <td><?php echo $t; ?></td>
                                            <td><input type="hidden" name="property_name[]" value="<?php echo $item["property"]; ?>"><strong><?php echo $item["property"]; ?></strong></td>
                                            <td><input type="hidden" name="plot_number[]" value="<?php echo $item["plot"]; ?>"> <?php echo $item["plot"]; ?></td>
                                            <td id="originalprice_<?php echo $t; ?>"><input type="hidden" name="plot_price[]" value="<?php echo $item["price"]; ?>"><?php echo number_format($item["price"]); ?></td>
                                            <td><input type="text" name="depositamount[]" id="depositamount_<?php echo $t; ?>" class="deposit" value="0"/></td>
                                            <td><select id="payment_plan_<?php echo $t; ?>" name="payment_plan[]" onchange="computeInterest('<?php echo $t; ?>')">
                                                    <option value="">--Select Plan--</option>
                                                    <option value="1">Cash</option>
                                                    <option value="2">2 Months</option>
                                                    <option value="3">3 Months</option>
                                                    <option value="4">4 Months</option>
                                                    <option value="5">5 Months</option>
                                                    <option value="6">6 Months</option>
                                                    <option value="7">7 Months</option>
                                                    <option value="8">8 Months</option>
                                                    <option value="9">9 Months</option>
                                                    <option value="10">10 Months</option>
                                                    <option value="11">11 Months</option>
                                                    <option value="12">12 Months</option>
                                                </select>

                                                <input type="hidden" name="installmentamount[]" id="installment_amount_<?php echo $t; ?>" />
                                                <input type="hidden" name="newprice[]" id="input_newprice_<?php echo $t; ?>" />

                                            </td>

                                            <td id="installmentamount_<?php echo $t; ?>">
                                                 0
                                            </td>
                                            <td id="interest_<?php echo $t; ?>" class="interest">
                                                0
                                            </td>
                                            <td id="newprice_<?php echo $t; ?>" class="new_price">
                                                0
                                            </td>
                                     </tr>
                                        <?php
                                        $item_total += $item["price"];
                                    }
                                    ?>

                                    <tr>
                                        <td colspan="2" align=right><strong>Total:</strong></td>
                                       <td></td>
                                        <td> <?php echo "Kshs ".number_format($item_total); ?></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td id="total_interest" align=right>Kshs 0</td>
                                        <td id="total_new_price" align=right>Kshs 0</td>

                                    </tr>
                                    <input type="hidden" id="total_new_price_field" name="total_new_price"/>
                                    <input type="hidden" id="total_deposit_field" name="total_deposit"/>
                                    </tbody>
                                </table>
                                <?php
                                }
                                ?>
                                <div class="form-option">
                                    <?php
                                    //action hook for plugin and extra fields
                                    do_action( 'save-items', $current_user );
                                    // WordPress Nonce for Security Check
                                    wp_nonce_field( 'save-items', 'save_items_nonce' );
                                    ?>
                                    <input type="hidden" name="action" value="prcweb_save_items" />
                                    <input type="hidden" name="redirect_to" value="<?php echo esc_url( home_url( '/my-information/' ) ); ?>" />

                                    <input type="submit" id="save-items" name="save-items" class="btn-small btn-orange" value="<?php _e( 'Continue', 'prcweb' ); ?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader-2.gif" id="ajax-loader" alt="Loading...">
                                </div>

                                <p id="form-message"></p>
                                <ul id="form-errors"></ul>
                                </form>
                            </div>
                            <!-- .user-profile-wrapper -->


                        </main>
                        <!-- .site-main -->

                    </div>
                    <!-- .site-main-content -->

                </div>
                <!-- .row -->

            </div>
            <!-- .container -->

        </div>
        <!-- .site-content -->

    </div><!-- .site-content-wrapper -->

<?php
/*
 * Footer
 */
get_footer();
?>

