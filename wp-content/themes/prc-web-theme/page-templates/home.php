<?php
/*
 * Template Name: Home
 */
get_header();

    /*
     * Homepage Slider or Banner or Google Map
     */
    global $prcweb_options;

    // For Demo Purposes
    if ( isset( $_GET['module_below_header'] ) ) {
        $prcweb_options[ 'prcweb_home_module_below_header' ] = $_GET['module_below_header'];
        if ( isset( $_GET['module_below_header'] ) ) {
            $prcweb_options[ 'prcweb_slider_type' ] = $_GET['slider_type'];
        }
    }
    if ( $prcweb_options[ 'prcweb_home_module_below_header' ] == 'slider' ) {

        /*
         * Types of slider
         */
        if ( $prcweb_options[ 'prcweb_slider_type' ] == 'revolution-slider' ) {

            $revolution_slider_alias = "home-slider-v1";
            if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
                if ( $prcweb_options[ 'prcweb_header_variation' ] == '1' ) {
                    // Image Banner should be displayed with header variation one to keep things in order
                    get_template_part( 'partials/header/banner' );
                }
                echo '<div class="prcweb-revolution-slider">';
                putRevSlider( $revolution_slider_alias );
                echo '</div>';
            } else {
                get_template_part( 'partials/header/banner' );
            }

        } elseif ( $prcweb_options[ 'prcweb_slider_type' ] == 'properties-slider-two' ) {
            get_template_part ( 'partials/home/slider-two');
        } elseif ( $prcweb_options[ 'prcweb_slider_type' ] == 'properties-slider-three' ) {
            get_template_part ( 'partials/home/slider-three' );
        } else {
            get_template_part( 'partials/home/slider' );
        }

    } else if ( $prcweb_options[ 'prcweb_home_module_below_header' ] == 'google-map' ) {
        // Google Map
        get_template_part( 'partials/header/map' );
    } else {
        // Image Banner
        get_template_part( 'partials/header/banner' );
    }

    /*
     * Home section width
     */
    global $prcweb_home_sections_width;
    $prcweb_home_sections_width = $prcweb_options[ 'prcweb_home_sections_width' ];
    if ( !in_array( $prcweb_home_sections_width, array( 'boxed', 'wide' ) ) ) {
        $prcweb_home_sections_width = 'wide';
    }
    if ( $prcweb_options[ 'prcweb_home_search' ] && $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
                    get_template_part( 'partials/home/search' );
                }

                // Homepage Layout Manager
                $enabled_sections = $prcweb_options['prcweb_home_sections']['enabled'];

                if ( $enabled_sections ) {
                    foreach ($enabled_sections as $key => $val  ) {
                        switch( $key ) {
								/*
                             * How it works
                             */
                            case 'how-it-works':
                                get_template_part( 'partials/home/why-us' );
                                break;

                            case 'featured':
                                get_template_part( 'partials/home/prcweb_featured' );
                                break;
                            case 'm-ploti':
                                get_template_part('partials/home/m-ploti');
                                break;
                            case 'home-testimonials':
                                get_template_part('partials/home/testimonials');
                                break;
                            case 'partners':
                                get_template_part('partials/home/prc-awards');
                                break;

                        }

                    }
                }

get_footer();
?>