<?php
/**
 * Template Name: Edit Profile
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/7/2017
 * Time: 4:55 PM
 */
get_header();

get_template_part( 'partials/header/banner' );
?>
    <div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">
            <div class="desk-progress-bar">
                <div class="progress-bar-inner">
                   <!-- <div class="navbar-header">
                        <a class="navbar-brand" href="../index.html">
                            <img src="<?=get_template_directory_uri();?>/images/nakuru_greens.png"
                                 onerror=""
                                 alt="Nukuru Greens | Everdine UK">
                        </a>
                    </div>-->
                    <ul>
                        <li>
                            <span>1<i>. Property Details</i></span>
                        </li>
                        <li>
                            <span class="active">2<i>. Customer Information</i></span>
                        </li>
                        <li>
                            <span >3<i>. Payment</i></span>
                        </li>
                        <li>
                            <span >4<i>. Confirmation</i></span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 site-main-content">

                        <div id="main" class="site-main">

                            <div class="white-box">
                                <div class="user-profile-wrapper">
                                <?php
                                /*
                                 * Display page contents if any
                                 */
                                if ( have_posts() ):
                                    while ( have_posts() ):
                                        the_post();
                                        $content = get_the_content();
                                        if ( !empty( $content ) ) {
                                            ?>
                                            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> >
                                                <div class="entry-content clearfix">
                                                    <?php the_content(); ?>
                                                </div>
                                            </article>
                                            <?php
                                        }
                                    endwhile;
                                endif;

                                if( is_user_logged_in() ) {
                                    // get user information
                                    $current_user = wp_get_current_user();
                                    $current_user_meta = get_user_meta( $current_user->ID );

                                    ?>
                                    <form id="inspiry-edit-user" action="<?php echo admin_url('admin-ajax.php'); ?>" class="submit-form" enctype="multipart/form-data" method="post">



                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="first-name"><?php _e('Surname', 'inspiry'); ?></label>
                                                    <input class="valid required" name="first-name" type="text" id="first-name"
                                                           value="<?php if( isset( $current_user_meta['first_name'] ) ) { echo esc_attr( $current_user_meta['first_name'][0] ); } ?>"
                                                           title="<?php _e('* Provide First Name!', 'prcweb'); ?>"
                                                           autofocus />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="last-name"><?php _e('Other Names', 'prcweb'); ?></label>
                                                    <input class="required" name="last-name" type="text" id="last-name"
                                                           value="<?php if( isset( $current_user_meta['last_name'] ) ) {  echo esc_attr( $current_user_meta['last_name'][0] ); } ?>"
                                                           title="<?php _e('* Provide Last Name!', 'prcweb'); ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="display-name"><?php _e('ID Number', 'prcweb'); ?> *</label>
                                                    <input class="required" name="id_number" type="text" id="id_number"
                                                           value="<?php if( isset( $current_user_meta['id_number'] ) ) {  echo esc_attr( $current_user_meta['id_number'][0] ); } ?>"
                                                           title="<?php _e('* Provide Display Name!', 'prcweb'); ?>"
                                                           required />
                                                </div>
                                            </div>

                                        </div>
                                        <!-- .row -->

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="email"><?php _e('Email', 'prcweb'); ?> *</label>
                                                    <input class="email required" name="email" type="email" id="email"
                                                           value="<?php echo esc_attr( $current_user->user_email ); ?>"
                                                           title="<?php _e('* Provide Valid Email Address!', 'prcweb'); ?>"
                                                           required/>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="mobile-number"><?php _e('Mobile Number', 'prcweb'); ?></label>
                                                    <input class="digits" name="mobile-number" type="text" id="mobile-number"
                                                           value="<?php if( isset( $current_user_meta['mobile_number'] ) ) { echo esc_attr( $current_user_meta['mobile_number'][0] ); } ?>"
                                                           title="<?php _e('* Only Digits are allowed!', 'prcweb'); ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="office-number"><?php _e('Alternative Number', 'prcweb'); ?></label>
                                                    <input class="digits" name="office-number" type="text" id="office-number"
                                                           value="<?php if( isset( $current_user_meta['office_number'] ) ) { echo esc_attr( $current_user_meta['office_number'][0] ); } ?>"
                                                           title="<?php _e('* Only Digits are allowed!', 'prcweb'); ?>" />
                                                </div>
                                            </div>


                                        </div>
                                        <!-- .row -->

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="fax-number"><?php _e('Physical Address/Residential area', 'prcweb'); ?></label>
                                                    <input class="valid required" name="physical_add" type="text" id="physical_add"
                                                           value="<?php if( isset( $current_user_meta['physical_add'] ) ) { echo esc_attr( $current_user_meta['physical_add'][0]); } ?>"
                                                           title="<?php _e('* Provide Physical Address details!', 'prcweb'); ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="twitter-url"><?php _e('Country', 'prcweb'); ?></label>
                                                    <input class="valid" name="country" type="text" id="country"
                                                           value="<?php if( isset( $current_user_meta['country'] ) ) { echo esc_attr( $current_user_meta['country'][0] ); } ?>"
                                                           title="<?php _e('* Provide Valid country!', 'prcweb'); ?>" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="facebook-url"><?php _e('Town/County/State', 'prcweb'); ?></label>
                                                    <input class="valid" name="town" type="text" id="town"
                                                           value="<?php if( isset( $current_user_meta['town'] ) ) { echo esc_attr( $current_user_meta['town'][0] ); } ?>"
                                                           title="<?php _e('* Provide Valid  Town!', 'prcweb'); ?>" />
                                                </div>
                                            </div>




                                        </div>
                                        <!-- .row -->
<h4 style="color:#07AA9C"> Next of Kin Details</h4>
                                        <hr/>
                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="google-plus-url"><?php _e('Name', 'prcweb'); ?></label>
                                                    <input class="valid required" name="kin_name" type="text" id="kin_name"
                                                           value="<?php if( isset( $current_user_meta['kin_name'] ) ) { echo esc_attr( $current_user_meta['kin_name'][0]); } ?>"
                                                           title="<?php _e('* Provide Name of Next of Kin!', 'prcweb'); ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="linkedin-url"><?php _e('Relationship', 'prcweb'); ?></label>
                                                    <input class="valid required" name="relationship" type="text" id="relationship"
                                                           value="<?php if( isset( $current_user_meta['relationship'] ) ) { echo esc_attr( $current_user_meta['relationship'][0]); } ?>"
                                                           title="<?php _e('* Provide Relationship!', 'prcweb'); ?>" />
                                                </div>
                                            </div>


                                        </div>
                                        <!-- .row -->

                                        <!-- todo: add instagram and pinterest -->

                                        <div class="row">

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="pass1"><?php _e( 'Mobile No', 'prcweb' ); ?>

                                                    </label>
                                                    <input class="digits" name="kin_mobile" type="text" id="kin_mobile"
                                                           value="<?php if( isset( $current_user_meta['kin_mobile'] ) ) { echo esc_attr( $current_user_meta['kin_mobile'][0]); } ?>"
                                                           title="<?php _e('* Provide Contact Details for Next of Kin!', 'prcweb'); ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <label for="pass1"><?php _e( 'ID Number', 'prcweb' ); ?>

                                                    </label>
                                                    <input class="valid" name="kin_id" type="text" id="kin_id"
                                                           value="<?php if( isset( $current_user_meta['kin_id'] ) ) { echo esc_attr( $current_user_meta['kin_id'][0]); } ?>"
                                                           title="<?php _e('* Provide ID Details for Next of Kin!', 'prcweb'); ?>" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-option">
                                                    <?php
                                                    //action hook for plugin and extra fields
                                                    do_action( 'edit_user_profile', $current_user );

                                                    // WordPress Nonce for Security Check
                                                    wp_nonce_field( 'update_user', 'user_profile_nonce' );
                                                    ?>
                                                    <input type="hidden" name="action" value="prcweb_update_profile" />
                                                    <input type="hidden" name="redirect_to" value="<?php echo esc_url( home_url( '/payment/' ) ); ?>" />
                                                </div>
                                            </div>

                                        </div>
                                        <!-- .row -->

                                        <div class="form-option">
                                            <input type="submit" id="update-user" name="update-user" class="btn-small btn-orange" value="<?php _e( 'Continue', 'prcweb' ); ?>">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader-2.gif" id="ajax-loader" alt="Loading...">
                                        </div>

                                        <p id="form-message"></p>
                                        <ul id="form-errors"></ul>

                                    </form>
                                    <!-- #inspiry-edit-user -->
                                    <?php

                                } else {
                                    inspiry_message( __( 'Login Required', 'inspiry' ), __( 'You need to login or create to edit your profile!', 'inspiry' ) );
                                }
                                ?>

                            </div>
                        </div>
                            <!-- .user-profile-wrapper -->


                        </main>
                        <!-- .site-main -->

                    </div>
                    <!-- .site-main-content -->

                </div>
                <!-- .row -->

            </div>
            <!-- .container -->

        </div>
        <!-- .site-content -->

    </div><!-- .site-content-wrapper -->

<?php
/*
 * Footer
 */
get_footer();
?>