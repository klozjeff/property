<?php
/**
 * Template Name:My Properties
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/19/2017
 * Time: 12:07 PM
 */
if ( !is_user_logged_in() ) {
    wp_redirect(  home_url() );
}

global $current_user, $post,$prcweb_options;

wp_get_current_user();
$userID         = $current_user->ID;
$user_login     = $current_user->user_login;


get_header();
get_template_part( 'partials/header/banner' );
$details = new Prc_Custom_Query( 'prc_property_order_items' );
$property= $details->showMyProperties($userID);
//var_dump($properties);
//die();
?>


    <div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">
            <div class="container">
<?php
if( is_user_logged_in() ):
    // get user information
    // $current_user = wp_get_current_user();
    $current_user_meta = get_user_meta($current_user->ID);

    ?>
                <div class="row user-profile">
                    <div class="col-lg-3 col-md-3 sidebar-left">
                        <div class="widget member-card">
                            <div class="member-card-header">
                                <a href="#" class="member-card-avatar"><img src="<?= get_template_directory_uri(); ?>/images/upload-sample-image.jpg" alt=""></a>
                                <h3><?php echo esc_attr( $current_user_meta['last_name'][0].' '.$current_user_meta['first_name'][0] );?></h3>
                                <!--<p><i class="fa fa-envelope icon"></i>jdoe@gmail.com</p>-->
                            </div>
                            <div class="member-card-content">
                                <!--<img class="hex" src="images/hexagon.png" alt="">-->
                                <ul>
                                    <li><a href="account"><i class="fa fa-user icon"></i>Profile</a></li>
                                    <li class="active"><a href="my-properties"><i class="fa fa-home icon"></i>My Properties</a></li>
                                    <li><a href="statements"><i class="fa fa-file-excel-o icon"></i>Statements</a></li>
                                    <li><a href="#"><i class="fa fa-reply icon"></i>Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9 col-md-9">
                        <table class="my-properties-list">
                            <tbody><tr>
                                <th>Image</th>
                                <th>Property</th>
                                <th>Plot/Unit No</th>

                                <th>Resell</th>
                                <th>Actions</th>
                            </tr>
<?php
$t=0;
foreach ($property as $item) {
    $t += 1;
    ?>
    <tr>
        <td class="property-img"><a href="property-single.html"><img
                        src="<?= get_template_directory_uri(); ?>/images/property1.jpg" alt=""></a></td>
        <td class="property-title">
            <a href="property-single.html"><?php echo $item->project_id; ?></a><br>
         <p class="property-address"><i class="fa fa-map-marker icon"></i>123 Smith Drive, Baltimore, MD</p>
            <p>Unit Price : <strong>Kshs <?php echo $item->amount; ?></strong></p>
            <p>Deposit : <strong>Kshs <?php echo $item->balance; ?></strong></p>
            <p>Next Installment Date : <strong>2/27/2017</strong></p>
        </td>
        <td><strong><?php echo $item->plot_no; ?></strong></td>

        <td class="property-date"></td>
        <td class="property-post-status">
            <span class="button small alt">View Details</span>
        </td>
    </tr>
    <?php
}
    ?>

                            </tbody></table>

                    </div><!-- end col -->
                </div>
    <?php
    endif;
    ?>
            </div>
        </div>
    </div>




<?php
get_footer();