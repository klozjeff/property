<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/8/2017
 * Time: 4:20 PM
 */

if( !function_exists( 'prcweb_profile_image_upload' ) ) {
    /**
     *  Profile image upload handler
     */
    function prcweb_profile_image_upload( ) {

        // Verify Nonce
        $nonce = $_REQUEST['nonce'];
        if ( ! wp_verify_nonce( $nonce, 'prcweb_allow_upload' ) ) {
            $ajax_response = array(
                'success' => false ,
                'reason' => __( 'Security check failed!', 'prcweb' )
            );
            echo json_encode( $ajax_response );
            die;
        }

        $submitted_file = $_FILES['prcweb_upload_file'];
        $uploaded_image = wp_handle_upload( $submitted_file, array( 'test_form' => false ) );   //Handle PHP uploads in WordPress, sanitizing file names, checking extensions for mime type, and moving the file to the appropriate directory within the uploads directory.

        if ( isset( $uploaded_image['file'] ) ) {
            $file_name          =   basename( $submitted_file['name'] );
            $file_type          =   wp_check_filetype( $uploaded_image['file'] );   //Retrieve the file type from the file name.

            // Prepare an array of post data for the attachment.
            $attachment_details = array(
                'guid'           => $uploaded_image['url'],
                'post_mime_type' => $file_type['type'],
                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $file_name ) ),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            $attach_id      =   wp_insert_attachment( $attachment_details, $uploaded_image['file'] );       // This function inserts an attachment into the media library
            $attach_data    =   wp_generate_attachment_metadata( $attach_id, $uploaded_image['file'] );     // This function generates metadata for an image attachment. It also creates a thumbnail and other intermediate sizes of the image attachment based on the sizes defined
            wp_update_attachment_metadata( $attach_id, $attach_data );                                      // Update metadata for an attachment.

            $thumbnail_url = prcweb_get_profile_image_url( $attach_data ); // returns escaped url

            $ajax_response = array(
                'success'   => true,
                'url' => $thumbnail_url,
                'attachment_id'    => $attach_id
            );
            echo json_encode( $ajax_response );
            die;

        } else {
            $ajax_response = array(
                'success' => false,
                'reason' => __( 'Image upload failed!', 'prcweb' )
            );
            echo json_encode( $ajax_response );
            die;
        }

    }
    add_action( 'wp_ajax_profile_image_upload', 'prcweb_profile_image_upload' );
}


if( !function_exists( 'prcweb_get_profile_image_url' ) ) {
    /**
     * Get thumbnail url based on attachment data
     * @param $attach_data
     * @return string
     */
    function prcweb_get_profile_image_url( $attach_data ) {
        $upload_dir         =   wp_upload_dir();
        $image_path_array   =   explode( '/', $attach_data['file'] );
        $image_path_array   =   array_slice( $image_path_array, 0, count( $image_path_array ) - 1 );
        $image_path         =   implode( '/', $image_path_array );
        $thumbnail_name     =   null;
        if ( isset( $attach_data['sizes']['prcweb-agent-thumbnail'] ) ) {
            $thumbnail_name     =   $attach_data['sizes']['prcweb-agent-thumbnail']['file'];
        } else {
            $thumbnail_name     =   $attach_data['sizes']['thumbnail']['file'];
        }
        return esc_url( $upload_dir['baseurl'] . '/' . $image_path . '/' . $thumbnail_name );
    }
}


if( !function_exists( 'prcweb_update_profile' ) ) {
    /**
     * Edit profile request handler
     */
    function prcweb_update_profile() {

        // Get user info
        $current_user = wp_get_current_user();

        // Array for errors
        $errors = array();

        if( wp_verify_nonce( $_POST['user_profile_nonce'], 'update_user' ) ) {

            // profile-image-id
            // Update profile image
            if ( !empty( $_POST['profile-image-id'] ) ) {
                $profile_image_id = intval( $_POST['profile-image-id'] );
                update_user_meta( $current_user->ID, 'profile_image_id', $profile_image_id );
            } else {
                delete_user_meta( $current_user->ID, 'profile_image_id' );
            }

            // Update first name
            if ( !empty( $_POST['first-name'] ) ) {
                $user_first_name = sanitize_text_field( $_POST['first-name'] );
                update_user_meta( $current_user->ID, 'first_name', $user_first_name );
            } else {
                delete_user_meta( $current_user->ID, 'first_name' );
            }

            // Update last name
            if ( !empty( $_POST['last-name'] ) ) {
                $user_last_name = sanitize_text_field( $_POST['last-name'] );
                update_user_meta( $current_user->ID, 'last_name', $user_last_name );
            } else {
                delete_user_meta( $current_user->ID, 'last_name' );
            }

            // Update display name
            if ( !empty( $_POST['display-name'] ) ) {
                $user_display_name = sanitize_text_field( $_POST['display-name'] );
                $return = wp_update_user( array(
                    'ID' => $current_user->ID,
                    'display_name' => $user_display_name
                ) );
                if ( is_wp_error( $return ) ) {
                    $errors[] = $return->get_error_message();
                }
            }

            // Update user email
            if ( !empty( $_POST['email'] ) ){
                $user_email = is_email( sanitize_email ( $_POST['email'] ) );
                if ( !$user_email )
                    $errors[] = __( 'Provided email address is invalid.', 'prcweb' );
                else {
                    $email_exists = email_exists( $user_email );    // email_exists returns a user id if a user exists against it
                    if( $email_exists ) {
                        if( $email_exists != $current_user->ID ){
                            $errors[] = __('Provided email is already in use by another user. Try a different one.', 'prcweb');
                        } else {
                            // no need to update the email as it is already current user's
                        }
                    } else {
                        $return = wp_update_user( array ('ID' => $current_user->ID, 'user_email' => $user_email ) );
                        if ( is_wp_error( $return ) ) {
                            $errors[] = $return->get_error_message();
                        }
                    }
                }
            }

            // update user description
            if ( !empty( $_POST['id_number'] ) ) {
                $user_description = sanitize_text_field( $_POST['id_number'] );
                update_user_meta( $current_user->ID, 'id_number', $user_description );
            } else {
                delete_user_meta( $current_user->ID, 'id_number' );
            }

            // Update Date of Birth
            if ( !empty( $_POST['DateOfBirth'] ) ) {
                $user_birth_date = sanitize_text_field( $_POST['DateOfBirth'] );
                update_user_meta( $current_user->ID, 'DateOfBirth', $user_birth_date );
            } else {
                delete_user_meta( $current_user->ID, 'DateOfBirth' );
            }

            // Update Box No
            if ( !empty( $_POST['box_number'] ) ) {
                $user_box_number = sanitize_text_field( $_POST['box_number'] );
                update_user_meta( $current_user->ID, 'box_number', $user_box_number );
            } else {
                delete_user_meta( $current_user->ID, 'box_number' );
            }

            // Update Postal Code
            if ( !empty( $_POST['postal_code'] ) ) {
                $user_postal_code = sanitize_text_field( $_POST['postal_code'] );
                update_user_meta( $current_user->ID, 'postal_code', $user_postal_code );
            } else {
                delete_user_meta( $current_user->ID, 'postal_code' );
            }

            // Update mobile number
            if ( !empty( $_POST['mobile-number'] ) ) {
                $user_mobile_number = sanitize_text_field( $_POST['mobile-number'] );
                update_user_meta( $current_user->ID, 'mobile_number', $user_mobile_number );
            } else {
                delete_user_meta( $current_user->ID, 'mobile_number' );
            }

            // Update office number
            if ( !empty( $_POST['office-number'] ) ) {
                $user_office_number = sanitize_text_field( $_POST['office-number'] );
                update_user_meta( $current_user->ID, 'office_number', $user_office_number );
            } else {
                delete_user_meta( $current_user->ID, 'office_number' );
            }

            // Update fax number
            if ( !empty( $_POST['physical_add'] ) ) {
                $user_address = sanitize_text_field( $_POST['physical_add'] );
                update_user_meta( $current_user->ID, 'physical_add', $user_address );
            } else {
                delete_user_meta( $current_user->ID, 'physical_add' );
            }

            // Update Gender
            if ( !empty( $_POST['gender'] ) ) {
                $user_gender = sanitize_text_field( $_POST['gender'] );
                update_user_meta( $current_user->ID, 'gender', $user_gender );
            } else {
                delete_user_meta( $current_user->ID, 'gender' );
            }


            // Update facebook url
            if ( !empty( $_POST['country'] ) ) {
                $country = sanitize_text_field( $_POST['country'] );
                update_user_meta( $current_user->ID, 'country', $country );
            } else {
                delete_user_meta( $current_user->ID, 'country' );
            }

            // Update twitter url
            if ( !empty( $_POST['town'] ) ) {
                $town = sanitize_text_field( $_POST['town'] );
                update_user_meta( $current_user->ID, 'town', $town );
            } else {
                delete_user_meta( $current_user->ID, 'town' );
            }

            // Update Next of Kin Name
            if ( !empty( $_POST['kin_name'] ) ) {
                $kin = sanitize_text_field( $_POST['kin_name'] );
                update_user_meta( $current_user->ID, 'kin_name', $kin );
            } else {
                delete_user_meta( $current_user->ID, 'kin_name' );
            }

            // Update Relationship to Next of Kin
            if ( !empty( $_POST['relationship'] ) ) {
                $relationship = sanitize_text_field( $_POST['relationship']);
                update_user_meta( $current_user->ID, 'relationship', $relationship );
            } else {
                delete_user_meta( $current_user->ID, 'relationship' );
            }
            // Update Contact to Next of Kin
            if ( !empty( $_POST['kin_mobile'] ) ) {
                $kin_mobile = sanitize_text_field( $_POST['kin_mobile']);
                update_user_meta( $current_user->ID, 'kin_mobile', $kin_mobile);
            } else {
                delete_user_meta( $current_user->ID, 'kin_mobile' );
            }
            // Update Id Number to Next of Kin
            if ( !empty( $_POST['kin_id'] ) ) {
                $kin_id = sanitize_text_field( $_POST['kin_id']);
                update_user_meta( $current_user->ID, 'kin_id', $kin_id);
            } else {
                delete_user_meta( $current_user->ID, 'kin_id');
            }
            // todo: add instagram and pin

            // Update user password
            if ( !empty($_POST['pass1'] ) && !empty( $_POST['pass2'] ) ) {
                if ( $_POST['pass1'] == $_POST['pass2'] ) {
                    $return = wp_update_user( array(
                        'ID' => $current_user->ID,
                        'user_pass' => $_POST['pass1']
                    ) );
                    if ( is_wp_error( $return ) ) {
                        $errors[] = $return->get_error_message();
                    }
                } else {
                    $errors[] = __('The passwords you entered do not match.  Your password is not updated.', 'prcweb');
                }
            }

            // if everything is fine
            if ( count( $errors ) == 0 ) {
                //Update Custom Table
                $userDetails=array(
                    'surname' =>sanitize_text_field( $_POST['first-name'] ),
                    'other_names' =>sanitize_text_field( $_POST['last-name'] ),
                    'email'=>sanitize_user( $_POST['email']),
                    'id_number' =>sanitize_text_field( $_POST['id_number']),
                    'mobile_number' =>sanitize_text_field($_POST['mobile-number']),
                    'optional_number' =>sanitize_text_field( $_POST['office-number']),
                    'dob' =>sanitize_text_field($_POST['DateOfBirth']),
                    'postal_address' =>sanitize_text_field( $_POST['box_number']),
                    'zip_code' =>sanitize_text_field( $_POST['postal_code']),
                    'gender' =>sanitize_text_field( $_POST['gender']),
                    'physical_address' =>sanitize_text_field( $_POST['physical_add']),
                    'country' =>sanitize_text_field( $_POST['country']),
                    'town' =>sanitize_text_field( $_POST['town']),
                    'kin_name' =>sanitize_text_field( $_POST['kin_name']),
                    'relationship' =>sanitize_text_field( $_POST['relationship']),
                    'kin_mobile' =>sanitize_text_field( $_POST['kin_mobile']),
                    'kin_id' =>sanitize_text_field( $_POST['kin_id']),
                    'readyToIntegrate' =>1);
                $conditionValue=array(
                    'wpID' => $current_user->ID
                );
                $details = new Prc_Custom_Query( 'prc_customers_information' );
                $details->update($userDetails,$conditionValue);

                //action hook for plugins and extra fields saving
                do_action( 'edit_user_profile_update', $current_user->ID );

                $response = array(
                    'success' => true,
                    'message' => __( 'Customer information is saved successfully!Proceed to Payment', 'prcweb' ),
                    'redirect' => $_POST['redirect_to']
                );
                echo json_encode( $response );
                die;
            }

        } else {
            $errors[] = __('Security check failed!', 'prcweb');
        }

        // in case of errors
        $response = array(
            'success' => false,
            'errors' => $errors
        );
        echo json_encode( $response );
        die;

    }
    add_action( 'wp_ajax_prcweb_update_profile', 'prcweb_update_profile' );    // only for logged in user
}

