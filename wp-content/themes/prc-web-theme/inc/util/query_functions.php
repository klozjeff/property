<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/27/2017
 * Time: 10:34 AM
 */

/** My Properties Query Class*/
class Prc_Custom_Query
{

    private $tableName;



    function __construct($tableName = '')
    {
        // Initiatiate Class
        $this->tableName = $tableName;
    }


    /**
     * Get all from the selected table
     *
     * @param  String $orderBy - Order by column name
     *
     * @return Table result
     */
    public function get_all( $orderBy = NULL )
    {
        global $wpdb;

        $sql = 'SELECT * FROM `'.$this->tableName.'`';

        if(!empty($orderBy))
        {
            $sql .= ' ORDER BY ' . $orderBy;
        }

        $all = $wpdb->get_results($sql);

        return $all;
    }

    /**
     * Get a value by a condition
     *
     * @param  Array $conditionValue - A key value pair of the conditions you want to search on
     * @param  String $condition - A string value for the condition of the query default to equals
     *
     * @return Table result
     */
    public function get_by(array $conditionValue, $condition = '=', $returnSingleRow = FALSE)
    {
        global $wpdb;

        try
        {
            $sql = 'SELECT * FROM `'.$this->tableName.'` WHERE ';

            $conditionCounter = 1;
            foreach ($conditionValue as $field => $value)
            {
                if($conditionCounter > 1)
                {
                    $sql .= ' AND ';
                }

                switch(strtolower($condition))
                {
                    case 'in':
                        if(!is_array($value))
                        {
                            throw new Exception("Values for IN query must be an array.", 1);
                        }

                        $sql .= $wpdb->prepare('`%s` IN (%s)', $field, implode(',', $value));
                        break;

                    default:
                        $sql .= $wpdb->prepare('`'.$field.'` '.$condition.' %s', $value);
                        break;
                }

                $conditionCounter++;
            }

            $result = $wpdb->get_results($sql);

            // As this will always return an array of results if you only want to return one record make $returnSingleRow TRUE
            if(count($result) == 1 && $returnSingleRow)
            {
                $result = $result[0];
            }

            return $result;
        }
        catch(Exception $ex)
        {
            return false;
        }
    }
    public function saveUser(array $data)
    {
        global $wpdb;
        if(empty($data))
        {
            return false;
        }
        $wpdb->insert($this->tableName, $data);
        return $wpdb->insert_id;

    }


    public function update(array $data, array $conditionValue)
    {
        global $wpdb;

        if(empty($data))
        {
            return false;
        }

        $updated = $wpdb->update( $this->tableName, $data, $conditionValue);

        return $updated;
    }

    /*
     * Get all properties/Orders by Specific Client
     * @join Join with prc_posts & property order items
     * @return results
     */
    public function showMyProperties($client)
    {
        global $wpdb;
        $appTable = $wpdb->prefix . $this->tableName;
        $querystr = "SELECT prc_property_order_items.*
             FROM prc_property_order_items 
            INNER JOIN prc_posts ON prc_property_order_items.project_id=prc_posts.post_title
             INNER JOIN prc_property_orders ON prc_property_order_items.order_id=prc_property_orders.id
             WHERE prc_property_orders.client_id=$client AND prc_posts.post_status='publish'
             ORDER BY created_at ASC";
        $query = $wpdb->prepare($querystr, OBJECT_K);
        $properties = $wpdb->get_results($query);
        return $properties;
    }

    /*
     * Get Client No of Bought Products
     */
    public function totalMyProperties($clientID)
    {
        global $wpdb;
        $query="SELECT prc_property_order_items.* 
                FROM prc_property_order_items
                INNER JOIN prc_property_orders ON prc_property_order_items.order_id=prc_property_orders.id
                 WHERE prc_property_orders.client_id=$clientID";
               $query = $wpdb->prepare($query, OBJECT_K);
              $wpdb->get_results($query);
            return $wpdb->num_rows;

    }



}