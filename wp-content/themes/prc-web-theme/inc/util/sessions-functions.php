<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/15/2017
 * Time: 8:44 AM
 */

//require_once( 'C:\xampp\htdocs\property\wp-load.php' );



// DO NOT, FOR ANY REASON, ACCESS DIRECTLY $_SESSION
// ONLY USE A VARIABLE WITHIN $_SESSION (here, "ajjx")
// OTHERWISE THIS MAY ALLOW ANYONE TO TAKE CONTROL OF YOUR INSTALLATION.
if (!function_exists('session_cart_items')):
    function shopping_cart_sessions()
    {
        session_start();
        if (!empty($_POST['plot'])) {
            $itemArray = array(
                $_POST['plot'] => array
                (
                    'property' => $_POST['property'], 'plot' => $_POST['plot'], 'price' => $_POST['price']
                ));
            if (!empty($_SESSION['cart-items'])) {
                if (in_array($_POST['plot'], $_SESSION['cart-items'])) {
                    foreach ($_SESSION['cart-items'] as $k => $v) {
                        if ($_POST['plot'] == $k){
                            $_SESSION["cart-items"][$k]["price"] = $_POST['price'];
                        }
                    }
                }
                else
                {
                    $_SESSION["cart-items"] = array_merge($_SESSION["cart-items"],$itemArray);
                }
            }
            else
            {
                $_SESSION["cart-items"]=$itemArray;
            }

        }
        $_SESSION['property-id'] = $_POST["propertyid"];
 //echo $_SESSION['cart-items']['property'];
    }
endif;
add_action('wp_ajax_shopping_cart_sessions', 'shopping_cart_sessions');
add_action( 'wp_ajax_nopriv_shoppingcartsessions', 'shopping_cart_sessions' );
/**
 * @param $varname
 * @param string $default
 * @return string
 */
function cartItems($varname, $default = '') {
    if (array_key_exists('cart-items', $_SESSION)) {
        if (array_key_exists($varname, $_SESSION['cart-items'])) {
               return $_SESSION['cart-items'][$varname];
    }
    }
    return $default;
}

if(!function_exists('prcweb_save_items'))
{
    function prcweb_save_items()
    {
        $current_user = wp_get_current_user();

        // Array for errors
        $errors = array();
     if(wp_verify_nonce($_POST['save_items_nonce'],'save-items'))
      {
          if(!empty($_POST['plot_number']))
          {
              global $wpdb;
              $saveItemData = array();
              $saveOrder=array(
                  'client_id'=>$current_user->ID,
                  'bill_amount'=>$_POST['total_new_price'],
                  'amount_paid'=>$_POST['total_deposit'],
                  'currency'=>'Kshs',
                  'status'=>1
              );
              $table = $wpdb->prefix . 'property_orders';
              $save_order=wp_insert_single_row($saveOrder,$table,false,'');
              if($save_order > 0)
              {
                  foreach ($_POST['plot_number'] as $key=>$value)
                  {
                      $saveItemData[]=array(
                          'order_id'=>$save_order,
                          'project_id'=>$_POST['property_name'][$key],
                          'payment_period'=>$_POST['payment_plan'][$key],
                          'downpayment_amount'=>number_format($_POST['depositamount'][$key]),
                          'installment_amount'=>$_POST['installmentamount'][$key],
                          'amount'=>$_POST['newprice'][$key],
                          'balance'=>$_POST['newprice'][$key]-number_format($_POST['depositamount'][$key]),
                          'status'=>1,
                          'plot_no'=>$_POST['plot_number'][$key]
                      );
                  }
                  $table_name = $wpdb->prefix . 'property_order_items';
                  $save=wp_insert_rows($saveItemData,$table_name,false,'');
                  // echo $_POST['total_new_price'];
                  //die();
                  //$table_name = $wpdb->prefix . 'property_orders';
                  // $count = array_values(array_fill(0, sizeof($saveItemData), '%s'));
                  // $save = $wpdb->insert($table_name, $saveItemData, $count);
                  if($save)
                  {
                      $response = array(
                          'success' => true,
                          'message' => __( 'Order Details saved successfully!Proceed to Customer Information Document', 'prcweb' ),
                          'redirect' => $_POST['redirect_to']
                      );
                      echo json_encode( $response );
                      die;

                  }
                  else
                  {
                      $response = array(
                          'success' => false,
                          'message' =>$_POST['total_new_price'],
                      );
                      echo json_encode( $response );
                      die;

                  }
              }



          }

      }

    }
    add_action( 'wp_ajax_prcweb_save_items', 'prcweb_save_items' );

}

?>