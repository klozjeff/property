<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 9:03 AM
 */
if( !function_exists( 'prcweb_modal_login' ) ) :
    /**
     * Generate modal login form
     */
    function prcweb_modal_login(){

        if ( ! is_user_logged_in() ) {
            /*
             * include modal login code
             */
            get_template_part( 'partials/members/modal-login');
        }

    }

    add_action( 'wp_footer', 'prcweb_modal_login', 5 );

endif;