<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 9:02 AM
 */

/*
 * Dynamic CSS
 */
require_once( get_template_directory() . '/css/dynamic-css.php' );

if ( ! function_exists( 'prcweb_generate_favicon' ) ) :
    /**
     * Generate favicon
     */
    function prcweb_generate_favicon() {
        if ( !function_exists( 'has_site_icon' ) || !has_site_icon() ) {
            global $prcweb_options;
            if ( !empty( $prcweb_options['prcweb_favicon'] ) && !empty( $prcweb_options['prcweb_favicon']['url'] ) ) {
                ?>
                <!-- favicon -->
                <link rel="shortcut icon" href="<?php echo esc_url( $prcweb_options['prcweb_favicon']['url'] ); ?>"/>
                <?php
            }
        }
    }
    add_action( 'wp_head', 'prcweb_generate_favicon', 5 );
endif;


if ( ! function_exists( 'prcweb_get_banner_image' ) ) :
    /**
     * Get banner image
     * @return bool|string
     */
    function prcweb_get_banner_image() {
        global $post;
        if( isset( $post->ID ) ){
            if ( is_page_template( 'page-templates/home.php' ) ) {
                $banner_image_id = get_post_meta( $post->ID, 'PRC_WEB_homepage_banner_image', true );
            } else {
                $banner_image_id = get_post_meta( $post->ID, 'PRC_WEB_page_banner_image', true );
            }

            if ( $banner_image_id ) {
                return wp_get_attachment_url( $banner_image_id );
            }
        }
        return prcweb_get_default_banner();
    }
endif;


if ( ! function_exists( 'prcweb_get_default_banner' ) ) :
    /**
     * Get default banner
     * @return string
     */
    function prcweb_get_default_banner() {
        global $prcweb_options;
        global $pageName; //$pageName from body.php
        $pagename = get_query_var('pagename');
        $banner_image_path = null;
        if ( !empty( $prcweb_options['prcweb_banner_image'] ) ) {
            $banner_image_path = $prcweb_options['prcweb_banner_image']['url'];
        }

        if (is_page_template( 'page-templates/user-dashboard.php' ) || is_page_template( 'page-templates/my-properties.php' ) ||  $pagename=='blog' ) {      // Home page
return '';
        }
        else
        {
            return empty( $banner_image_path ) ? esc_url( get_template_directory_uri() . '/images/home-cover.jpg' ) : $banner_image_path;
        }
    }
endif;

