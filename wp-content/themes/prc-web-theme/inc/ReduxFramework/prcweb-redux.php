<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 3:07 PM
 */

// load the theme's framework
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname(__FILE__) . '/ReduxCore/framework.php' ) ) {
    require_once( dirname(__FILE__) . '/ReduxCore/framework.php' );
}

require_once ( get_template_directory() . '/inc/theme-options/prcweb-options-config.php' );

