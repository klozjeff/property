<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 6/7/2017
 * Time: 12:29 PM
 */

require_once ( get_template_directory() . '/inc/util/facebookoauth.php' );
class Facebook_Login_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct("facebook_login_widget", "Facebook Login", array("description" => __("Display a Facebook Login Button")));
    }

    public function form( $instance )
    {
        // Check values
        if($instance)
        {
            $title = esc_attr($instance['title']);
            $app_key = $instance['app_key'];
            $app_secret = $instance['app_secret'];
        }
        else
        {
            $title = '';
            $app_key = '';
            $app_secret = '';
        }
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'facebook_login_widget'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('app_key'); ?>"><?php _e('App ID:', 'facebook_login_widget'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('app_key'); ?>" name="<?php echo $this->get_field_name('app_key'); ?>" value="<?php echo $app_key; ?>" />
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('app_secret'); ?>"><?php _e('App Secret:', 'facebook_login_widget'); ?></label>
            <input type="text" class="widefat" id="<?php echo $this->get_field_id('app_secret'); ?>" name="<?php echo $this->get_field_name('app_secret'); ?>" value="<?php echo $app_secret; ?>" />
        </p>

        <?php
    }

    public function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['app_key'] = strip_tags($new_instance['app_key']);
        $instance['app_secret'] = strip_tags($new_instance['app_secret']);

        update_option("facebook_app_id", $new_instance['app_key']);
        update_option("facebook_app_secret", $new_instance['app_secret']);

        return $instance;
    }

    public function widget( $args, $instance )
    {
        extract($args);

        $title = apply_filters('widget_title', $instance['title']);
        echo $before_widget;

        if($title)
        {
            echo $before_title . $title . $after_title ;
        }

        if(is_user_logged_in())
        {
            ?>
            <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout"><input type="button" value="Logout" /></a>
            <?php
        }
        else
        {
            ?>
            <a href="<?php echo site_url() . '/wp-admin/admin-ajax.php?action=facebook_oauth_redirect'; ?>"><input type="button" value="Login Using Facebook" /></a>
            <?php
        }

        echo $after_widget;
    }
}
register_widget("Facebook_Login_Widget");