<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 3:37 PM
 */
 
 
/*
 * Home Options
 */
global $opt_name;
Redux::setSection( $opt_name, array(
    'title' => __('Home', 'prcweb'),
    'id'    => 'home-section',
    'desc'  => __('This section contains options for homepage.', 'prcweb'),
    'fields'=> array(
        array(
            'id'       => 'prcweb_home_module_below_header',
            'type'     => 'button_set',
            'title'    => __( 'What to display below header', 'prcweb' ),
            'options'  => array(
                'banner'        => __( 'Image Banner', 'prcweb' ),
                'google-map'    => __( 'Google Map', 'prcweb' ),
                'slider'        => __( 'Slider', 'prcweb' ),
            ),
            'default'  => 'slider',
        ),
        array(
            'id'       => 'prcweb_slider_type',
            'type'     => 'select',
            'title'    => __( 'Select the type of slider', 'prcweb' ),
            'options'  => array(
                'properties-slider'     => __( 'Properties Slider', 'prcweb' ),
                'properties-slider-two' => __( 'Properties Slider Two', 'prcweb' ),
                'properties-slider-three' => __( 'Properties Slider Three', 'prcweb' ),
                'revolution-slider'     => __( 'Revolution Slider', 'prcweb' ),
            ),
            'default'  => 'properties-slider',
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_module_below_header', '=', 'slider' ),
        ),
        array(
            'id'       => 'prcweb_home_slides_number',
            'type'     => 'select',
            'title'    => __( 'Number of slides to display', 'prcweb' ),
            'options'  => array(
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
                6 => '6',
                7 => '7',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_slider_type', '=', array( 'properties-slider', 'properties-slider-two', 'properties-slider-three' ) ),
        ),
        array(
            'id'       => 'prcweb_slide_button_text',
            'type'     => 'text',
            'title'    => __( 'Slider Button Text', 'prcweb' ),
            'default'  => __( 'More Details', 'prcweb' ),
            'required' => array( 'prcweb_slider_type', '=', array( 'properties-slider-two', 'properties-slider-three' ) ),
        ),
        array(
            'id'        => 'prcweb_revolution_slider_alias',
            'type'      => 'text',
            'title'     => __( 'Revolution Slider Alias', 'prcweb' ),
            'required'  => array( 'prcweb_slider_type', '=', array( 'revolution-slider' ) )
        ),

    ) ) );

/*
 * Layout Sub Section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Layout', 'prcweb'),
    'id'    => 'layout-section',
    'desc'  => __('This section contains homepage layout related options.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(
        array(
            'id'       => 'prcweb_home_sections_width',
            'type'     => 'button_set',
            'title'    => __( 'Home sections width', 'prcweb' ),
            'subtitle' => __( 'Some section can appear in full width.', 'prcweb' ),
            'options'  => array(
                'boxed'   => __( 'Keep Boxed', 'prcweb' ),
                'wide'    => __( 'Allow Full Width', 'prcweb' ),
            ),
            'default'  => 'wide',
        ),
        array(
            'id'       => 'prcweb_home_search',
            'type'     => 'switch',
            'title'    => __( 'Property Search Form', 'prcweb' ),
            'desc'     => __( 'The 1st header variation already includes it, So this applies only with other header variations.', 'prcweb' ),
            'default'  => 0,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
            'required' => array( 'prcweb_header_variation', '=', array( '2', '3' ) ),
        ),
        array(
            'id'      => 'prcweb_home_sections',
            'type'    => 'sorter',
            'title'   => __('Homepage layout manager', 'prcweb'),
            'desc'    => __('Organize homepage sections in the way you want them to appear.', 'prcweb'),
            'options' => array(
                'enabled'  => array(
                    'content'       => __( 'Page Contents', 'prcweb' ),
                ),
                'disabled' => array(
                    'properties'    => __( 'Properties', 'prcweb' ),
                    'featured'      => __( 'Featured', 'prcweb' ),
                    'how-it-works'  => __( 'Why Us', 'prcweb' ),
                    'm-ploti'  => __( 'M-Ploti', 'prcweb' ),
                    'home-testimonials'  => __( 'Testimonials', 'prcweb' ),
                    'partners'      => __( 'Awards', 'prcweb' ),
                    'news'          => __( 'Press Release', 'prcweb' ),
                ),
            )
        ),
    ) ) );


if ( class_exists( 'prcweb_Real_Estate' ) ) {

/*
 * Properties Sub Section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Properties', 'prcweb'),
    'id'    => 'properties-section',
    'desc'  => __('This section contains homepage properties related options.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(

        array(
            'id'        => 'prcweb_home_properties_variation',
            'type'      => 'image_select',
            'title'     => __( 'Properties design variation', 'prcweb' ),
            'subtitle'  => __( 'Select the design variation that you want to use for homepage properties.', 'prcweb' ),
            'options'   => array(
                '1' => array(
                    'title' => __('1st Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/properties-variation-1.png',
                ),
                '2' => array(
                    'title' => __('2nd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/properties-variation-2.png',
                ),
                '3' => array(
                    'title' => __('3rd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/properties-variation-3.png',
                ),
            ),
            'default'   => '1',
        ),
        array(
            'id'       => 'prcweb_home_properties_number_1',
            'type'     => 'select',
            'title'    => __( 'Number of properties to display', 'prcweb' ),
            'options'  => array(
                2  => '2',
                4  => '4',
                6  => '6',
                8  => '8',
                10 => '10',
                12 => '12',
            ),
            'default'  => 4,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_properties_variation', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_home_properties_gallery',
            'type'     => 'switch',
            'title'    => __( 'Multiple Photos', 'prcweb' ),
            'subtitle' => __( '* This can slow website performance', 'prcweb' ),
            'desc'     => __( 'Each property can display multiple photos sliding over each other', 'prcweb' ),
            'default'  => 0,
            'on'       => __('Enabled', 'prcweb'),
            'off'      => __('Disabled', 'prcweb'),
            'required' => array( 'prcweb_home_properties_variation', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_home_properties_gallery_limit',
            'type'     => 'select',
            'title'    => __( 'Max number of photos', 'prcweb' ),
            'options'  => array(
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_properties_gallery', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_home_properties_number_2',
            'type'     => 'select',
            'title'    => __( 'Number of properties to display', 'prcweb' ),
            'options'  => array(
                4  => '4',
                8  => '8',
                12 => '12',
                16 => '16',
                20 => '20',
            ),
            'default'  => 8,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_properties_variation', '=', 2 ),
        ),
        array(
            'id'       => 'prcweb_welcome_title',
            'type'     => 'text',
            'title'    => __( 'Welcome Board Title', 'prcweb' ),
            'default'  => __( 'We are Offering the Best Real Estate Deals', 'prcweb' ),
            'required' => array( 'prcweb_home_properties_variation', '=', 3 ),
        ),
        array(
            'id'           => 'prcweb_welcome_text',
            'type'         => 'textarea',
            'title'        => __( 'Welcome Board Text', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array(),
                    'target' => array(),
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array()
            ), //see http://codex.wordpress.org/Function_Reference/wp_kses
            'required' => array( 'prcweb_home_properties_variation', '=', 3 ),
        ),
        array(
            'id'       => 'prcweb_home_properties_number_3',
            'type'     => 'select',
            'title'    => __( 'Number of properties to display', 'prcweb' ),
            'options'  => array(
                3  => '3',
                6  => '6',
                9  => '9',
                12 => '12',
                15 => '15',
            ),
            'default'  => 8,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_properties_variation', '=', 3 ),
        ),
        array(
            'id'       => 'prcweb_home_properties_order',
            'type'     => 'select',
            'title'    => __( 'Sort Order', 'prcweb' ),
            'options'  => array(
                'price-asc'     => __( 'Sort by Price Low to High', 'prcweb' ),
                'price-desc'    => __( 'Sort by Price High to Low', 'prcweb' ),
                'date-asc'      => __( 'Sort by Date Old to New', 'prcweb' ),
                'date-desc'     => __( 'Sort by Date New to Old', 'prcweb' ),
            ),
            'default'  => 'date-desc',
            'select2'  => array( 'allowClear' => false ),
        ),
        array(
            'id'       => 'prcweb_home_properties_kind',
            'type'     => 'radio',
            'title'    => __( 'Select the kind of properties you want to display', 'prcweb' ),
            'options'  => array(
                'default'   => __( 'Default ( No Filtration )', 'prcweb' ),
                'featured'  => __( 'Featured', 'prcweb' ),
                'selection' => __( 'Based on my selection of Locations, Statuses and Types.', 'prcweb' ),
            ),
            'default'  => 'default'
        ),
        array(
            'id'    =>'prcweb_home_properties_locations',
            'type'  => 'select',
            'title' => __( 'Locations', 'prcweb' ),
            'multi' => true,
            'data'  => 'terms',
            'args'  => array(
                'taxonomies' => 'property-city',
                'args' => array()
            ),
            'required' => array( 'prcweb_home_properties_kind', '=', 'selection' ),
        ),
        array(
            'id'    =>'prcweb_home_properties_statuses',
            'type'  => 'select',
            'title' => __( 'Statuses', 'prcweb' ),
            'multi' => true,
            'data'  => 'terms',
            'args'  => array(
                'taxonomies' => 'property-status',
                'args' => array()
            ),
            'required' => array( 'prcweb_home_properties_kind', '=', 'selection' ),
        ),
        array(
            'id'    =>'prcweb_home_properties_types',
            'type'  => 'select',
            'title' => __( 'Types', 'prcweb' ),
            'multi' => true,
            'data'  => 'terms',
            'args'  => array(
                'taxonomies' => 'property-type',
                'args' => array()
            ),
            'required' => array( 'prcweb_home_properties_kind', '=', 'selection' ),
        ),

    ) ) );

}


/*
 * How it Works Sub Section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Why Us', 'prcweb'),
    'id'    => 'how-it-works-section',
    'desc'  => __('This section contains options related to "Why Us" section.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(

        array(
            'id'       => 'prcweb_hiw_welcome',
            'type'     => 'text',
            'title'    => __( 'Welcome Text', 'prcweb' ),
            'default'  => __( 'Welcome', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_hiw_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'prcweb' ),
            'default'  => __( 'An Awesome title', 'prcweb' ),
        ),
        array(
            'id'           => 'prcweb_hiw_description',
            'type'         => 'textarea',
            'title'        => __( 'Description', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array()
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array(),
            )
        ),
        array(
            'id'        => 'prcweb_hiw_section_bg',
            'type'      => 'switch',
            'title'     => __('Background Image', 'prcweb'),
            'default'   => 1,
            'on'        => __('Enable','prcweb'),
            'off'       => __('Disable','prcweb'),
        ),
        array(
            'id'       => 'prcweb_hiw_bg_image',
            'type'     => 'media',
            'url'      => false,
            'title'    => '',
            'desc' => __( 'Provide a background image for "How it Works" section.', 'prcweb' ),
            'compiler' => 'true',
            'required' => array( 'prcweb_hiw_section_bg', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_hiw_columns',
            'type'     => 'button_set',
            'title'    => __( 'Number of Columns', 'prcweb' ),
            'desc' => __( 'Choose the number of columns to display below basic introduction on "why us" section.', 'prcweb' ),
            'options'  => array(
                '0' => __('None', 'prcweb'),
                '1' => __('1 Column', 'prcweb'),
                '2' => __('2 Columns', 'prcweb'),
                '3' => __('3 Columns', 'prcweb'),
				'4' => __('4 Columns', 'prcweb'),
            ),
            'default'  => '4',
        ),
        array(
            'id'       => 'prcweb_hiw_column_alignment',
            'type'     => 'button_set',
            'title'    => __( 'Content alignment in a column', 'prcweb' ),
            'options'  => array(
                'left' => __('Left', 'prcweb'),
                'center' => __('Center', 'prcweb'),
            ),
            'default'  => 'left',
            'required' => array( 'prcweb_hiw_columns', '=', array( '1', '2', '3', '4' ) ),
        ),

        /*
         * 1st Column
         */
        array(
            'id'       => 'prcweb_hiw_1st_col_icon',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( '1st Column Icon', 'prcweb' ),
            'compiler' => 'true',
            'default'  => array( 'url' => get_template_directory_uri() . '/images/register-icon.svg' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '1', '2', '3','4' ) ),
        ),
        array(
            'id'       => 'prcweb_hiw_1st_col_title',
            'type'     => 'text',
            'title'    => __( '1st Column Title', 'prcweb' ),
            'default'  => __( 'Register', 'prcweb' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '1', '2', '3','4' ) ),
        ),
        array(
            'id'           => 'prcweb_hiw_1st_col_description',
            'type'         => 'textarea',
            'title'        => __( '1st Column Description', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array()
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array(),
            ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '1', '2', '3','4' ) ),
        ),

        /*
         * 2nd Column
         */
        array(
            'id'       => 'prcweb_hiw_2nd_col_icon',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( '2nd Column Icon', 'prcweb' ),
            'compiler' => 'true',
            'default'  => array( 'url' => get_template_directory_uri() . '/images/fill-details-icon.svg' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '2', '3','4' ) ),
        ),
        array(
            'id'       => 'prcweb_hiw_2nd_col_title',
            'type'     => 'text',
            'title'    => __( '2nd Column Title', 'prcweb' ),
            'default'  => __( 'Fill Up Property Details', 'prcweb' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '2', '3','4' ) ),
        ),
        array(
            'id'           => 'prcweb_hiw_2nd_col_description',
            'type'         => 'textarea',
            'title'        => __( '2nd Column Description', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array()
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array(),
            ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '2', '3','4' ) ),
        ),

        /*
         * 3rd Column
         */
        array(
            'id'       => 'prcweb_hiw_3rd_col_icon',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( '3rd Column Icon', 'prcweb' ),
            'compiler' => 'true',
            'default'  => array( 'url' => get_template_directory_uri() . '/images/done-icon.svg' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '3','4' ) ),
        ),
        array(
            'id'       => 'prcweb_hiw_3rd_col_title',
            'type'     => 'text',
            'title'    => __( '3rd Column Title', 'prcweb' ),
            'default'  => __( 'You are Done!', 'prcweb' ),
            'required' => array( 'prcweb_hiw_columns', '=', array(  '3','4' ) ),
        ),
        array(
            'id'           => 'prcweb_hiw_3rd_col_description',
            'type'         => 'textarea',
            'title'        => __( '3rd Column Description', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array()
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array(),
            ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '3','4' ) ),
        ),
		  /*
         * 4th Column
         */
        array(
            'id'       => 'prcweb_hiw_4th_col_icon',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( '4th Column Icon', 'prcweb' ),
            'compiler' => 'true',
            'default'  => array( 'url' => get_template_directory_uri() . '/images/done-icon.svg' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '4' ) ),
        ),
        array(
            'id'       => 'prcweb_hiw_4th_col_title',
            'type'     => 'text',
            'title'    => __( '4th Column Title', 'prcweb' ),
            'default'  => __( 'You are Done!', 'prcweb' ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '4' ) ),
        ),
        array(
            'id'           => 'prcweb_hiw_4th_col_description',
            'type'         => 'textarea',
            'title'        => __( '4th Column Description', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array()
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array(),
            ),
            'required' => array( 'prcweb_hiw_columns', '=', array( '4' ) ),
        ),
       /* array(
            'id'        => 'prcweb_hiw_submit_button',
            'type'      => 'switch',
            'title'     => __('Submit Property Button', 'prcweb'),
            'default'   => 1,
            'on'        => __('Display','prcweb'),
            'off'       => __('Hide','prcweb'),
        ),
        array(
            'id'       => 'prcweb_hiw_button_text',
            'type'     => 'text',
            'title'    => __( 'Button Text', 'prcweb' ),
            'default'  => __( 'Submit Your Property', 'prcweb' ),
            'required' => array( 'prcweb_hiw_submit_button', '=', true ),
        ),
        array(
            'id'       => 'prcweb_hiw_button_url',
            'type'     => 'text',
            'title'    => __( 'Button Target URL', 'prcweb' ),
            'validate' => 'url',
            'default'  => '',
            'required' => array( 'prcweb_hiw_submit_button', '=', true ),
        ),*/

    ) ) );



/*
 * Featured properties sub section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Featured Properties', 'prcweb'),
    'id'    => 'featured-section',
    'desc'  => __('This section contains options related to featured properties on homepage.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(

        array(
            'id'        => 'prcweb_home_featured_variation',
            'type'      => 'image_select',
            'title'     => __( 'Featured properties design variation', 'prcweb' ),
            'subtitle'  => __( 'Select the design variation that you want to use for featured properties.', 'prcweb' ),
            'options'   => array(
                '1' => array(
                    'title' => __('1st Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/featured-variation-1.png',
                ),
                '2' => array(
                    'title' => __('2nd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/featured-variation-2.png',
                ),
                '3' => array(
                    'title' => __('3rd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/featured-variation-3.png',
                ),
            ),
            'default'   => '1',
        ),
        array(
            'id'       => 'prcweb_home_featured_columns',
            'type'     => 'select',
            'title'    => __( 'Number of columns', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
            ),
            'default'  => 4,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_featured_variation', '=', array( 1 , 2 )  ),
        ),
        array(
            'id'       => 'prcweb_home_featured_number_1',
            'type'     => 'select',
            'title'    => __( 'Number of properties', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                6  => '6',
                8  => '8',
                9  => '9',
                10 => '10',
                12 => '12',
            ),
            'default'  => 4,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_featured_variation', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_home_featured_number_2',
            'type'     => 'select',
            'title'    => __( 'Number of properties', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                6  => '6',
                8  => '8',
                9  => '9',
                10 => '10',
                12 => '12',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_featured_variation', '=', 2 ),
        ),
        array(
            'id'       => 'prcweb_home_featured_number_3',
            'type'     => 'select',
            'title'    => __( 'Number of properties', 'prcweb' ),
            'options'  => array(
                2  => '2',
                4  => '4',
                6  => '6',
                8  => '8',
                10 => '10',
                12 => '12',
            ),
            'default'  => 3,
            'required' => array( 'prcweb_home_featured_variation', '=', 3 ),
        ),
        array(
            'id'       => 'prcweb_home_featured_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'prcweb' ),
            'default'  => __( 'Featured Properties', 'prcweb' ),
            'required' => array( 'prcweb_home_featured_variation', '=', array( 2, 3 ) ),
        ),

    ) ) );

/**
 *M-ploti Sub Section
 */

Redux::setSection($opt_name,array(
    'title'=>__('M-Ploti','prcweb'),
    'id'=>'mploti-section',
    'desc'=>__('This section contains options related to M-ploti on Homepage','prcweb'),
    'subsection'=>true,
    'fields'=>array(
        array(
            'id'       => 'prcweb_mploti_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'prcweb' ),
            'default'  => __( 'M-ploti', 'prcweb' ),
        ),
        array(
            'id'           => 'prcweb_mploti_description',
            'type'         => 'textarea',
            'title'        => __( 'Description', 'prcweb' ),
            'desc'         => __( 'Following html tags are allowed: a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => '',
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array()
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array(),
            )
        ),
        array(
            'id'        => 'prcweb_mploti_section_bg',
            'type'      => 'switch',
            'title'     => __('Background Image', 'prcweb'),
            'default'   => 1,
            'on'        => __('Enable','prcweb'),
            'off'       => __('Disable','prcweb'),
        ),
        array(
            'id'       => 'prcweb_mploti_bg_image',
            'type'     => 'media',
            'url'      => false,
            'title'    => '',
            'desc' => __( 'Provide a background image for "M-Ploti" section.', 'prcweb' ),
            'compiler' => 'true',
            'required' => array( 'prcweb_mploti_section_bg', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_mploti_action_image',
            'type'     => 'media',
            'url'      => false,
            'title'    => 'Call to Action Image',
            'desc' => __( 'Provide a call to action image for "M-Ploti" section.', 'prcweb' ),
            'compiler' => 'true',
        ),
    )
));
/*
 * partners sub section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Awards', 'prcweb'),
    'id'    => 'awards-section',
    'desc'  => __('This section contains options related to Awards on homepage.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(
        array(
            'id'       => 'prcweb_home_partners_number',
            'type'     => 'select',
            'title'    => __( 'Number of Awards', 'prcweb' ),
            'options'  => array(
                1  => '1',
                2  => '2',
                3  => '3',
                4  => '4',
                5  => '5',
                6  => '6',
                7  => '7',
                8  => '8',
                9  => '9',
                10 => '10',
            ),
            'default'  => 5,

        ),
        array(
            'id'           => 'prcweb_home_awards_title',
            'type'         => 'textarea',
            'title'        => __( 'Awards Title', 'prcweb' ),
            'desc'         => __( 'You can use span tag to highlight a part of title.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => 'Awards Title <span>With Highlighted</span> Text.',
            'allowed_html' => array(
                'span'     => array(),
            ),
        ),

    ) ) );


/*
 * news sub section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Press Releases', 'prcweb'),
    'id'    => 'news-section',
    'desc'  => __('This section contains options related to press releases on homepage.', 'prcweb' ),
    'subsection' => true,
    'compiler' => true,
    'fields'=> array(
        array(
            'id'       => 'prcweb_home_posts_number',
            'type'     => 'select',
            'title'    => __( 'Number of posts to display', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                5  => '5',
                6  => '6',
                7  => '7',
                8  => '8',
                9  => '9',
                10 => '10',
            ),
            'default'  => 3,

        ),
        array(
            'id'       => 'prcweb_home_news_title',
            'type'     => 'text',
            'title'    => __( 'News Title', 'prcweb' ),
            'default'  => __( 'Latest News', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_home_news_type',
            'type'     => 'button_set',
            'title'    => __( 'Type of News Posts', 'prcweb' ),
            'desc' => __( 'Choose the type of news posts to display on homepage.', 'prcweb' ),
            'options'  => array(
                'recent' => __('Recent', 'prcweb'),
                'category' => __('Based on Categories', 'prcweb'),
                'tag' => __('Based on Tags', 'prcweb'),
            ),
            'default'  => 'recent',
        ),
        array(
            'id'       => 'prcweb_home_news_category',
            'type'     => 'select',
	        'multi'    => true,
            'data'     => 'categories',
            'title'    => __( 'Select Categories', 'prcweb' ),
            'required' => array( 'prcweb_home_news_type', '=', 'category' ),
        ),
        array(
            'id'       => 'prcweb_home_news_tag',
            'type'     => 'select',
            'multi'    => true,
            'data'     => 'tags',
            'title'    => __( 'Select Tags', 'prcweb' ),
            'required' => array( 'prcweb_home_news_type', '=', 'tag' ),
        ),
        array(
            'id'       => 'prcweb_home_news_link_text',
            'type'     => 'text',
            'title'    => __( 'More Link Text', 'prcweb' ),
            'default'  => __( 'More', 'prcweb' ),
        ),

    ) ) );