<?php
/*
 * Header Options
 */
global $opt_name;
Redux::setSection( $opt_name, array(
    'title' => __('Header', 'prcweb'),
    'id'    => 'header-section',
    'desc'  => __('This section contains options for header.', 'prcweb'),
    'fields'=> array(

        array(
            'id'        => 'prcweb_header_variation',
            'type'      => 'image_select',
            'title'     => __( 'Header Design Variation', 'prcweb' ),
            'subtitle'  => __( 'Select the design variation that you want to use for site header.', 'prcweb' ),
            'options'   => array(
                '1' => array(
                    'title' => __('1st Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/header-variation-1.png',
                ),
                '2' => array(
                    'title' => __('2nd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/header-variation-2.png',
                ),
                '3' => array(
                    'title' => __('3rd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/header-variation-3.png',
                )
            ),
            'default'   => '1',
        ),
        array(
            'id'        => 'prcweb_sticky_header',
            'type'      => 'switch',
            'title'     => __('Sticky Header', 'prcweb'),
            'desc'     => __('This feature only works above 992px screen size.', 'prcweb'),
            'default'   => 0,
            'on'        => __('Enable','prcweb'),
            'off'       => __('Disable','prcweb'),
        ),
        array(
            'id'        => 'prcweb_header_menu_title',
            'type'      => 'text',
            'title'     => __( 'Menu Button Title', 'prcweb' ),
            'default'   => __( 'Menu', 'prcweb' ),
            'required' => array( 'prcweb_header_variation', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_favicon',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Favicon', 'prcweb' ),
            'subtitle' => __( 'Upload your website favicon.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Logo', 'prcweb' ),
            'subtitle' => __( 'Upload logo image for your Website. Otherwise site title will be displayed in place of logo.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_land_logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Land Logo', 'prcweb' ),
            'subtitle' => __( 'Upload logo image for custom Land Properties Page. Otherwise site title will be displayed in place of logo.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_housing_logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Housing Logo', 'prcweb' ),
            'subtitle' => __( 'Upload logo image for custom Housing Properties Page. Otherwise site title will be displayed in place of logo.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_kilimo_logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Kilimo Biashara Logo', 'prcweb' ),
            'subtitle' => __( 'Upload logo image for custom Kilimo Biashara Page. Otherwise site title will be displayed in place of logo.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_mploti_logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'M-Ploti Logo', 'prcweb' ),
            'subtitle' => __( 'Upload logo image for custom M-Ploti Page. Otherwise site title will be displayed in place of logo.', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_header_email',
            'type'      => 'text',
            'title'     => __( 'Email Address', 'prcweb' ),
            'default'   => '',
            'validate'  => 'email',
            'required'  => array( 'prcweb_header_variation', '=', 3 ),
        ),
        array(
            'id'        => 'prcweb_header_phone',
            'type'      => 'text',
            'title'     => __( 'Phone Number', 'prcweb' ),
            'default'   => '',
        ),
		 array(
            'id'        => 'prcweb_header_address',
            'type'      => 'text',
            'title'     => __( 'Address/location', 'prcweb' ),
            'default'   => '',
        ),
        array(
            'id'       => 'prcweb_facebook_url',
            'type'     => 'text',
            'title'    => __( 'Facebook URL', 'prcweb' ),
            'validate' => 'url',
            'default'  => '',
        ),
        array(
            'id'       => 'prcweb_twitter_url',
            'type'     => 'text',
            'title'    => __( 'Twitter URL', 'prcweb' ),
            'validate' => 'url',
            'default'  => '',
        ),
        array(
            'id'       => 'prcweb_google_url',
            'type'     => 'text',
            'title'    => __( 'Google Plus URL', 'prcweb' ),
            'validate' => 'url',
            'default'  => '',
        ),
        array(
            'id'       => 'prcweb_banner_image',
            'type'     => 'media',
            'url'      => false,
            'title'    => __('Banner Image', 'prcweb'),
            'desc'     => __('Banner image should have minimum width of 2000px and minimum height of 320px.', 'prcweb'),
            'subtitle' => __('This banner image will be displayed on all the pages where banner image is not overridden by page specific banner settings.', 'prcweb'),
        ),
        array(
            'id'        => 'prcweb_display_wpml_flags',
            'type'      => 'switch',
            'title'     => __('WPML Language Switcher Flags', 'prcweb'),
            'subtitle'     => __('Do you want to display WPML language switcher flags in header top bar ?', 'prcweb'),
            'desc'     => __('This option only works if WPML plugin is installed.', 'prcweb'),
            'default'   => 1,
            'on'        => __('Display','prcweb'),
            'off'       => __('Hide','prcweb'),
        ),
        array(
            'id'        => 'prcweb_page_loader',
            'type'      => 'switch',
            'title'     => __('Page Loader', 'prcweb'),
            'desc'     => __('You can enable or disable page loader.', 'prcweb'),
            'default'   => false,
            'on'        => __('Enable','prcweb'),
            'off'       => __('Disable','prcweb'),
        ),
        array(
            'id'       => 'prcweb_page_loader_gif',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Page Loader Gif', 'prcweb' ),
            'desc'     => __( 'You can upload your page loader gif here or default one will be displayed.', 'prcweb' ),
            'required' => array( 'prcweb_page_loader', '=', 1 ),
        ),
        array(
            'id'        => 'prcweb_quick_js',
            'type'      => 'ace_editor',
            'title'     => __('Quick JavaScript', 'prcweb'),
            'desc'  => __('You can paste your JavaScript code here.', 'prcweb'),
            'mode'      => 'javascript',
            'theme'     => 'chrome',
        ),

    ) ) );
