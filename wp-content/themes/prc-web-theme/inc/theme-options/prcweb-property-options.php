<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 3:51 PM
 */

/*
 * Property Details Options
 */
global $opt_name;
Redux::setSection( $opt_name, array (
    'title' => __('Property Detail', 'prcweb'),
    'id'    => 'property-detail-section',
    'desc'  => __('This section contains options for property detail page.', 'prcweb'),
    'fields'=> array (

        array(
            'id'       => 'prcweb_property_header_variation',
            'type'     => 'radio',
            'title'    => __( 'Property Header Design Variation', 'prcweb' ),
            'options'  => array(
                '1' => __( 'Variation One - Simple slider with basic information on right side of slider.', 'prcweb' ),
                '2' => __( 'Variation Two - Thumbnail slider with basic information below slider.', 'prcweb' ),
                '3' => __( 'Variation Three - Horizontally scrollable slider with basic information below slider.', 'prcweb' ),
            ),
            'default'  => '1',
        ),
		
		/*
		 * Breadcrumbs
		*/
		array(
            'id'       => 'prcweb_property_breadcrumbs',
            'type'     => 'switch',
            'title'    => __( 'Property Breadcrumbs', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
		
        /*
         * Description
         */
        array(
            'id'       => 'prcweb_property_description',
            'type'     => 'switch',
            'title'    => __( 'Property Description', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_desc_title',
            'type'      => 'text',
            'title'     => __( 'Description Title', 'prcweb' ),
            'default'   => __( 'Description', 'prcweb' ),
            'required' => array( 'prcweb_property_description', '=', 1 ),
        ),


        /*
         * Additional Details
         */
        array(
            'id'       => 'prcweb_property_details',
            'type'     => 'switch',
            'title'    => __( 'Property Additional Details', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_details_title',
            'type'      => 'text',
            'title'     => __( 'Additional Details Title', 'prcweb' ),
            'default'   => __( 'Additional Details', 'prcweb' ),
            'required' => array( 'prcweb_property_details', '=', 1 ),
        ),


        /*
         * Features
         */
        array(
            'id'       => 'prcweb_property_features',
            'type'     => 'switch',
            'title'    => __( 'Property Features', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_features_title',
            'type'      => 'text',
            'title'     => __( 'Features Title', 'prcweb' ),
            'default'   => __( 'Features', 'prcweb' ),
            'required' => array( 'prcweb_property_features', '=', 1 ),
        ),

        /*
         * Floor Plans
         */
        array(
            'id'       => 'prcweb_property_floor_plans',
            'type'     => 'switch',
            'title'    => __( 'Property Floor Plans', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_floor_plans_title',
            'type'      => 'text',
            'title'     => __( 'Floor Plans Title', 'prcweb' ),
            'default'   => __( 'Floor Plans', 'prcweb' ),
            'required' => array( 'prcweb_property_floor_plans', '=', 1 ),
        ),

        /*
         * Video
         */
        array(
            'id'       => 'prcweb_property_video',
            'type'     => 'switch',
            'title'    => __( 'Property Video', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_video_title',
            'type'      => 'text',
            'title'     => __( 'Video Title', 'prcweb' ),
            'default'   => __( 'Virtual Tour', 'prcweb' ),
            'required' => array( 'prcweb_property_video', '=', 1 ),
        ),


        /*
         * Map
         */
        array(
            'id'       => 'prcweb_property_map',
            'type'     => 'switch',
            'title'    => __( 'Property Map', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_map_title',
            'type'      => 'text',
            'title'     => __( 'Map Title', 'prcweb' ),
            'default'   => __( 'Location', 'prcweb' ),
            'required' => array( 'prcweb_property_map', '=', 1 ),
        ),


        /*
         * Attachments
         */
        array(
            'id'       => 'prcweb_property_attachments',
            'type'     => 'switch',
            'title'    => __( 'Property Attachments', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_attachments_title',
            'type'      => 'text',
            'title'     => __( 'Attachments Title', 'prcweb' ),
            'default'   => __( 'Attachments', 'prcweb' ),
            'required' => array( 'prcweb_property_attachments', '=', 1 ),
        ),


        /*
         * Share
         */
        array(
            'id'       => 'prcweb_property_share',
            'type'     => 'switch',
            'title'    => __( 'Share This Property', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_share_title',
            'type'      => 'text',
            'title'     => __( 'Share This Property Title', 'prcweb' ),
            'default'   => __( 'Share This Property', 'prcweb' ),
            'required' => array( 'prcweb_property_share', '=', 1 ),
        ),


        /*
         * Children Properties
         */
        array(
            'id'       => 'prcweb_children_properties',
            'type'     => 'switch',
            'title'    => __( 'Children Properties', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_property_children_title',
            'type'      => 'text',
            'title'     => __( 'Children Properties Title', 'prcweb' ),
            'default'   => __( 'Sub Properties', 'prcweb' ),
            'required' => array( 'prcweb_children_properties', '=', 1 ),
        ),


        /*
         * Property Comments
         */
        array(
            'id'       => 'prcweb_property_comments',
            'type'     => 'switch',
            'title'    => __( 'Property Comments', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),


        /*
         * Agent
         */
        array(
            'id'       => 'prcweb_property_agent',
            'type'     => 'switch',
            'title'    => __( 'Agent Information', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_agent_contact_form',
            'type'     => 'switch',
            'title'    => __( 'Agent Contact Form', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
            'required' => array( 'prcweb_property_agent', '=', 1 ),
        ),
        array(
            'id'        => 'prcweb_agent_contact_form_title',
            'type'      => 'text',
            'title'     => __( 'Agent Contact Form Title', 'prcweb' ),
            'default'   => __( 'Contact Agent', 'prcweb' ),
            'required' => array( 'prcweb_agent_contact_form', '=', 1 ),
        ),


        /*
         * Similar Properties
         */
        array(
            'id'       => 'prcweb_similar_properties',
            'type'     => 'switch',
            'title'    => __( 'Similar Properties', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_similar_properties_title',
            'type'      => 'text',
            'title'     => __( 'Similar Properties Title', 'prcweb' ),
            'default'   => __( 'Similar Properties', 'prcweb' ),
            'required' => array( 'prcweb_similar_properties', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_similar_properties_number',
            'type'     => 'select',
            'title'    => __( 'Maximum number of similar properties', 'prcweb' ),
            'options'  => array(
                1  => '1',
                2  => '2',
                3  => '3',
                4  => '4',
                5  => '5',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_similar_properties', '=', 1 ),
        ),
    )
) );
