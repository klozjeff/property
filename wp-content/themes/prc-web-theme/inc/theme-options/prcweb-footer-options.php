<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 4:09 PM
 */
 
/*
 * Footer Options
 */
global $opt_name;
Redux::setSection( $opt_name, array(
    'title' => __( 'Footer', 'prcweb' ),
    'id'    => 'footer-section',
    'desc'  => __( 'This section contains options for footer.', 'prcweb' ),
    'fields'=> array(

        array(
            'id'       => 'prcweb_footer_logo',
            'type'     => 'media',
            'url'      => false,
            'title'    => __( 'Footer Logo', 'prcweb' ),
            'subtitle' => __( 'Upload footer logo.', 'prcweb' ),
        ),
        array(
            'id'           => 'prcweb_copyright_html',
            'type'         => 'textarea',
            'title'        => __( 'Footer Copyright Text', 'prcweb' ),
            'desc'         => __( 'Allowed html tags are a, br, em and strong.', 'prcweb' ),
            'validate'     => 'html_custom',
            'default'      => sprintf( '&copy; Copyright 2015 All rights reserved by <a href="%1$s" target="_blank">prcweb Themes</a>', esc_url( 'http://themeforest.net/user/prcwebThemes' ) ),
            'allowed_html' => array(
                'a'      => array(
                    'href'  => array(),
                    'title' => array(),
                    'target' => array(),
                ),
                'br'     => array(),
                'em'     => array(),
                'strong' => array()
            ), //see http://codex.wordpress.org/Function_Reference/wp_kses
        ),

    ) ) );