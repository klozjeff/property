<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 3:37 PM
 */
 
 
/*
 * Home Options
 */
global $opt_name;
Redux::setSection( $opt_name, array(
    'title' => __('Brand(Land & Housing)', 'prcweb'),
    'id'    => 'brand-section',
    'desc'  => __('This section contains options for Brand Page.', 'prcweb'),
    'fields'=> array(
        array(
            'id'       => 'prcweb_brand_module_below_header',
            'type'     => 'button_set',
            'title'    => __( 'What to display below header', 'prcweb' ),
            'options'  => array(
                'banner'        => __( 'Image Banner', 'prcweb' ),
                'google-map'    => __( 'Google Map', 'prcweb' ),
                'slider'        => __( 'Slider', 'prcweb' ),
            ),
            'default'  => 'slider',
        ),
        array(
            'id'       => 'prcweb_brand_slider_type',
            'type'     => 'select',
            'title'    => __( 'Select the type of slider', 'prcweb' ),
            'options'  => array(
                'properties-slider'     => __( 'Properties Slider', 'prcweb' ),
                'properties-slider-two' => __( 'Properties Slider Two', 'prcweb' ),
                'properties-slider-three' => __( 'Properties Slider Three', 'prcweb' ),
                'revolution-slider'     => __( 'Revolution Slider', 'prcweb' ),
            ),
            'default'  => 'properties-slider',
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_home_module_below_header', '=', 'slider' ),
        ),
        array(
            'id'       => 'prcweb_brand_slides_number',
            'type'     => 'select',
            'title'    => __( 'Number of slides to display', 'prcweb' ),
            'options'  => array(
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
                6 => '6',
                7 => '7',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_slider_type', '=', array( 'properties-slider', 'properties-slider-two', 'properties-slider-three' ) ),
        ),
        array(
            'id'       => 'prcweb_slide_button_text',
            'type'     => 'text',
            'title'    => __( 'Slider Button Text', 'prcweb' ),
            'default'  => __( 'More Details', 'prcweb' ),
            'required' => array( 'prcweb_slider_type', '=', array( 'properties-slider-two', 'properties-slider-three' ) ),
        ),
        array(
            'id'        => 'prcweb_revolution_slider_alias',
            'type'      => 'text',
            'title'     => __( 'Revolution Slider Alias', 'prcweb' ),
            'required'  => array( 'prcweb_slider_type', '=', array( 'revolution-slider' ) )
        ),

    ) ) );

/*
 * Layout Sub Section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Layout', 'prcweb'),
    'id'    => 'brand-layout-section',
    'desc'  => __('This section contains Brand Page layout related options.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(
        array(
            'id'       => 'prcweb_brand_sections_width',
            'type'     => 'button_set',
            'title'    => __( 'Brand Page(Land & Housing) sections width', 'prcweb' ),
            'subtitle' => __( 'Some section can appear in full width.', 'prcweb' ),
            'options'  => array(
                'boxed'   => __( 'Keep Boxed', 'prcweb' ),
                'wide'    => __( 'Allow Full Width', 'prcweb' ),
            ),
            'default'  => 'wide',
        ),
        array(
            'id'       => 'prcweb_brand_search',
            'type'     => 'switch',
            'title'    => __( 'Property Search Form', 'prcweb' ),
            'desc'     => __( 'The 1st header variation already includes it, So this applies only with other header variations.', 'prcweb' ),
            'default'  => 0,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
            'required' => array( 'prcweb_header_variation', '=', array( '2', '3' ) ),
        ),
        array(
            'id'      => 'prcweb_brand_sections',
            'type'    => 'sorter',
            'title'   => __('Brand Pages(Land & Housing) layout manager', 'prcweb'),
            'desc'    => __('Organize BrandPage(Land & Housing) sections in the way you want them to appear.', 'prcweb'),
            'options' => array(
                'enabled'  => array(
                    'content'       => __( 'Page Contents', 'prcweb' ),
                ),
                'disabled' => array(
                    'sub-nav'    => __( 'Sub Nav', 'prcweb' ),
                    'featured'      => __( 'Featured', 'prcweb' ),
                    'press-release'  => __( 'Press Release', 'prcweb' ),
                    'testimonials'  => __( 'Testimonials', 'prcweb' ),
                    
                ),
            )
        ),
    ) ) );

/*
 * Featured properties sub section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Featured Properties', 'prcweb'),
    'id'    => 'brand-featured-section',
    'desc'  => __('This section contains options related to featured properties on homepage.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(

        array(
            'id'        => 'prcweb_brand_featured_variation',
            'type'      => 'image_select',
            'title'     => __( 'Featured properties design variation', 'prcweb' ),
            'subtitle'  => __( 'Select the design variation that you want to use for featured properties.', 'prcweb' ),
            'options'   => array(
                '1' => array(
                    'title' => __('1st Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/featured-variation-1.png',
                ),
                '2' => array(
                    'title' => __('2nd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/featured-variation-2.png',
                ),
                '3' => array(
                    'title' => __('3rd Variation', 'prcweb'),
                    'img' => get_template_directory_uri() . '/inc/theme-options/images/featured-variation-3.png',
                ),
            ),
            'default'   => '1',
        ),
        array(
            'id'       => 'prcweb_brand_featured_columns',
            'type'     => 'select',
            'title'    => __( 'Number of columns', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
            ),
            'default'  => 4,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_brand_featured_variation', '=', array( 1 , 2 )  ),
        ),
        array(
            'id'       => 'prcweb_brand_featured_number_1',
            'type'     => 'select',
            'title'    => __( 'Number of properties', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                6  => '6',
                8  => '8',
                9  => '9',
                10 => '10',
                12 => '12',
            ),
            'default'  => 4,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_brand_featured_variation', '=', 1 ),
        ),
        array(
            'id'       => 'prcweb_brand_featured_number_2',
            'type'     => 'select',
            'title'    => __( 'Number of properties', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                6  => '6',
                8  => '8',
                9  => '9',
                10 => '10',
                12 => '12',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_brand_featured_variation', '=', 2 ),
        ),
        array(
            'id'       => 'prcweb_brand_featured_number_3',
            'type'     => 'select',
            'title'    => __( 'Number of properties', 'prcweb' ),
            'options'  => array(
                2  => '2',
                4  => '4',
                6  => '6',
                8  => '8',
                10 => '10',
                12 => '12',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_brand_featured_variation', '=', 3 ),
        ),
        array(
            'id'       => 'prcweb_brand_featured_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'prcweb' ),
            'default'  => __( 'Featured Properties', 'prcweb' ),
            'required' => array( 'prcweb_brand_featured_variation', '=', array( 2, 3 ) ),
        ),

    ) ) );
/*
 * Testimonials sub section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Testimonials', 'prcweb'),
    'id'    => 'testimonials-section',
    'desc'  => __('This section contains options related to Testimonials on homepage.', 'prcweb'),
    'subsection' => true,
    'fields'=> array(
        array(
            'id'       => 'prcweb_testimonials_number',
            'type'     => 'select',
            'title'    => __( 'Number of Testimonials to Display', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                6  => '6',
                8  => '8',
                9  => '9',
                10 => '10',
                12 => '12',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),

        ),
        array(
            'id'       => 'prcweb_home_testimonials_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'prcweb' ),
            'default'  => __( 'Testimonials', 'prcweb' ),
        ),
        array(
            'id'        => 'prcweb_testimonials_section_bg',
            'type'      => 'switch',
            'title'     => __('Background Image', 'prcweb'),
            'default'   => 1,
            'on'        => __('Enable','prcweb'),
            'off'       => __('Disable','prcweb'),
        ),
        array(
            'id'       => 'prcweb_testimonials_bg_image',
            'type'     => 'media',
            'url'      => false,
            'title'    => '',
            'desc' => __( 'Provide a background image for "Testimonials" section.', 'prcweb' ),
            'compiler' => 'true',
            'required' => array( 'prcweb_testimonials_section_bg', '=', 1 ),
        ),

    ) ) );
/*
 * news sub section
 */
Redux::setSection( $opt_name, array(
    'title' => __( 'Press Releases', 'prcweb'),
    'id'    => 'brand-news-section',
    'desc'  => __('This section contains options related to press releases on Brand pages.', 'prcweb' ),
    'subsection' => true,
    'fields'=> array(
        array(
            'id'       => 'prcweb_brand_posts_number',
            'type'     => 'select',
            'title'    => __( 'Number of posts to display', 'prcweb' ),
            'options'  => array(
                2  => '2',
                3  => '3',
                4  => '4',
                5  => '5',
                6  => '6',
                7  => '7',
                8  => '8',
                9  => '9',
                10 => '10',
            ),
            'default'  => 3,
            'select2'  => array( 'allowClear' => false ),
        ),
        array(
            'id'       => 'prcweb_brand_news_title',
            'type'     => 'text',
            'title'    => __( 'News Title', 'prcweb' ),
            'default'  => __( 'Latest News', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_brand_news_type',
            'type'     => 'button_set',
            'title'    => __( 'Type of News Posts', 'prcweb' ),
            'desc' => __( 'Choose the type of news posts to display on BrandPage(Land & Housing).', 'prcweb' ),
            'options'  => array(
                'reprcwebnt' => __('Reprcwebnt', 'prcweb'),
                'category' => __('Based on Categories', 'prcweb'),
                'tag' => __('Based on Tags', 'prcweb'),
            ),
            'default'  => 'reprcwebnt',
        ),
        array(
            'id'       => 'prcweb_brand_news_category',
            'type'     => 'select',
	        'multi'    => true,
            'data'     => 'categories',
            'title'    => __( 'Select Categories', 'prcweb' ),
            'required' => array( 'prcweb_brand_news_type', '=', 'category' ),
        ),
        array(
            'id'       => 'prcweb_brand_news_tag',
            'type'     => 'select',
            'multi'    => true,
            'data'     => 'tags',
            'title'    => __( 'Select Tags', 'prcweb' ),
            'required' => array( 'prcweb_brand_news_type', '=', 'tag' ),
        ),
        array(
            'id'       => 'prcweb_brand_news_link_text',
            'type'     => 'text',
            'title'    => __( 'More Link Text', 'prcweb' ),
            'default'  => __( 'More', 'prcweb' ),
        ),

    ) ) );