<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 3:54 PM
 */

/*
 * Members Options
 */
global $opt_name;
Redux::setSection( $opt_name, array(
    'title' => __( 'Members', 'prcweb' ),
    'id'    => 'memebers-section',
    'desc'  => __( 'This section contains options related to registered users.', 'prcweb' ),
    'fields'=> array(

        array(
            'id'       => 'prcweb_header_user_nav',
            'type'     => 'switch',
            'title'    => __( 'User Navigation in Header', 'prcweb' ),
            'default'  => 1,
            'on'       => __( 'Show', 'prcweb' ),
            'off'      => __( 'Hide', 'prcweb' ),
        ),

        array(
            'id'       => 'prcweb_restricted_level',
            'type'     => 'radio',
            'title'    => __( 'Restrict Admin Access', 'prcweb' ),
            "desc"     => __( 'Restrict admin access to any user level equal to or below the selected user level.', 'prcweb' ),
            'options'  => array(
                '0' => __( 'Subscriber ( Level 0 )', 'prcweb' ),
                '1' => __( 'Contributor ( Level 1 )', 'prcweb' ),
                '2' => __( 'Author ( Level 2 )', 'prcweb' ),
                // '7' => __( 'Editor ( Level 7 )', 'prcweb' ),
            ),
            'default'  => '0',
        ),


         // Commented out this as I have change the approach to register by sending password as part of email during registration.
       /* array(
            'id'       => 'prcweb_new_user_notification',
            'type'     => 'switch',
            'title'    => __( 'Registration Email Notification to Admin and Newly Registered User', 'prcweb' ),
            'default'  => 1,
            'on'       => __('Enabled', 'prcweb'),
            'off'      => __('Disabled', 'prcweb'),
        ),
*/

        /*
         * Profile Page
         */
        array(
            'id'       => 'prcweb_edit_profile_page',
            'type'     => 'select',
            'data'     => 'pages',
            'title'    => __( 'Edit Profile Page', 'prcweb' ),
            'desc'     => __( 'Make sure the selected page is using Edit Profile template.', 'prcweb' ),
        ),


        /*
         * Favorites Page
         */
        array(
            'id'       => 'prcweb_favorites_page',
            'type'     => 'select',
            'data'     => 'pages',
            'title'    => __( 'Favorites Page', 'prcweb' ),
            'subtitle' => __( 'Select a page to display favorite properties.', 'prcweb' ),
            'desc'     => __( 'Make sure the selected page is using Favorites template.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_favorites_properties_number',
            'type'     => 'select',
            'title'    => __( 'Number of Properties on Favorites Page', 'prcweb' ),
            'options'  => array(
                3 => '3',
                6 => '6',
                9 => '9',
                12 => '12',
                15 => '15',
            ),
            'default'  => 6,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_favorites_page', '>', 0 )
        ),


        /*
         * Properties Page
         */
        array(
            'id'       => 'prcweb_my_properties_page',
            'type'     => 'select',
            'data'     => 'pages',
            'title'    => __( 'My Properties Page', 'prcweb' ),
            'desc'     => __( 'Make sure the selected page is using My Properties template.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_my_properties_number',
            'type'     => 'select',
            'title'    => __( 'Number of Properties on My Properties Page', 'prcweb' ),
            'options'  => array(
                1 => '1',
                2 => '2',
                3 => '3',
                4 => '4',
                5 => '5',
                6 => '6',
                7 => '7',
                8 => '8',
                9 => '9',
                10 => '10',
            ),
            'default'  => 5,
            'select2'  => array( 'allowClear' => false ),
            'required' => array( 'prcweb_my_properties_page', '>', 0 )
        ),


        /*
         * Submit Property
         */
        array(
            'id'       => 'prcweb_submit_property_page',
            'type'     => 'select',
            'data'     => 'pages',
            'title'    => __( 'Sell land to us Property Page', 'prcweb' ),
            'desc'     => __( 'Make sure the selected page is using Submit Property template.', 'prcweb' ),
        ),
        array(
            'id'       => 'prcweb_default_submit_status',
            'type'     => 'button_set',
            'title'    => __( 'Default Status of Submitted Property', 'prcweb' ),
            'options'  => array(
                'pending' => __( 'Pending for Review', 'prcweb' ),
                'publish' => __( 'Publish', 'prcweb' ),
            ),
            'default'  => 'pending',
            'required' => array( 'prcweb_submit_property_page', '>', 0 )
        ),
        array(
            'id'       => 'prcweb_submit_address',
            'type'     => 'text',
            'title'    => __( 'Default Address for Property Submit Form', 'prcweb' ),
            'default'  => '15421 Southwest 39th Terrace, Miami, FL 33185, USA',
            'required' => array( 'prcweb_submit_property_page', '>', 0 )
        ),
        array(
            'id'       => 'prcweb_submit_location_coordinates',
            'type'     => 'text',
            'title'    => __( 'Default Location Coordinates for Property Submit Map', 'prcweb' ),
            'desc'     => sprintf ( 'You can use <a href="%1$s" target="_blank">latlong.net</a> or <a href="%2$s" target="_blank">itouchmap.com</a> to get Latitude and longitude of your desired location.', esc_url( 'http://www.latlong.net/' ), esc_url( 'http://itouchmap.com/latlong.html' ) ),
            'default'  => '25.7308309,-80.44414899999998',
            'required' => array( 'prcweb_submit_property_page', '>', 0 )
        ),
        array(
            'id'       => 'prcweb_submit_notice_email',
            'type'     => 'text',
            'title'    => __( 'Email for Property Submit Notice', 'prcweb' ),
            'desc'     => __( 'This email address will receive a notice whenever a user submits a property.', 'prcweb' ),
            'validate' => 'email',
            'required' => array( 'prcweb_submit_property_page', '>', 0 )
        ),
        array(
            'id'       => 'prcweb_submit_message_to_reviewer',
            'type'     => 'switch',
            'title'    => __( 'Message for Reviewer on Property Submit Form', 'prcweb' ),
            'default'  => 0,
            'on'       => __('Enabled', 'prcweb'),
            'off'      => __('Disabled', 'prcweb'),
            'required' => array(
                array( 'prcweb_submit_property_page', '>', 0 ),
                array( 'prcweb_submit_notice_email', '!=', '' ),
            ),
        ),

    ) ) );