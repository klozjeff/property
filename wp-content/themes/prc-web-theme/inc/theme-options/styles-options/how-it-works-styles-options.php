<?php
/*
 * How it Works Styles Options
 */
global $opt_name;

Redux::setSection( $opt_name, array(
    'title' => __( 'Why Us', 'prcweb'),
    'id'    => 'how-it-works-styles',
    'desc'  => __('This section contains how it works section styles options.', 'prcweb' ),
    'subsection' => true,
    'fields'=> array (

        array(
            'id'        => 'prcweb_hiw_background_color',
            'type'      => 'color',
            'title'     => __( 'Background Color', 'prcweb' ),
            'desc'      => 'default: #202020',
            'default'   => '#202020',
            'validate'  => 'color',
            'transparent' => false,
        ),

        array(
            'id'        => 'prcweb_hiw_overlay_background_color',
            'type'      => 'color_rgba',
            'mode'      => 'background-color',
            'output'    => array( '.submit-property-one:before' ),
            'title'     => __( 'Overlay Background Color', 'prcweb' ),
            'desc'      => 'default: rgba(0, 0, 0, 0.7)',
            'default'   => array(
                'color'     => '#000000',
                'alpha'     => 0.7
            ),
            'required' => array( 'prcweb_hiw_section_bg', '=', 1 ),
        ),

        array(
            'id'        => 'prcweb_hiw_section_title_color',
            'type'      => 'color',
            'output'    => array(
                '.submit-property .title',
                '.submit-property .sub-title',
            ),
            'title'     => __( 'Section Title Color', 'prcweb' ),
            'desc'      => 'default: #ffffff',
            'default'   => '#ffffff',
            'validate'  => 'color',
            'transparent' => false,
        ),

        array(
            'id'        => 'prcweb_hiw_text_color',
            'type'      => 'color',
            'output'    => array( '.submit-property' ),
            'title'     => __( 'Overall Text Color', 'prcweb' ),
            'desc'      => 'default: #b3b6bb',
            'default'   => '#b3b6bb',
            'validate'  => 'color',
            'transparent' => false,
        ),

        array(
            'id'        => 'prcweb_hiw_title_color',
            'type'      => 'color',
            'output'    => array( '.submit-property .submit-property-title' ),
            'title'     => __( 'Title Color in Columns', 'prcweb' ),
            'desc'      => 'default: #50b848',
            'default'   => '#50b848',
            'validate'  => 'color',
            'transparent' => false,
        ),

        array(
            'id'        => 'prcweb_hiw_button_color',
            'type'      => 'link_color',
            'output'    => array( '.submit-property .btn-green' ),
            'title'     => __( 'Button Text Color', 'prcweb' ),
            'default'   => array(
                'regular' => '#ffffff',
                'hover'   => '#ffffff',
                'active'  => '#ffffff',
            ),
            'transparent' => false,
        ),

        array(
            'id'        => 'prcweb_hiw_btn_background_color',
            'type'      => 'color',
            'mode'      => 'background-color',
            'output'    => array( '.submit-property .btn-green' ),
            'title'     => __( 'Button Background Color', 'prcweb' ),
            'desc'      => 'default: #50b848',
            'default'   => '#50b848',
            'validate'  => 'color',
            'transparent' => false,
        ),

        array(
            'id'        => 'prcweb_hiw_btn_hover_background_color',
            'type'      => 'color',
            'mode'      => 'background-color',
            'output'    => array(
                '.submit-property .btn-green:hover',
                '.submit-property .btn-green:focus'
            ),
            'title'     => __( 'Button Background Color on Hover', 'prcweb' ),
            'desc'      => 'default: #4bad43',
            'default'   => '#4bad43',
            'validate'  => 'color',
            'transparent' => false,
        ),


    ) ) );