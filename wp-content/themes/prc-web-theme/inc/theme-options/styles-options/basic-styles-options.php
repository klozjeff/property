<?php
/*
 * Basic Styles Options
 */
global $opt_name;

Redux::setSection( $opt_name, array(
    'title' => __( 'Basic', 'prcweb'),
    'id'    => 'basic-styles',
    'desc'  => __('This sub section contains basic styles options.', 'prcweb' ),
    'subsection' => true,
    'fields'=> array(

        array(
            'id'        => 'prcweb_body_background',
            'type'      => 'background',
            'output'    => array( 'body', '.site-pages' ),
            'title'     => __( 'Main Background', 'prcweb' ),
            'subtitle'  => __( 'Configure body background of your choice. ( default:#eff1f5 )', 'prcweb' ),
            'default'   => '#eff1f5'
        ),
        array(
            'id'        => 'prcweb_change_font',
            'type'      => 'switch',
            'title'     => __( 'Do you want to change fonts?', 'prcweb' ),
            'default'   => '0',
            'on'    => __( 'Yes', 'prcweb' ),
            'off'   => __( 'No', 'prcweb' )
        ),
        array(
            'id'        => 'prcweb_headings_font',
            'type'      => 'typography',
            'title'     => __( 'Headings Font', 'prcweb' ),
            'subtitle'  => __( 'Select the font for headings.', 'prcweb' ),
            'desc'      => __( 'Varela Round is default font.', 'prcweb' ),
            'required'  => array( 'prcweb_change_font', '=', '1' ),
            'google'    => true,
            'font-style'    => false,
            'font-weight'   => false,
            'font-size'     => false,
            'line-height'   => false,
            'color'         => false,
            'text-align'    => false,
            'output'        => array( 'h1','h2','h3','h4','h5','h6', '.h1','.h2','.h3','.h4','.h5','.h6' ),
            'default'       => array(
                'font-family' => 'Varela Round',
                'google'      => true
            )
        ),
        array(
            'id'        => 'prcweb_body_font',
            'type'      => 'typography',
            'title'     => __( 'Text Font', 'prcweb' ),
            'subtitle'  => __( 'Select the font for body text.', 'prcweb' ),
            'desc'      => __( 'Varela Round is default font.', 'prcweb' ),
            'required'  => array( 'prcweb_change_font', '=', '1' ),
            'google'    => true,
            'font-style'    => false,
            'font-weight'   => false,
            'font-size'     => false,
            'line-height'   => false,
            'color'         => false,
            'text-align'    => false,
            'output'        => array( 'body' ),
            'default'       => array(
                'font-family' => 'Varela Round',
                'google'      => true
            )
        ),
        array(
            'id'        => 'prcweb_headings_color',
            'type'      => 'color',
            'output'    => array( 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6' ),
            'title'     => __( 'Headings Color', 'prcweb' ),
            'default'   => '#4a525d',
            'validate'  => 'color',
            'transparent' => false,
            'desc'  => 'default: #4a525d',
        ),
        array(
            'id'        => 'prcweb_text_color',
            'type'      => 'color',
            'transparent' => false,
            'output'    => array( 'body' ),
            'title'     => __( 'Text Color', 'prcweb' ),
            'desc'  => 'default: #4a525d',
            'default'   => '#4a525d',
            'validate'  => 'color'
        ),
        array(
            'id'        => 'prcweb_blockquote_color',
            'type'      => 'color',
            'output'    => array( 'blockquote', 'blockquote p' ),
            'title'     => __('Quote Text Color', 'prcweb'),
            'default'   => '#4a525d',
            'validate'  => 'color',
            'transparent' => false,
            'desc'  => 'default: #4a525d',
        ),
        array(
            'id'        => 'prcweb_link_color',
            'type'      => 'link_color',
            'title'     => __( 'Link Color', 'prcweb' ),
            'active'    => true,
            'output'    => array( 'a' ),
            'default'   => array(
                'regular' => '#191c20',
                'hover'   => '#0dbae8',
                'active'  => '#0dbae8',
            )
        ),
        array(
            'id'        => 'prcweb_content_link_color',
            'type'      => 'link_color',
            'title'     => __( 'Link Color in Page and Post Contents', 'prcweb' ),
            'active'    => true,
            'output'    => array( '.default-page .entry-content a' ),
            'default'   => array(
                'regular' => '#0dbae8',
                'hover'   => '#ff8000',
                'active'  => '#ff8000',
            )
        ),
        array(
            'id'        => 'prcweb_animation',
            'type'      => 'switch',
            'title'     => __('Animation', 'prcweb'),
            'desc'     => __('You can enable or disable CSS3 animation on various components.', 'prcweb'),
            'default'   => 1,
            'on'        => __('Enable','prcweb'),
            'off'       => __('Disable','prcweb'),
        ),
        array(
            'id'        => 'prcweb_quick_css',
            'type'      => 'ace_editor',
            'title'     => __( 'Quick CSS', 'prcweb' ),
            'desc'      => __( 'You can use this box for some quick css changes. For big changes, Use the custom.css file in css folder. In case of child theme please use style.css file in child theme.', 'prcweb' ),
            'mode'      => 'css',
            'theme'     => 'monokai'
        )

    ) ) );