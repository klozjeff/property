<?php
/*
 * Single property page
 */
get_header();

get_template_part( 'partials/header/banner' );

global $prcweb_options;

// Flag for gallery images in case of 3rd variation
global $gallery_images_exists;
$gallery_images_exists = false;



?>
    <div id="content-wrapper" class="site-content-wrapper site-pages">

        <div id="content" class="site-content layout-boxed">

            <div class="container">

                <div class="container-property-single clearfix">

                    <?php
                    if ( have_posts() ) :
                        while ( have_posts() ) :
                            the_post();

                            global $prcweb_single_property;
                            $prcweb_single_property = new inspiry_Property( get_the_ID() );

                            /*
                             * Property header variation one
                             */

                                ?>
                                <div class="col-lg-8 zero-horizontal-padding property-slider-wrapper">
                                    <?php
                                    /*
                                     * Slider one
                                     */
                                    get_template_part( 'partials/property/single/slider' );

                                    ?>
                                </div>

                                <div class="col-lg-4 zero-horizontal-padding property-title-wrapper">
                                    <?php
                                    /*
                                     * Title
                                     */
                                    get_template_part( 'partials/property/single/title' );
                                    ?>
                                </div>

                            <div class="col-md-8 site-main-content property-single-content">

                                <main id="main" class="site-main">

                                    <article class="hentry clearfix">

                                    </article>
                                    <?php

                                    /*
                                     * Attachments
                                     */
                                    if ( $prcweb_options[ 'prcweb_property_attachments' ] ) {
                                        get_template_part( 'partials/property/single/attachments' );
                                    }

                                    /*
                                     * Share this property
                                     */
                                    if ( $prcweb_options[ 'prcweb_property_share' ] ) {
                                        get_template_part( 'partials/property/single/share' );
                                    }

                                    /*
                                     * Children Properties
                                     */
                                    if ( $prcweb_options[ 'prcweb_children_properties' ] ) {
                                        get_template_part( 'partials/property/single/children' );
                                    }

                                    /*
                                     * Comments
                                     */
                                    if ( $prcweb_options[ 'prcweb_property_comments' ] ) {
                                        if ( comments_open() || '0' != get_comments_number() ) :
                                            comments_template();
                                        endif;
                                    }
                                    ?>
                                </main>
                                <!-- .site-main -->

                            </div>
                            <!-- .site-main-content -->
                            <?php

                        endwhile;
                    endif;
                    ?>

                    <div class="col-md-4 zero-horizontal-padding">

                        <aside class="sidebar sidebar-property-detail">

                        </aside><!-- .sidebar -->

                    </div>
                    <!-- .site-sidebar-content -->

                </div>
                <!-- .container-property-single -->

            </div>
            <!-- .container -->

        </div>
        <!-- .site-content -->

    </div><!-- .site-content-wrapper -->

<?php
get_footer();