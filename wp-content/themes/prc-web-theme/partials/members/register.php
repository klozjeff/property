
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1616243768418285";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="form-wrapper">

    <div class="form-heading clearfix">
        <span><?php _e( 'Register', 'prcweb' ); ?></span>
        <button type="button" class="close close-modal-dialog" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times fa-lg"></i></button>
    </div>
   <!-- <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
-->
    <form id="register-form" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data">
        <div class="form-element">
            <label class="login-form-label" for="register_name"><?php _e( 'Full Name', 'prcweb' ); ?><span>*</span></label>
            <input id="register_name" name="register_name" type="text" class="login-form-input login-form-input-common"
                   title="<?php _e( '* Please enter your Full Name.', 'prcweb' ); ?>"
                   placeholder="<?php _e( 'Full Name', 'prcweb' ); ?>" />
        </div>
       <!-- <div class="form-element">
            <label class="login-form-label" for="register_username"><?php _e( 'Username', 'prcweb' ); ?><span>*</span></label>
            <input id="register_username" name="register_username" type="text" class="login-form-input login-form-input-common"
                   title="<?php _e( '* Please enter a valid username.', 'prcweb' ); ?>"
                   placeholder="<?php _e( 'Username', 'prcweb' ); ?>" />
        </div>-->

        <div class="form-element">
            <label class="login-form-label" for="register_email"><?php _e( 'Email', 'prcweb' ); ?><span>*</span></label>
            <input id="register_email" name="register_email" type="text" class="login-form-input login-form-input-common"
                   title="<?php _e( '* Please provide valid email address!', 'prcweb' ); ?>"
                   placeholder="<?php _e( 'Email', 'prcweb' ); ?>" />
        </div>

        <div class="form-element clearfix">
            <input type="submit" id="register-button" name="user-submit" class="login-form-submit login-form-input-common" value="<?php _e( 'Register', 'prcweb' ); ?>" />
            <input type="hidden" name="action" value="prcweb_ajax_register" />
            <?php  // nonce for security
            wp_nonce_field( 'prcweb-ajax-register-nonce', 'prcweb-secure-register' );

            if ( is_page() || is_single() ) {
                ?><input type="hidden" name="redirect_to" value="<?php wp_reset_postdata(); global $post; the_permalink( $post->ID ); ?>" /><?php
            } else {
                ?><input type="hidden" name="redirect_to" value="<?php echo esc_url( home_url( '/' ) ); ?>" /><?php
            }

            ?>
            <div class="text-center">
                <div id="register-message" class="modal-message"></div>
                <div id="register-error" class="modal-error"></div>
                <img id="register-loader" class="modal-loader" src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="Working...">
            </div>
        </div>

    </form>

    <div class="clearfix">
        <span class="login-link pull-left">
            <a href="#" class="activate-section" data-section="login-section"><?php _e( 'Login', 'prcweb' ); ?></a>
        </span>
        <span class="forgot-password pull-right">
            <a href="#" class="activate-section" data-section="password-section"><?php _e( 'Forgot Password?', 'prcweb' ); ?></a>
        </span>
    </div>

</div>

