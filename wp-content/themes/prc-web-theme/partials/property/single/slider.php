<?php
/*
 * Homepage properties slider
 */
global $post;
$gallery_images = prcweb_get_post_meta(
    'PRC_WEB_property_images',
    array(
        'type' => 'image_advanced',
        'size' => 'post-thumbnail'
    ),
    $post->ID
);
if ( !empty( $gallery_images ) ) {
    ?>
    <div class="single-property-slider">
        <ul id="image-gallery" class="list-unstyled">

                <?php

               foreach( $gallery_images as $gallery_image ) {
                     // caption
                     $caption = (!empty($gallery_image['caption'])) ? $gallery_image['caption'] : $gallery_image['alt'];
                     echo '<li data-thumb="'. $gallery_image['url'].'"> ';
                     echo '<a class="swipebox" data-rel="gallery-'. $post->ID  .'" href="'. $gallery_image['full_url'] .'" title="'. $caption .'" >';
                     echo '<img src="'. $gallery_image['url'] .'" alt="'. $gallery_image['title'] .'" />';
                     echo '</a>';
                     echo '</li>';
                 }
                ?>

        </ul>
    </div>
    <!--
     <div class="homepage-slider slider-variation-three flexslider slider-loader">
        <ul class="slides">
            <?php

   /* foreach( $gallery_images as $gallery_image ) {
        // caption
        $caption = (!empty($gallery_image['caption'])) ? $gallery_image['caption'] : $gallery_image['alt'];
        echo '<li>';
        echo '<a class="swipebox" data-rel="gallery-'. $post->ID  .'" href="'. $gallery_image['full_url'] .'" title="'. $caption .'" >';
        echo '<img src="'. $gallery_image['url'] .'" alt="'. $gallery_image['title'] .'" />';
        echo '</a>';
        echo '</li>';
    }*/
 ?>
 </ul>
     </div>-->


    <?php
}
else
{
    get_template_part( 'partials/header/banner' );
}

?>