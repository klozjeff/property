<?php
global $prcweb_options;
global $prcweb_single_property;
global $contact_form_email;

$property_title = get_the_title( $prcweb_single_property->get_post_ID() );
$property_permalink = get_permalink( $prcweb_single_property->get_post_ID() );

?>
<div class="property-contact-form">
    <h4 class="fancy-title">Talk to our Relation Manager</h4>

    <form class="agent-form contact-form-small" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>" novalidate="novalidate">


                <input type="text" name="name" placeholder="<?php _e('Name', 'prcweb'); ?>" class="form-control required" title="<?php _e('* Please provide your name', 'prcweb'); ?>" />

                <input type="text" name="email" placeholder="<?php _e('Email', 'prcweb'); ?>" class="form-control email required" title="<?php _e('* Please provide valid email address', 'prcweb'); ?>" />




                <input type="text" name="phone" placeholder="<?php _e('Phone No', 'prcweb'); ?>" class="form-control required" title="<?php _e('* Please provide your Phone No', 'prcweb'); ?>" />




        <select type="text" name="how_reach_us"  class="form-control">
            <option value="">How did you reach us</option>
            <option value="Advertisement">Advertisement</option>
            <option value="Website or Search Engine">Website or Search Engine</option>
            <option value="Email or Newsletter">Email or Newsletter</option>
            <option value="Facebook">Facebook</option>
            <option value="Family or Friend">Family or Friend</option>
        </select>
        <textarea  name="message" rows="5" class="form-control required" placeholder="<?php _e('Message', 'prcweb'); ?>" title="<?php _e('* Please provide your message', 'prcweb'); ?>">I am interested in <?php the_title();?>.</textarea>

        <?php //get_template_part( 'partials/common/google-reCAPTCHA' ); ?>

        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce( 'agent_message_nonce' ); ?>"/>

        <input type="hidden" name="target" value="klozjeff@gmail.com" />

        <input type="hidden" name="action" value="send_message_to_agent" />

        <input type="hidden" name="property_title" value="<?php echo esc_attr( $property_title ); ?>" />

        <input type="hidden" name="property_permalink" value="<?php echo esc_url( $property_permalink ); ?>" />

        <input type="submit" name="submit" class="agent-submit btn-default btn-round" value="<?php _e( 'Send Message', 'inspiry' ); ?>" />

        <img src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" class="agent-loader" alt="Loading..." />

        <div class="agent-error"></div>

        <div class="agent-message"></div>

    </form>

</div>