
<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 9/21/2017
 * Time: 9:50 AM
 */
global $prcweb_options;
global $prcweb_single_property;
global $post;
?>
<div class="col-md-12"><?php get_template_part( 'partials/property/single/title' ); ?></div>
<div class="col-md-6">
    <?php
    if (!empty($prcweb_options['prcweb_property_desc_title'])) {
        ?>
        <h4 class="fancy-title-housing">Introducing <?php the_title(); ?></h4><?php
    }
    ?>
    <div class="property-content"><?php the_content(); ?></div>
    <?php get_template_part( 'partials/property/single/attachments'); ?>
</div>
<div class="col-md-6">
    <?php get_template_part( 'partials/property/single/video' ); ?>
    <button class="call_to_button" id="submit-form">BUY NOW</button>
</div>
<div class="col-md-12">
    <div class="sectionAmenities">
        <div class="tabs-container">
                    <ul class="tabs-list">
                        <li class="tab vc_active" data-tab="rooftop"><a href="#rooftop" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Salient Features</span></a></li>
                        <li class="tab" data-tab="two-bed"><a href="#two-bed" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Two Bed Units</span></a></li>
                        <li class="tab" data-tab="three-bed"><a href="#three-bed" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Three Bed Units</span></a></li>
                        <li class="tab" data-tab="location"><a href="#location" data-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Location Map</span></a></li>
                    </ul>
                </div>

        <div class="panels-container">
            <div class="panels">
                <div class="vc_tta-panel .exterior vc_active" id="rooftop" data-vc-content=".vc_tta-panel-body">

                    <div class="panel-body" style="">
                        <div class="row row-fluid">
                            <div class="section_left col-sm-6">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


