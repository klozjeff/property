
<div id="site-visit" class="booked-modal modal" tabindex="-1" role="dialog" aria-labelledby="login-modal" aria-hidden="true">

    <div class="modal-dialog bm-window">

        <div class="site-visit modal-section booked-scrollable">
            <div class="form-wrapper">

                <div class="form-heading clearfix" style="background-color:#FFAB00 !important;border-bottom:0px solid;">
                    <p class="booked-title-bar"><?php _e( 'Schedule Site Visit', 'prcweb' ); ?></p>
                    <button type="button" class="close close-modal-dialog " data-dismiss="modal" aria-hidden="true"><i class="fa fa-times fa-lg"></i></button>
                </div>
                <form id="siteVisitForm" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" enctype="multipart/form-data">
                    <div class="bookings">

                        <p>Please provide date that you would like to visit the following property:</p>
                        <div class="form-group">
                            <div class="input-group date" id="site_visits">
                                <input class="required form-control" id="site_visits_date" name="site_visits_date" data-date-format="DD-MM-YYYY" type="text">
                                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
                            </div>
                        </div>


                        <div class="field">
                            <label class="field-label">Your Information:<i class="required-asterisk fa fa-asterisk" aria-hidden="true"></i></label>
                            <p class="field-small-p">Your Name:</p>
                        </div>
                        <div class="field form-group">
                            <label class="login-form-label" for="login-username"><?php _e( 'Username', 'prcweb' ); ?></label>
                            <input id="login-username" name="log" type="text" class="form-control required"
                                   title="<?php _e( '* Provide your username', 'prcweb' ); ?>"
                                   placeholder="<?php _e( 'Full Name', 'prcweb' ); ?>" />
                        </div>
                        <div class="field">
                       <p class="field-small-p">Phone No or Email:</p>
                        </div>
                        <div class="field form-group">
                            <label class="login-form-label" for="login-username"><?php _e( 'Username', 'prcweb' ); ?></label>
                            <input id="login-username" name="log" type="text" class="form-control  required"
                                   title="<?php _e( '* Provide your username', 'prcweb' ); ?>"
                                   placeholder="<?php _e( 'Email or Phone No', 'prcweb' ); ?>" />
                        </div>

                        <div class="field">
                            <input id="submit-request-appointment" class="button button-primary" value="Schedule Site Visit" type="submit">
                        </div>
                        <input type="hidden" name="action" value="prcweb_ajax_login" />
                        <input type="hidden" name="user-cookie" value="1" />
                        <?php
                        // nonce for security
                        wp_nonce_field( 'prcweb-ajax-login-nonce', 'prcweb-secure-login' );

                        if ( is_page() || is_single() ) {
                            ?><input type="hidden" name="redirect_to" value="<?php wp_reset_postdata(); global $post; the_permalink( $post->ID ); ?>" /><?php
                        } else {
                            ?><input type="hidden" name="redirect_to" value="<?php echo esc_url( home_url( '/' ) ); ?>" /><?php
                        }

                        ?>
                        <div class="text-center">
                            <div id="login-message" class="modal-message"></div>
                            <div id="login-error" class="modal-error"></div>
                            <img id="login-loader" class="modal-loader" src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="Working...">
                        </div>

                    </div>




                    <!--<div class="form-element clearfix">
                        <input type="submit" id="login-button" class="login-form-submit login-form-input-common" value="<?php _e( 'Login', 'prcweb' ); ?>" />
                        <input type="hidden" name="action" value="prcweb_ajax_login" />
                        <input type="hidden" name="user-cookie" value="1" />
                        <?php
                        // nonce for security
                        wp_nonce_field( 'prcweb-ajax-login-nonce', 'prcweb-secure-login' );

                        if ( is_page() || is_single() ) {
                            ?><input type="hidden" name="redirect_to" value="<?php wp_reset_postdata(); global $post; the_permalink( $post->ID ); ?>" /><?php
                        } else {
                            ?><input type="hidden" name="redirect_to" value="<?php echo esc_url( home_url( '/' ) ); ?>" /><?php
                        }

                        ?>
                        <div class="text-center">
                            <div id="login-message" class="modal-message"></div>
                            <div id="login-error" class="modal-error"></div>
                            <img id="login-loader" class="modal-loader" src="<?php echo get_template_directory_uri(); ?>/images/ajax-loader.gif" alt="Working...">
                        </div>
                    </div>-->

                </form>


                <div class="clearfix" style="border-top: 1px solid #e9e9ea">

                        <span class="sign-up pull-left">

                            <a href="#">**Note: This is a site visit booking form.Kindly provide required details</a>

                        </span>
                </div>

            </div>
        </div>
        <!-- .site-visit-modal -->





    </div>
    <!-- .modal-dialog -->

</div><!-- .modal -->

<script>
    (function ($) {
        "use strict";
        var dateFormat = $(this).attr('data-vat-rate');
        $('#site_visits').datetimepicker({
            showClose: false,
            format: dateFormat
        });

    })(jQuery);
</script>