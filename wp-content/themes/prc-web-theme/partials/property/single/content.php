<div class="entry-content clearfix">

    <?php
    global $prcweb_options;
    global $prcweb_single_property;
    global $post;

    ?>


    <ul class="nav nav-tabs" id="myTabs">
        <li class="tab-title" data-tab="property_details">Property Info</li>
        <li class="tab-title" data-tab="property_video">Video</li>
        <li class="tab-title active" id="mapTab" data-tab="property_map">Select Plots</li>
        <li class="tab-title" data-tab="property_features">Additional Info</li>
        <li class="tab-title" data-tab="property_tools">Tools</li>
    </ul>

    <!--Video Code Start -->
    <div class="tab-content" id="property_video">
        <?php get_template_part('partials/property/single/video'); ?>
    </div>
    <!--Video code End -->
    <!--Map Section Start -->
    <div class="tab-content  active" id="property_map">
        <?php get_template_part( 'partials/property/single/plots'); ?>

    </div>

    <!--Map Section End -->


    <!-- Other Property details -->
    <div class="tab-content" id="property_details">
        <div class="tevolution_custom_field  listing_custom_field"><p class="">
                <label>Land Reference:&nbsp;</label> <strong><span>
                <?= '#' . get_post_meta($post->ID, 'PRC_WEB_reference', true); ?></span></strong>
            </p>
        </div>
        <div id="overview" class="entry-content">

            <div class="entry-content frontend-entry-content ">
                <?php
                if (!empty($post->post_content) && $prcweb_options['prcweb_property_description']) {
                    if (!empty($prcweb_options['prcweb_property_desc_title'])) {
                        ?>
                        <h4 class="fancy-title"><?php echo esc_html($prcweb_options['prcweb_property_desc_title']); ?></h4><?php
                    }
                    ?>
                    <div class="property-content"><?php the_content(); ?>

                    <div class="mtl">

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <p></p>
                                <svg xmlns="http://www.w3.org/2000/svg" class="meta-icon-container" width="30"
                                     height="30" viewBox="0 0 48 48">
                                    <path class="meta-icon" fill="#07AA9C"
                                          d="M46 16v-12c0-1.104-.896-2.001-2-2.001h-12c0-1.103-.896-1.999-2.002-1.999h-11.997c-1.105 0-2.001.896-2.001 1.999h-12c-1.104 0-2 .897-2 2.001v12c-1.104 0-2 .896-2 2v11.999c0 1.104.896 2 2 2v12.001c0 1.104.896 2 2 2h12c0 1.104.896 2 2.001 2h11.997c1.106 0 2.002-.896 2.002-2h12c1.104 0 2-.896 2-2v-12.001c1.104 0 2-.896 2-2v-11.999c0-1.104-.896-2-2-2zm-4.002 23.998c0 1.105-.895 2.002-2 2.002h-31.998c-1.105 0-2-.896-2-2.002v-31.999c0-1.104.895-1.999 2-1.999h31.998c1.105 0 2 .895 2 1.999v31.999zm-5.623-28.908c-.123-.051-.256-.078-.387-.078h-11.39c-.563 0-1.019.453-1.019 1.016 0 .562.456 1.017 1.019 1.017h8.935l-20.5 20.473v-8.926c0-.562-.455-1.017-1.018-1.017-.564 0-1.02.455-1.02 1.017v11.381c0 .562.455 1.016 1.02 1.016h11.39c.562 0 1.017-.454 1.017-1.016 0-.563-.455-1.019-1.017-1.019h-8.933l20.499-20.471v8.924c0 .563.452 1.018 1.018 1.018.561 0 1.016-.455 1.016-1.018v-11.379c0-.132-.025-.264-.076-.387-.107-.249-.304-.448-.554-.551z"></path>
                                </svg>
                                <strong>Price:</strong> <?php $prcweb_single_property->price(); ?><p/>
                                <p>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="meta-icon-container" width="30"
                                         height="30" viewBox="0 0 48 48">
                                        <path class="meta-icon" fill-rule="evenodd" clip-rule="evenodd" fill="#07AA9C"
                                              d="M24 48.001c-13.255 0-24-10.745-24-24.001 0-13.254 10.745-24 24-24s24 10.746 24 24c0 13.256-10.745 24.001-24 24.001zm10-27.001l-10-8-10 8v11c0 1.03.888 2.001 2 2.001h3.999v-9h8.001v9h4c1.111 0 2-.839 2-2.001v-11z"></path>
                                    </svg>
                                    <strong>Property
                                        Type:</strong> <?= $prcweb_single_property->get_taxonomy_first_term('property-type', 'all')->name; ?>
                                </p>
                                <p>
                                    <?= get_post_meta($post->ID, 'PRC_WEB_property_install_desc', true); ?>

                                </p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="meta-icon-container" width="30"
                                         height="30" viewBox="0 0 48 48">
                                        <path class="meta-icon" fill="#07AA9C"
                                              d="M46 16v-12c0-1.104-.896-2.001-2-2.001h-12c0-1.103-.896-1.999-2.002-1.999h-11.997c-1.105 0-2.001.896-2.001 1.999h-12c-1.104 0-2 .897-2 2.001v12c-1.104 0-2 .896-2 2v11.999c0 1.104.896 2 2 2v12.001c0 1.104.896 2 2 2h12c0 1.104.896 2 2.001 2h11.997c1.106 0 2.002-.896 2.002-2h12c1.104 0 2-.896 2-2v-12.001c1.104 0 2-.896 2-2v-11.999c0-1.104-.896-2-2-2zm-4.002 23.998c0 1.105-.895 2.002-2 2.002h-31.998c-1.105 0-2-.896-2-2.002v-31.999c0-1.104.895-1.999 2-1.999h31.998c1.105 0 2 .895 2 1.999v31.999zm-5.623-28.908c-.123-.051-.256-.078-.387-.078h-11.39c-.563 0-1.019.453-1.019 1.016 0 .562.456 1.017 1.019 1.017h8.935l-20.5 20.473v-8.926c0-.562-.455-1.017-1.018-1.017-.564 0-1.02.455-1.02 1.017v11.381c0 .562.455 1.016 1.02 1.016h11.39c.562 0 1.017-.454 1.017-1.016 0-.563-.455-1.019-1.017-1.019h-8.933l20.499-20.471v8.924c0 .563.452 1.018 1.018 1.018.561 0 1.016-.455 1.016-1.018v-11.379c0-.132-.025-.264-.076-.387-.107-.249-.304-.448-.554-.551z"></path>
                                    </svg>
                                    <strong>Size:</strong> <?= $prcweb_single_property->get_area() . ' ' . $prcweb_single_property->get_area_postfix(); ?>
                                <p/>
                                <p>
                                <p>
                                    <svg xmlns="http://www.w3.org/2000/svg" class="meta-icon-container" width="30"
                                         height="30" viewBox="0 0 48 48">
                                        <path class="meta-icon" fill-rule="evenodd" clip-rule="evenodd" fill="#0DBAE8"
                                              d="M47.199 24.176l-23.552-23.392c-.504-.502-1.174-.778-1.897-.778l-19.087.09c-.236.003-.469.038-.696.1l-.251.1-.166.069c-.319.152-.564.321-.766.529-.497.502-.781 1.196-.778 1.907l.092 19.124c.003.711.283 1.385.795 1.901l23.549 23.389c.221.218.482.393.779.523l.224.092c.26.092.519.145.78.155l.121.009h.012c.239-.003.476-.037.693-.098l.195-.076.2-.084c.315-.145.573-.319.791-.539l18.976-19.214c.507-.511.785-1.188.781-1.908-.003-.72-.287-1.394-.795-1.899zm-35.198-9.17c-1.657 0-3-1.345-3-3 0-1.657 1.343-3 3-3 1.656 0 2.999 1.343 2.999 3 0 1.656-1.343 3-2.999 3z"></path>
                                    </svg>
                                    <strong>Status:</strong> <?= $prcweb_single_property->get_taxonomy_first_term('property-status', 'all')->name; ?>
                                <p/>
                                <p>
                            </div>
                            <div class="col-md-12">
                                <?php get_template_part( 'partials/property/single/features'); ?></div>


                            <div class="col-md-12">
                                <?php get_template_part( 'partials/property/single/attachments'); ?></div>
                        </div>
                    </div>

                    <hr>
                    </div><?php
                } ?>
            </div>
        </div><!-- end .entry-content -->
    </div>
    <!-- End Property details -->
    <div class="tab-content" id="property_features">
        <div class="entry-proprty_feature frontend_additional_features ">
            <div class="mtl">
                <?php if (!empty(get_post_meta($post->ID, 'PRC_WEB_property_highlight', true))): ?>
                    <div class="pbn mbn h5">
                        Highlights
                    </div>
                    <hr>
                    <p>
                        <?= get_post_meta($post->ID, 'PRC_WEB_property_highlight', true); ?>
                    </p>
                <?php endif; ?>
                <?php if (!empty(get_post_meta($post->ID, 'PRC_WEB_property_amenities', true))): ?>
                    <div class="pbn mbn h5">
                        Amenities
                    </div>
                    <hr>
                    <p>
                        <?= get_post_meta($post->ID, 'PRC_WEB_property_amenities', true); ?>
                    </p>
                <?php endif; ?>
                <?php if (!empty(get_post_meta($post->ID, 'PRC_WEB_property_overview', true))): ?>
                    <div class="pbn mbn h5">
                        Property Overview
                    </div>
                    <hr>
                    <p>
                        <?= get_post_meta($post->ID, 'PRC_WEB_property_overview', true); ?>
                    </p>
                <?php endif; ?>
                <?php if (!empty(get_post_meta($post->ID, 'PRC_WEB_property_forecast', true))): ?>
                    <div class="pbn mbn h5">
                        Property Forecast
                    </div>
                    <hr>
                    <p>
                        <?= get_post_meta($post->ID, 'PRC_WEB_property_forecast', true); ?>
                    </p>
                <?php endif; ?>
            </div>

        </div>
    </div>

    <div class="tab-content" id="property_tools">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse">
                            Affordability </a>
                        <p>Calculate your monthly Installment payments for <span
                                    class="typeWeightNormal typeLowlight"><?php the_title(); ?></span></p>
                    </h4>
                </div>
                <div id="collapse" class="panel-collapse collapse in">
                    <div class="panel-body">

                        <div class="col-md-12">
                            <div class="widget">

                                <div class="wbody">
                                    <div class="box boxBasic backgroundLowlight phm pvl">
                                        <div class="boxBody">
                                            <div class="row form-group">
                                                <div class="col-md-4"><p>Property Price</p>
<div class="input-group">
    <span class="input-group-addon">Kshs</span>   <input class="form-control" id="propertyprice_1" value="<?= $prcweb_single_property->get_price_without_prefix();?>" pattern="" placeholder="" type="text"></div>

                                                </div>
                                                <div class="col-md-4"><p>Down Payment</p>
                                                    <div class="input-group">
                                                        <input  class="form-control" id="deposit_1"  value="<?= $prcweb_single_property->get_deposit();?>" pattern="" placeholder="" type="text">
                                                        <span class="input-group-addon">30%</span></div>

                                                </div>
                                                <div class="col-md-3"><p>Interest Rate</p>
                                                    <div class="input-group">

                                                        <select class="form-control"  id="repayment" onchange="getInterest()" type="text">
                                                     <option value="">Select Option</option>
                                                            <option value="1">Cash</option>
                                                            <option value="2">2 Months</option>
                                                            <option value="3">3 Months</option>
                                                            <option value="4">4 Months</option>
                                                            <option value="5">5 Months</option>
                                                            <option value="6">6 Months</option>
                                                            <option value="7">7 Months</option>
                                                            <option value="8">8 Months</option>
                                                            <option value="9">9 Months</option>
                                                            <option value="10">10 Months</option>
                                                            <option value="11">11 Months</option>
                                                            <option value="12">12 Months</option>
                                                        </select>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <br/><br/>

                                    <div id="result"></div>
                                    <div id="total_interest"></div>
                                    <div id="total_new_price"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>