<?php
/*
 * Property Features
 */
global $post;
$features = get_the_terms( $post->ID, 'property-feature' );

if ( !empty( $features ) && is_array( $features ) && !is_wp_error( $features ) ) {
    ?>
    <div class="property-features">

        <div class="pbn mbn h5">
        Features
                            <span class="typeWeightNormal typeLowlight">of this property </span>
        </div><hr/>
        <?php
        if( !empty( $property_features_title ) ) {
            ?><h4 class="fancy-title"><?php echo esc_html( $property_features_title ); ?></h4><?php
        }
        ?>
        <ul class="property-features-list clearfix">
            <?php
            foreach( $features as $single_feature ) {
                echo '<li><a href="' . get_term_link( $single_feature->slug, 'property-feature' ) .'">'. $single_feature->name . '</a></li>';
            }
            ?>
        </ul>
    </div>
    <?php
}
