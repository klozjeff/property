<?php
global $prcweb_options;
global $prcweb_single_property;
?>


<header class="entry-header clearfix">
                        <div class="entry-header-title clearfix">
                            <div class="entry-header-left">
                                <div class="spt-left">

                        <span class="property-logo">

                        </span>

                                    <h1 class="entry-title ">
                                        <?php the_title(); ?>
</h1>

                                    <p class="address"><i class="fa fa-map-marker"></i> <span id="frontend_address"
                                                                                              class="listing_custom frontend_address"><?=$prcweb_single_property->get_address();?>   |    <?= $prcweb_single_property->get_area() . ' ' . $prcweb_single_property->get_area_postfix(); ?></span>
                                    </p>
                                </div>
                                <div class="spt-right">
                                </div>
                            </div>
                            <!-- Show Property type -->
                            <div class="entry-header-right">
                                <span class="property-price"><span> <?php echo $prcweb_single_property->get_price_without_postfix(); ?></span></span>



                                <div class="entry-content">

                                    <div role="group" aria-label="Button group with nested dropdown" class="listing_actions btn-group float-md-right">
                                        <div class="share_unit" style="display: none;">
                                            <a href="http://www.facebook.com/sharer.php?u=http://localhost/prc/properties/condo-in-financial-district/&amp;t=Condo+In+Financial+District" target="_blank" class="social_facebook"></a>
                                            <a href="http://twitter.com/home?status=Condo+In+Financial+District+http%3A%2F%2Flocalhost%2Fprc%2Fproperties%2Fcondo-in-financial-district%2F" class="social_tweet" target="_blank"></a>
                                            <a href="http://plus.google.com/share?url=http://localhost/prc/properties/condo-in-financial-district/" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="social_google"></a>
                                            <a href="http://pinterest.com/pin/create/button/?url=http://localhost/prc/properties/condo-in-financial-district/&amp;media=&amp;description=Condo+In+Financial+District" target="_blank" class="social_pinterest"></a>
                                        </div>

                                        <a href="#site-visit" data-toggle="modal" class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Site Visit</a>
                                        <a href="timeline-center.html" class="btn btn-outline-primary"><i class="fa fa-share"></i> Share</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </header>