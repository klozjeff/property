<section class="">
    <?php
    define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/uploads');
    global $_options;

    ?><h4 class="fancy-title">Property Items</h4><?php


    global $prcweb_single_property;
    global $post;
    $property_marker = array();

    $property_marker['lat'] = $prcweb_single_property->get_latitude();
    $property_marker['lang'] = $prcweb_single_property->get_longitude();

    /*
     * Property Map Icon Based on Property Type
     */
    $property_type_slug = $prcweb_single_property->get_taxonomy_first_term( 'property-type', 'slug' );
    if ( !empty( $property_type_slug ) ) {
        $property_type_slug = 'single-family-home'; // Default Icon Slug
    }

    $template_dir = get_template_directory();
    $template_dir_uri = get_template_directory_uri();
    $base_icon_path = $template_dir .'/images/map/' . $property_type_slug;
    $base_icon_uri = $template_dir_uri .'/images/map/' . $property_type_slug;

    if ( file_exists( $base_icon_path .'-map-icon.png' ) ) {
        $property_marker['icon'] = $base_icon_uri . '-map-icon.png';
        if( file_exists( $base_icon_path . '-map-icon@2x.png' ) ) {
            $property_marker['retinaIcon'] = $base_icon_uri . '-map-icon@2x.png';   // retina icon
        }
    } else {
        $property_marker['icon'] = $base_icon_uri . '-map-icon.png';           // default icon
        $property_marker['retinaIcon'] = $base_icon_uri . '-map-icon@2x.png';  // default retina icon
    }

    ?>
<!--<script src="<?php echo get_template_directory_uri();?>/js/cart.js" type="text/javascript"></script>-->
   <style> #map-plots {width:650px; height:650px;
    }</style>
    <div id="map-plots"></div>

    <script type="text/javascript">

        var plotBooked=[{"name":"6","status":"3"},{"name":"7","status":"2"},{"name":"8","status":"2"},{"name":"9","status":"3"},{"name":"10","status":"3"},{"name":"11","status":"3"},{"name":"12","status":"3"},{"name":"13","status":"2"},{"name":"14","status":"3"},{"name":"15","status":"3"},{"name":"16","status":"3"},{"name":"17","status":"3"},{"name":"18","status":"3"},{"name":"19","status":"3"},{"name":"20","status":"3"},{"name":"21","status":"2"},{"name":"22","status":"3"},{"name":"23","status":"3"},{"name":"24","status":"3"},{"name":"25","status":"3"},{"name":"26","status":"3"},{"name":"27","status":"3"},{"name":"28","status":"3"},{"name":"29","status":"3"},{"name":"30","status":"3"},{"name":"31","status":"3"},{"name":"32","status":"3"},{"name":"33","status":"3"},{"name":"34","status":"3"},{"name":"35","status":"3"},{"name":"36","status":"3"},{"name":"37","status":"3"},{"name":"38","status":"3"},{"name":"39","status":"3"},{"name":"40","status":"3"},{"name":"41","status":"3"},{"name":"42","status":"3"},{"name":"43","status":"3"},{"name":"44","status":"3"},{"name":"45","status":"3"},{"name":"46","status":"2"},{"name":"47","status":"3"},{"name":"48","status":"3"},{"name":"49","status":"3"},{"name":"50","status":"3"},{"name":"51","status":"3"},{"name":"52","status":"3"},{"name":"53","status":"3"},{"name":"54","status":"3"},{"name":"55","status":"3"},{"name":"56","status":"3"},{"name":"57","status":"3"},{"name":"58","status":"3"},{"name":"70","status":"3"},{"name":"69","status":"3"},{"name":"68","status":"3"},{"name":"67","status":"3"},{"name":"66","status":"3"},{"name":"65","status":"3"},{"name":"64","status":"3"},{"name":"63","status":"2"},{"name":"62","status":"3"},{"name":"61","status":"3"},{"name":"60","status":"3"},{"name":"59","status":"3"},{"name":"71","status":"3"},{"name":"90","status":"3"},{"name":"72","status":"3"},{"name":"89","status":"3"},{"name":"73","status":"3"},{"name":"88","status":"3"},{"name":"74","status":"3"},{"name":"87","status":"3"},{"name":"75","status":"3"},{"name":"86","status":"2"},{"name":"76","status":"3"},{"name":"85","status":"3"},{"name":"77","status":"3"},{"name":"84","status":"3"},{"name":"78","status":"3"},{"name":"83","status":"3"},{"name":"79","status":"3"},{"name":"82","status":"3"},{"name":"80","status":"3"},{"name":"81","status":"3"},{"name":"108","status":"3"},{"name":"91","status":"3"},{"name":"107","status":"3"},{"name":"92","status":"3"},{"name":"106","status":"3"},{"name":"93","status":"3"},{"name":"105","status":"3"},{"name":"94","status":"3"},{"name":"104","status":"3"},{"name":"95","status":"3"},{"name":"103","status":"3"},{"name":"96","status":"3"},{"name":"102","status":"3"},{"name":"97","status":"3"},{"name":"101","status":"3"},{"name":"98","status":"3"},{"name":"100","status":"3"},{"name":"99","status":"2"},{"name":"132","status":"3"},{"name":"109","status":"3"},{"name":"131","status":"2"},{"name":"110","status":"3"},{"name":"130","status":"3"},{"name":"111","status":"3"},{"name":"129","status":"2"},{"name":"112","status":"3"},{"name":"128","status":"3"},{"name":"113","status":"3"},{"name":"127","status":"3"},{"name":"114","status":"2"},{"name":"126","status":"2"},{"name":"115","status":"3"},{"name":"125","status":"3"},{"name":"116","status":"2"},{"name":"124","status":"2"},{"name":"117","status":"3"},{"name":"123","status":"3"},{"name":"118","status":"3"},{"name":"122","status":"3"},{"name":"119","status":"3"},{"name":"121","status":"3"},{"name":"120","status":"3"},{"name":"156","status":"3"},{"name":"133","status":"2"},{"name":"155","status":"3"},{"name":"134","status":"3"},{"name":"154","status":"3"},{"name":"135","status":"3"},{"name":"153","status":"3"},{"name":"136","status":"3"},{"name":"152","status":"3"},{"name":"137","status":"2"},{"name":"151","status":"3"},{"name":"138","status":"3"},{"name":"150","status":"2"},{"name":"139","status":"3"},{"name":"149","status":"3"},{"name":"140","status":"3"},{"name":"148","status":"3"},{"name":"141","status":"3"},{"name":"147","status":"3"},{"name":"142","status":"3"},{"name":"146","status":"3"},{"name":"143","status":"3"},{"name":"145","status":"3"},{"name":"144","status":"3"},{"name":"180","status":"3"},{"name":"157","status":"2"},{"name":"179","status":"3"},{"name":"158","status":"3"},{"name":"178","status":"3"},{"name":"159","status":"3"},{"name":"177","status":"3"},{"name":"160","status":"3"},{"name":"176","status":"3"},{"name":"161","status":"3"},{"name":"175","status":"2"},{"name":"162","status":"2"},{"name":"174","status":"2"},{"name":"163","status":"3"},{"name":"173","status":"3"},{"name":"164","status":"3"},{"name":"172","status":"3"},{"name":"165","status":"3"},{"name":"171","status":"3"},{"name":"166","status":"3"},{"name":"170","status":"3"},{"name":"167","status":"3"},{"name":"169","status":"3"},{"name":"168","status":"3"},{"name":"204","status":"3"},{"name":"181","status":"3"},{"name":"203","status":"3"},{"name":"182","status":"3"},{"name":"202","status":"3"},{"name":"183","status":"3"},{"name":"201","status":"3"},{"name":"184","status":"3"},{"name":"200","status":"3"},{"name":"185","status":"2"},{"name":"199","status":"3"},{"name":"186","status":"3"},{"name":"198","status":"2"},{"name":"187","status":"3"},{"name":"197","status":"3"},{"name":"188","status":"2"},{"name":"196","status":"3"},{"name":"189","status":"3"},{"name":"195","status":"3"},{"name":"190","status":"3"},{"name":"194","status":"3"},{"name":"191","status":"3"},{"name":"193","status":"3"},{"name":"192","status":"2"},{"name":"205","status":"3"},{"name":"206","status":"3"},{"name":"207","status":"3"},{"name":"208","status":"3"},{"name":"209","status":"3"},{"name":"210","status":"3"},{"name":"211","status":"3"},{"name":"212","status":"3"},{"name":"213","status":"3"},{"name":"214","status":"2"},{"name":"217","status":"2"},{"name":"218","status":"3"},{"name":"216","status":"3"},{"name":"219","status":"3"},{"name":"215","status":"3"},{"name":"220","status":"2"},{"name":"221","status":"2"},{"name":"222","status":"3"},{"name":"223","status":"3"},{"name":"224","status":"3"},{"name":"225","status":"3"},{"name":"242","status":"3"},{"name":"227","status":"3"},{"name":"226","status":"3"},{"name":"241","status":"3"},{"name":"228","status":"2"},{"name":"240","status":"2"},{"name":"229","status":"2"},{"name":"239","status":"3"},{"name":"230","status":"2"},{"name":"238","status":"3"},{"name":"231","status":"3"},{"name":"237","status":"3"},{"name":"232","status":"3"},{"name":"236","status":"3"},{"name":"233","status":"3"},{"name":"235","status":"3"},{"name":"234","status":"3"},{"name":"243","status":"3"},{"name":"254","status":"3"},{"name":"244","status":"3"},{"name":"253","status":"3"},{"name":"245","status":"3"},{"name":"252","status":"3"},{"name":"246","status":"3"},{"name":"251","status":"3"},{"name":"247","status":"3"},{"name":"250","status":"3"},{"name":"248","status":"3"},{"name":"249","status":"3"},{"name":"270","status":"3"},{"name":"255","status":"3"},{"name":"269","status":"3"},{"name":"256","status":"3"},{"name":"268","status":"3"},{"name":"257","status":"3"},{"name":"267","status":"3"},{"name":"258","status":"3"},{"name":"266","status":"3"},{"name":"259","status":"3"},{"name":"265","status":"3"},{"name":"260","status":"3"},{"name":"264","status":"3"},{"name":"261","status":"3"},{"name":"263","status":"3"},{"name":"262","status":"3"},{"name":"3","status":"3"},{"name":"2","status":"2"},{"name":"5","status":"3"},{"name":"82","status":"3"},{"name":"6","status":"3"}];
        var plotbookedarray=plotBooked;
        var mapInstance;
        var parser;
        var infowindow = new google.maps.InfoWindow();

        function plotAction(position, text,name,price, map) {
          //  console.log(map);
var property='<?php echo the_title();?>';
var propertyID='<?php echo $post->ID;?>';

            var bookedflag = false;

            for (var i = 0; i < plotbookedarray.length; i++) {

                if(plotbookedarray[i].name == name)
                {
                    bookedflag = true;
                }
            }


            if(bookedflag)
            {
                var content = '<div style="color:red">Sorry, that plot has already been booked. Please select another one.</div>';
            }else{
                var content = 'This plot number '+name+' is available. Click the button below to add to cart.<br><a href="#0" class="cd-add-to-cart" data-proid="'+propertyID+'" data-property="'+property+'" data-name="'+name+'" data-price='+price+'>Add to Cart</a></div>';
            }

           // console.log(infowindow);

            infowindow.setContent(content);
            infowindow.setPosition(position);
            infowindow.open(map);
        }


        function initialize() {
            //console.log("in init");
            var latlng = new google.maps.LatLng(-0.0215282535,36.6690911933);
            var mapOptions = {
                Zoom: 17,
                center: latlng,
                scrollwheel: false,
                draggable: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.DEFAULT
                }
            };
            mapInstance = new google.maps.Map(document.getElementById("map-plots"), mapOptions);
            var mcOptions = {gridSize: 100, maxZoom: 15};
            var markerclusterer = new MarkerClusterer(mapInstance, [], mcOptions);
            parser = new geoXML3.parser({
                    map: mapInstance,
                    zoom: false,
                    processStyles: true,
                    singleInfoWindow: true,
                    createMarker: function(placemark) {
                        var point = placemark.latlng;
                        if(/^[0-9]+$/.test(String(placemark.name))) {
                            var bookedflag = false;

                            for (var i = 0; i < plotbookedarray.length; i++) {

                                if (plotbookedarray[i].name == placemark.name) {
                                    bookedflag = true;
                                }
                            }

                            if (bookedflag) {
                                var pinColor = "b30303";//checkIfAvailable(placemark.name);
                            } else {
                                var pinColor = "07AA9C";
                            }
                        }

                        //checkIfAvailable(placemark.name);
                        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                            new google.maps.Size(21, 34),
                            new google.maps.Point(0,0),
                            new google.maps.Point(10, 34));
                        var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                            new google.maps.Size(40, 37),
                            new google.maps.Point(0, 0),
                            new google.maps.Point(12, 35));
                        var marker = new google.maps.Marker({
                            position: point,
                            map: mapInstance,
                            icon: pinImage,
                            shadow: pinShadow
                        });
var price=219000;
                        google.maps.event.addListener(marker, 'click', function(){
                            plotAction(placemark.latlng, placemark.description, placemark.name,price,mapInstance);
                        });

                        markerclusterer.addMarker(marker);
                    }
                });

            parser.parse('http://localhost/property/wp-content/uploads/2017/05/aberdare_phase_2.kml');
        }


      google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <div class="cd-cart-container empty">
        <a href="" class="cd-cart-trigger">
            Cart
            <ul class="count"> <!-- cart items count -->
                <li>0</li>
                <li>0</li>
            </ul> <!-- .count -->
        </a>

        <div class="cd-cart">
            <div class="wrapper">
                <header>
                    <h2>Cart</h2>
                    <span class="undo">Item removed. <a href="#0">Undo</a></span>
                </header>

                <div class="body">
                    <ul>

                        <!-- products added to the cart will be inserted here using JavaScript -->
                    </ul>
                </div>

                <footer>
                    <a href="<?php echo home_url() ?>/checkout" class="checkout btn"><em>Checkout - Kshs <span>0</span></em></a>
                </footer>
            </div>
        </div> <!-- .cd-cart -->
    </div> <!-- cd-cart-container -->

</section>