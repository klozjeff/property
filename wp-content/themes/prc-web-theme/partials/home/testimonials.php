<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/29/2017
 * Time: 9:57 PM
 */

global $prcweb_options;
$number_of_testimonials = intval( $prcweb_options[ 'prcweb_home_testimonials_number' ] );
if( !$number_of_testimonials ) {
    $number_of_testimonials = 4;
}
$testimonials_query_args = array(
    'post_type' => 'testimonial',
    'posts_per_page' => $number_of_testimonials
);

$testimonials_query = new WP_Query( $testimonials_query_args );

if ( $testimonials_query->have_posts() ) :
?>

<div id="testimonials" class="testimony">
    <section class="xs-hide sm-hide container mx-auto rounded">
        <div class="mxn6 overflow-hidden rounded shadow">
            <div class="flex bg-slate" style="background-image: url(<?=get_template_directory_uri();?>/images/Images-2.png); background-position: center right; background-size: cover;">
                <div class="col-6 p6 bg-quote-red">
                    <div class="carousel slide" id="carousel-testimonial">

                        <div class="carousel-inner">
    <?php
    $counter = 1;
    while ($testimonials_query->have_posts()):
        $testimonials_query->the_post();
        $testimony_id=get_the_ID();

        ?> <div class="item<?php if($counter <= 1){echo " active"; } ?>"><blockquote class="mb5">
                        <h4 class="white caps mb2 m0 h3"><?php the_title(); ?></h4>
                        <p class="white hanging-quote block"><?= esc_html(the_content());?></p>
                        <cite class="slate hanging-emdash block">—<?= get_post_meta($testimony_id,'PRC_WEB_client_name',true).','.get_post_meta($testimony_id,'PRC_WEB_client_title',true).'-'.get_post_meta($testimony_id,'PRC_WEB_client_company',true);?></cite>
        <p> <a href="<?php permalink_link();?>" class="white hover-white btn btn-outline">Read the testimony</a></p>
        </blockquote>

    </div>
        <?php   $counter++; endwhile;?>
                        </div>
                        <p>
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-testimonial" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-testimonial" data-slide-to="1"></li>
                            <li data-target="#carousel-testimonial" data-slide-to="2"></li>
                            <li data-target="#carousel-testimonial" data-slide-to="3"></li>
                        </ol></p>
                    </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

</div>
<?php
endif;
?>