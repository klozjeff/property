<?php
global $prcweb_options;
$variation    = 'one';
$hiw_bg_color = '#ffffff';

$hiw_bg_img   = null;
if ( isset( $prcweb_options[ 'prcweb_hiw_bg_image' ] ) ) {
    $hiw_bg_img = $prcweb_options[ 'prcweb_hiw_bg_image' ][ 'url' ];
}

if ( $prcweb_options[ 'prcweb_hiw_section_bg' ] == 0 ) {
    $variation    = 'two';
    $hiw_bg_img   = null;
    $hiw_bg_color = '#ffffff';
}

if ( isset( $prcweb_options[ 'prcweb_hiw_background_color' ] ) ){
    $hiw_bg_color = $prcweb_options[ 'prcweb_hiw_background_color' ];
}

$prcweb_allowed_tags = array(
    'a' => array(
        'href' => array(),
        'title' => array(),
        'target' => array(),
    ),
    'em' => array(),
    'strong' => array(),
    'br' => array(),
);

?>
<section class="sub-nav" id="sub-nav">
    <div class="container"><div class="content"><div class="actions">
                <a class="primary" href=""><span>View All Properties</span></a></div>
            <a class="prompt-link" href="#">Browse</a>
        <div class="destinations"><a href="#top">Overview</a>
                <a href="#featured">Recent Properties</a>
                <a href="#new-mploti">M-Ploti</a><a href="#testimonials">Testimonials</a><a href="#awards">Awards</a></div></div></div> </section>
 <section id="overview" class="prc-why-us prc-why-us-three" style="background-color:#f5f5f5;">
        <div class="container">
           <!-- <header class="prc-why-us-header">
                <h2 class="title"><?php if ( !empty( $prcweb_options[ 'prcweb_hiw_welcome' ] ) ) {
                ?><h3 class="sub-title"><?php echo esc_html( $prcweb_options[ 'prcweb_hiw_welcome' ] ); ?></h3><?php
            }?>
</h2>
            </header>-->
			
			 <?php
        /*
         * Columns
         */
        $hiw_columns = $prcweb_options[ 'prcweb_hiw_columns' ];
        if ( $hiw_columns != '0' ) {

            // column class
            $hiw_column_class = 'col-sm-3';
			 if ( $hiw_columns == '3' ) {
                $hiw_column_class = 'col-sm-4';
            } 
            elseif ( $hiw_columns == '2' ) {
                $hiw_column_class = 'col-sm-6';
            } else if ( $hiw_columns == '1' ) {
                $hiw_column_class = 'col-sm-12';
            }

            // alignment class
            if ( $prcweb_options[ 'prcweb_hiw_column_alignment' ] == 'center' ) {
                $hiw_column_class .= ' text-center';
            }
            
            // todo: provide anchor styles in description text in version 1.1
            ?>
            <div class="row prc-why-us-placeholders">
			 <?php if ( in_array( $hiw_columns, array( '1', '2', '3','4' ) ) ) { ?>
                <div class="<?php echo esc_attr( $hiw_column_class ); ?> prc-why-us-placeholder">
                    <div class="image-wrapper">
                        <a href="#"><img src="<?php echo esc_url( $prcweb_options['prcweb_hiw_1st_col_icon']['url'] ); ?>" alt="Icon"/></a>
                    </div>
                    <h3 class="submit-property-title"><?php echo esc_html( $prcweb_options['prcweb_hiw_1st_col_title'] ); ?></h3>
                    <p>
                        <?php echo wp_kses( $prcweb_options['prcweb_hiw_1st_col_description'], $prcweb_allowed_tags ); ?>
                    </p>
                </div>
			 <?php } if ( in_array( $hiw_columns, array('2', '3','4' ) ) ) { ?>
                <div class="<?php echo esc_attr( $hiw_column_class ); ?> prc-why-us-placeholder">
                    <div class="image-wrapper">
                        <a href="#"><img src="<?php echo esc_url( $prcweb_options['prcweb_hiw_2nd_col_icon']['url'] ); ?>" alt="Icon"/></a>
                    </div>
                    <h3 class="prc-why-us-title"><?php echo esc_html( $prcweb_options['prcweb_hiw_2nd_col_title'] ); ?></h3>
                    <p>
                     <?php echo wp_kses( $prcweb_options['prcweb_hiw_2nd_col_description'], $prcweb_allowed_tags ); ?>     </p>
                </div>
				 <?php } if ( in_array( $hiw_columns, array('3','4' ) ) ) {?>
                <div class="<?php echo esc_attr( $hiw_column_class ); ?> prc-why-us-placeholder">
                    <div class="image-wrapper">
                        <a href="#"><img src="<?php echo esc_url( $prcweb_options['prcweb_hiw_3rd_col_icon']['url'] ); ?>" alt="Icon"/></a>
                    </div>
                    <h3 class="prc-why-us-title"><?php echo esc_html( $prcweb_options['prcweb_hiw_3rd_col_title'] ); ?></h3>
                    <p>
                        <?php echo wp_kses( $prcweb_options['prcweb_hiw_3rd_col_description'], $prcweb_allowed_tags ); ?>       </p>
                </div>
				 <?php } if ( in_array( $hiw_columns, array('4' ) ) ) {?>
                <div class="<?php echo esc_attr( $hiw_column_class ); ?> prc-why-us-placeholder">
                    <div class="image-wrapper">
                        <a href="#"><img src="<?php echo esc_url( $prcweb_options['prcweb_hiw_4th_col_icon']['url'] ); ?>" alt="Icon"/></a>
                    </div>
                    <h3 class="prc-why-us-title"><?php echo esc_html( $prcweb_options['prcweb_hiw_4th_col_title'] ); ?></h3>
                    <p>
                     <?php echo wp_kses( $prcweb_options['prcweb_hiw_4th_col_description'], $prcweb_allowed_tags ); ?> 
                    </p>
                </div>
				 <?php } ?>
            </div>
			
			<?php
		}
		?>

        </div>
        <!-- .container -->
    </section>


