<?php
global $prcweb_options;
$number_of_featured_properties = intval( $prcweb_options[ 'prcweb_home_featured_number_3' ] );
if( !$number_of_featured_properties ) {
    $number_of_featured_properties = 2;
}


// Featured properties query arguments
//Land
$featured_properties_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_featured_properties,
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'PRC_WEB_featured',
            'value' => 1,
            'compare' => '=',
            'type'  => 'NUMERIC'
        ),
        array(
            'taxonomy' => 'property-type',
            'field'    => 'slug',
            'terms'    => array('land'),
              'include_children' => true,
        'operator' => 'IN'
        )

    )
);

$featured_properties_query = new WP_Query( $featured_properties_args );
$totalProperties=$featured_properties_query->max_num_pages;

//Housing Query
$featured_properties_housing = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_featured_properties,
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'PRC_WEB_featured',
            'value' => 1,
            'compare' => '=',
            'type'  => 'NUMERIC'
        ),
        array(
            'taxonomy' => 'property-type',
            'field'    => 'slug',
            'terms'    => array('housing'),
            'include_children' => true,
            'operator' => 'IN'
        )

    )
);

$featured_housing_query = new WP_Query( $featured_properties_housing);
$totalPropertiesHousing=$featured_housing_query->max_num_pages;

if ( $featured_properties_query->have_posts() || $featured_housing_query->have_posts()) :
?>
<div id="featured" class="container">
<div class="featured-properties-three">

    <div class="tabs-container">
        <div class="row border-bottom section-header">


                <div class="tab-menu">
                    <span class="title"> <?php echo esc_html( $prcweb_options[ 'prcweb_home_featured_title' ] ); ?></span>
                    <div class="btn-group js-tab-type-menu pull-right">
                        <a class="tab-menu--land small-uppercase--spaced btn--selected" data-target="land" href="#land">Land</a>
                        <a class="tab-menu--housing small-uppercase--spaced" data-target="housing" href="#housing">Housing</a>
                    </div>
                </div>


        </div>
    </div>
</div>
        <div class="container tabs-content" id="tabs-content">
            <div class="row section-padding tab-item tab-item--land active">

                <?php

                /*
                 * Columns
                 */
                // $columns_count = intval( $prcweb_options[ 'prcweb_home_featured_columns' ] );
                //if( !$totalProperties ) {
                //   $columns_count = 2;
                // }
                $column_class = 'col-md-6';
                if ($totalProperties==1) {
                    $column_class = 'col-md-6';
                }
                $properties_count = 1;

                echo '<div class="row row-odd zero-horizontal-margin">';

                while ( $featured_properties_query->have_posts() ) :
                $featured_properties_query->the_post();

                $featured_property = new inspiry_Property(get_the_ID());

                if ( 0 == ( $properties_count % 2 ) ) {
                    $odd_even_class = 'property-post-even';
                } else {
                    $odd_even_class = 'property-post-odd';
                }
                ?>
                <div class="<?php echo $column_class;?> custom-col-xs-12">

                    <article class="row property_listing hentry property-listing-home property-listing-one meta-item-half">

                        <div class="property-thumbnail zero-horizontal-padding col-lg-6">
                            <div class="prop_new_details">
                                <div class="prop_new_details_back"></div>
                                <div class="property_media">
                                    <i class="fa fa-camera" aria-hidden="true"></i> 1
                                </div><div class="property_location_image">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <?php echo $featured_property->get_address();?></div>
                            </div>
                            <?php prcweb_thumbnail_featured(); ?>    </div>
                        <!-- .property-thumbnail -->

                        <div class="property-description clearfix col-lg-6">
                            <header class="entry-header">
                                <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                <div class="price-and-status">
                                    <span class="price"><?php echo esc_html( $featured_property->get_price_without_postfix()); ?></span>
                                    <!--<?php
                                    $first_status_term = $featured_property->get_taxonomy_first_term( 'property-status', 'all' );
                                    if ( $first_status_term ) {
                                        ?>
                            <a href="<?php the_permalink(); ?>">
                                <span class="property-status-tag"><?php echo esc_html( $first_status_term->name ); ?></span>
                            </a>
                            <?php
                                    }
                                    ?>-->

                                </div>
                            </header>
                            <p><?php prcweb_excerpt( 15 ); ?></p><hr>
                            <!-- .property-meta -->
                            <p>
                            <div class="listing_actions">

                                <div class="share_unit" style="display: none;">
                                    <a href="http://www.facebook.com/sharer.php?u=http://localhost/prc/properties/condo-in-financial-district/&amp;t=Condo+In+Financial+District" target="_blank" class="social_facebook"></a>
                                    <a href="http://twitter.com/home?status=Condo+In+Financial+District+http%3A%2F%2Flocalhost%2Fprc%2Fproperties%2Fcondo-in-financial-district%2F" class="social_tweet" target="_blank"></a>
                                    <a href="http://plus.google.com/share?url=http://localhost/prc/properties/condo-in-financial-district/" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="social_google"></a>
                                    <a href="http://pinterest.com/pin/create/button/?url=http://localhost/prc/properties/condo-in-financial-district/&amp;media=&amp;description=Condo+In+Financial+District" target="_blank" class="social_pinterest"></a>
                                </div>



                                <span class="share_list share_on" data-original-title="share"></span>
                                <span class="icon-fav icon-fav-off" data-original-title="add to favorites" data-postid="3701"></span>

                           </div>
                                <a href="<?php the_permalink(); ?>" class="white hover-white btn btn-outline">More Details </a></p>
                        </div>
                        <!-- .property-description -->


                    </article>
                    <!-- .property-post-odd -->

                </div>


                <?php

                if ( $number_of_featured_properties > $properties_count ) {
                if ( 0 == ( $properties_count % $totalProperties ) ) {
                ?><div class="row zero-horizontal-margin"><?php
                }
                }
                $properties_count++;

                endwhile;
                echo '</div>';
                wp_reset_postdata();
                ?>


            </div>


        </div>

            <!-- HOUSING FEATURED -->
            <div class="row section-padding tab-item tab-item--housing">
                <?php
                $column_class = 'col-md-6';
                if ($totalPropertiesHousing==1) {
                    $column_class = 'col-md-6';
                }
                $properties_count = 1;

                echo '<div class="row row-odd zero-horizontal-margin">';

                while ( $featured_housing_query->have_posts() ) :
                $featured_housing_query->the_post();

                $featured_housing = new inspiry_Property(get_the_ID());

                if ( 0 == ( $properties_count % 2 ) ) {
                    $odd_even_class = 'property-post-even';
                } else {
                    $odd_even_class = 'property-post-odd';
                }
                ?>
                <div class="<?php echo $column_class;?> custom-col-xs-12">

                    <article class="row property_listing hentry property-listing-home property-listing-one meta-item-half">

                        <div class="property-thumbnail zero-horizontal-padding col-lg-6">

                            <?php prcweb_thumbnail_featured(); ?>    </div>
                        <!-- .property-thumbnail -->

                        <div class="property-description clearfix col-lg-6">
                            <header class="entry-header">
                                <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                <div class="price-and-status">
                                    <span class="price"><?php echo esc_html( $featured_housing->get_price_without_postfix()); ?></span>
                                    <!--<?php
                                    $first_status_term = $featured_housing->get_taxonomy_first_term( 'property-status', 'all' );
                                    if ( $first_status_term ) {
                                        ?>
                            <a href="<?php the_permalink(); ?>">
                                <span class="property-status-tag"><?php echo esc_html( $first_status_term->name ); ?></span>
                            </a>
                            <?php
                                    }
                                    ?>-->

                                </div>
                            </header>
                            <p><?php prcweb_excerpt( 15 ); ?></p><hr>
                            <!-- .property-meta -->
                            <p>    <div class="listing_actions">

                                <div class="share_unit" style="display: none;">
                                    <a href="http://www.facebook.com/sharer.php?u=http://localhost/prc/properties/condo-in-financial-district/&amp;t=Condo+In+Financial+District" target="_blank" class="social_facebook"></a>
                                    <a href="http://twitter.com/home?status=Condo+In+Financial+District+http%3A%2F%2Flocalhost%2Fprc%2Fproperties%2Fcondo-in-financial-district%2F" class="social_tweet" target="_blank"></a>
                                    <a href="http://plus.google.com/share?url=http://localhost/prc/properties/condo-in-financial-district/" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="social_google"></a>
                                    <a href="http://pinterest.com/pin/create/button/?url=http://localhost/prc/properties/condo-in-financial-district/&amp;media=&amp;description=Condo+In+Financial+District" target="_blank" class="social_pinterest"></a>
                                </div>



                                <span class="share_list share_on" data-original-title="share"></span>
                                <span class="icon-fav icon-fav-off" data-original-title="add to favorites" data-postid="3701"></span>

                            </div>
                            <a href="<?php the_permalink(); ?>" class="white hover-white btn btn-outline">More Details </a></p>
                        </div>
                        <!-- .property-description -->


                    </article>
                    <!-- .property-post-odd -->

                </div>


            <?php

            if ( $number_of_featured_properties > $properties_count ) {
            if ( 0 == ( $properties_count % $totalProperties ) ) {
            ?><div class="row zero-horizontal-margin"><?php
                    }
                    }
                    $properties_count++;

                    endwhile;
                    echo '</div>';
                    wp_reset_postdata();
                    ?>


                </div>
                <?php
                endif;
                ?>
            </div>
</div>
        </div>





