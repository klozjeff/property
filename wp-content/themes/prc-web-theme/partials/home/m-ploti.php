<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/22/2017
 * Time: 12:43 PM
 */

global $prcweb_options;
?>
<section id="new-mploti" class="new-mploti">
         <div class="row z1 relative pb6 bg-gray" style="background-color:#fafafa;margin-top: 20px">
    <div class="container mx-auto z2 mnb6">
        <div class="md-flex flex-center mxn2">
            <div class="col-md-6 px2">
                <h3 class="red h2 mt0 mb4">M-Ploti</h3>
                <div class="mbn2">
                    <div class="py2">
                        <h3 class="mt0 mb1 slate h5">Invite and manage visitors</h3>
                        <p class="m0">If your company uses Envoy, you can invite visitors, view your visitor history and more—right from your phone. </p>
                    </div>
                    <div class="py2">
                        <h3 class="mt0 mb1 slate h5">Set notification preferences</h3>
                        <p class="m0">With Passport in your pocket, you can easily choose how you’d like to be notified when visitors arrive. </p>
                    </div>
                    <div class="py2">
                        <h3 class="mt0 mb1 slate h5">Make sign-in even easier</h3>
                        <p class="m0">Skip the typing and sign in with just a tap at any office that uses Envoy. Plus, collect a sharable stamp for each visit </p>
                    </div>
                </div>
                <div class="border-top py3 mt3 ">
                    <a href="passport/index.html">Learn more about M-Ploti →</a>
                </div>
            </div>
            <div class="col-md-2">

            </div>
            <div class="col-md-4 px2 md-px6">
                <img src="<?=get_template_directory_uri();?>/images/Images-1.png" class="mt5 md-mt0" style="transform: rotate(12deg);" alt="">

            </div>

        </div>
    </div>
         </div>
</section>
<!--
<section class="prc-mploti submit-property-one" style="background: url(<?= $prcweb_options[ 'prcweb_mploti_bg_image' ]['url'];?>) no-repeat center top;  background-size: cover;
    background-position: center;
    background-repeat: no-repeat;">
        <div class="container" style="max-width:100% !important">

            <div class="row col-sm-8 content-placeholders">
                <div class="content-placeholder">

                    <h3 class="prc-mploti-title"><?= $prcweb_options[ 'prcweb_mploti_title' ];?></h3>
                    <p>
                        <?= $prcweb_options[ 'prcweb_mploti_description' ];?>
                    </p>
                    <a class="btn-default hidden-sm hidden-md" href="http://localhost/property/property/emali-valley-phase-2/">More Details<i class="fa fa-angle-right"></i></a>
                </div>
                <div class="col-sm-4 call-to-action">
                    <div id=" lp-pom-box-30-color-overlay"></div>

                    <div class="call-to-action-image" id="call-to-action-image-29">
                        <div class="call-to-action-image-container" style="overflow: hidden;">
                            <img src="<?= $prcweb_options['prcweb_mploti_action_image']['url'];?>" alt=""></div>
                    </div>
                </div>
            </div>


        </div>
        <!-- .container
    </section> -->