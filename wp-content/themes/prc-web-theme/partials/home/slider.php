<?php
/*
 * Homepage properties slider
 */
global $prcweb_options;
$number_of_slides = intval( $prcweb_options[ 'prcweb_home_slides_number' ] );
if( !$number_of_slides ) {
    $number_of_slides = 3;
}

$slider_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_slides,
    'meta_query' => array(
        array(
            'key' => 'PRC_WEB_add_in_slider',
            'value' => 'yes',
            'compare' => 'LIKE',
        )
    )
);

$slider_query = new WP_Query( apply_filters( 'prcweb_slider_query_args', $slider_args ) );

if ( $slider_query->have_posts() ) {
    ?>
    <div class="homepage-slider slider-variation-three flexslider slider-loader">
        <ul class="slides">
            <?php
            while ( $slider_query->have_posts() ) {

                $slider_query->the_post();

                $slider_property = new Inspiry_Property(get_the_ID());

                $slider_image = $slider_property->get_slider_image();
				

                if ( !empty( $slider_image ) ) {
                    ?>
                    <li>
					<div class="slide-overlay hidden-xs container">
                    <div class="slide-inner-container">
                        <div class="slide-header">
                            <h3 class="slide-entry-title entry-title">
                                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo get_prcweb_custom_excerpt( get_the_title(), 6 ); ?></a>
                            </h3>
                            <div class="price-and-status">
                                <span class="price"><?php $slider_property->price(); ?></span>
                              <!--  <a href="#">
                                    <span class="property-status-tag">Monthly Installments</span>
                                </a>-->
                            </div>
                        </div>
                        <p class="hidden-sm"><?php prcweb_excerpt( 30 ); ?></p>
                        <a class="btn-default hidden-sm hidden-md" href="<?php the_permalink(); ?>">More Details<i class="fa fa-angle-right"></i></a>
                    </div>
                </div>
                        <!--<div class="slide-overlay hidden-xs container">
                            <div class="">
                                <div class="slide-inner-container">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h3 class="slide-entry-title entry-title">
                                                <a href="<?php the_permalink(); ?>" rel="bookmark"><?php echo get_prcweb_custom_excerpt( get_the_title(), 6 ); ?></a>
                                            </h3>
                                            <div class="price-and-status">
                                                <span class="price"><?php $slider_property->price(); ?></span>
                                                <?php
                                                $first_status_term = $slider_property->get_taxonomy_first_term( 'property-status', 'all' );
                                                if ( $first_status_term ) {
                                                    ?>
                                                    <a href="<?php echo esc_url( get_term_link( $first_status_term ) ); ?>">
                                                        <span class="property-status-tag"><?php echo esc_html( $first_status_term->name ); ?></span>
                                                    </a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <a href="<?php the_permalink(); ?>">
                            <img src="<?php echo esc_url( $slider_image ); ?>" alt="<?php the_title(); ?>" />
                        </a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
    <?php
} else {
    get_template_part( 'partials/header/banner' );
}