<!--<div id="testimonials" class="testimony">
        <section class="xs-hide sm-hide container mx-auto rounded">
            <div class="mxn6 overflow-hidden rounded shadow">
                <div class="flex bg-slate" style="background-image: url(<?= get_template_directory_uri(); ?>/images/bjorn.jpg); background-position: center right; background-size: cover;">
                    <div class="col-6 p6 bg-quote-red">
                        <blockquote class="mb5">
                            <h4 class="white caps mb2 m0 h3">Envoy at POPSUGAR</h4>
                            <p class="white hanging-quote block">“Envoy was up and running at POPSUGAR’s three offices within days. It’s clear the team at Envoy has thought about the entire experience, from setup to finish.”</p>
                            <cite class="slate hanging-emdash block">—Bjorn Pave, Senior Director of Information Technology</cite>
                        </blockquote>
                        <a href="case-studies/popsugar/index.html" class="white hover-white btn btn-outline">Read the case study</a>
                    </div>
                </div>
            </div>
        </section>

    </div>-->
<section id="testimonials" class="brand-testimonials">
    <div class="container"><div class="row bg-white"><div class="col-md-4 nopadding">
                <img alt="" src="<?= get_template_directory_uri();?>/images/web banner.jpg" class="img-responsive desktop">
                <img alt="" src="<?= get_template_directory_uri();?>images/left-col-bg-m.jpg" class="img-responsive mobile">
                <div class="carousel-caption text-center white desktop">
                    <p class="testimonial">"I received my property title deed within a record 3 working Days."</p>
                    <p class="testimonial-by">- David G.<span>, Jane Doe,Corporate Marketing Executive-Zinc Inc</span></p>
                </div>
            </div>
            <div class="col-md-8 row-cta-container">
                <div class="row-cta desktop">
                    <div><h3>Get Quality & Affordable Land</h3>
                        <p>We you more when looking for your next land. More data. More properties. More neighborhood information. See for yourself.</p>
                        <a href="#" class="btn">Start Your Journey</a>
                    </div>
                </div>
                <div class="row-cta">
                    <div><div class="desktop">
                            <h3>Know more than the average buyer</h3>
                            <p>The more information you have at your fingertips, the more confident you'll be that it's the home for you.</p>
                        </div>
                        <div class="mobile row">
                            <div class="row-height"><a href="pages/buying.html"><div class="col-sm-2 col-xs-3 col-height col-top"><i class="sprite sprite-icon-house"></i></div>
                                    <div class="col-sm-9 col-xs-8 col-height col-top">
                                        <h3 class="nopadding">Know more than the average buyer</h3>
                                        <p>See why buyers choose Berkshire Hathaway HomeServices.</p>
                                    </div>
                                    <div class="col-sm-1 col-xs-1 col-height col-middle"><i class="sprite-icon-arrow-right pull-right mobile"></i></div>
                                </a>
                            </div>
                        </div>
                        <div class="row text-center img-text desktop"><div class="col-md-4">
                                <div class="img">
                                    <i class="sprite sprite-icon-tag"></i>
                                </div>
                                <div class="itext">Find the value of a Land</div>
                            </div>
                            <div class="col-md-4">
                                <div class="img">
                                    <i class="sprite sprite-icon-people"></i>
                                </div>
                                <div class="itext">Explore Unique Places</div>
                            </div>
                            <div class="col-md-4"><div class="img">
                                    <i class="sprite sprite-icon-key"></i>
                                </div>
                                <div class="itext">Get Yourself Future</div>
                            </div>
                        </div>
                        <a href="pages/buying.html" class="btn desktop">Buy smarter</a>
                    </div>
                </div>
                <div class="row-cta noborder" style="margin-bottom: 0px;"><div><div class="desktop">
                            <h3>Work with the best</h3>
                            <p>Our team are local market experts. Access to more information than other land sellers nationwide - down to the neighborhood - makes them good to know.</p>
                        </div>
                        <div class="mobile row"><div class="row-height"><a href="pages/agent-office-search.html"><div class="col-sm-2 col-xs-3 col-height col-top"><i class="sprite sprite-icon-star mobile"></i></div>
                                    <div class="col-sm-9 col-xs-8 col-height col-top">
                                        <h3 class="nopadding">Work with the best</h3>
                                        <p>Buying or selling, it's time to find a local agent.</p>
                                    </div>
                                    <div class="col-sm-1 col-xs-1 col-height col-middle"><i class="sprite-icon-arrow-right pull-right mobile"></i></div>
                                </a>
                            </div>
                        </div>
                        <a href="pages/agent-office-search.html" class="btn desktop">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

