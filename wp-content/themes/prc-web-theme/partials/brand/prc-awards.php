<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/22/2017
 * Time: 3:58 PM
 */
global $prcweb_options;
$number_of_awards = intval( $prcweb_options[ 'prcweb_home_partners_number' ] );
if( !$number_of_awards ) {
    $number_of_awards = 3;
}
$awards_query_args = array(
    'post_type' => 'awards',
    'posts_per_page' => $number_of_awards
);

$awards_query = new WP_Query( $awards_query_args );

if ( $awards_query->have_posts() ) :
?>
    <div id="testimonials" class="testimony">
        <section class="xs-hide sm-hide container mx-auto rounded">
            <div class="mxn6 overflow-hidden rounded shadow">
                <div class="flex bg-slate" style="background-image: url(<?=get_template_directory_uri();?>/images/bjorn.jpg); background-position: center right; background-size: cover;">
                    <div class="col-6 p6 bg-quote-red">
                        <blockquote class="mb5">
                            <h4 class="white caps mb2 m0 h3">Envoy at POPSUGAR</h4>
                            <p class="white hanging-quote block">“Envoy was up and running at POPSUGAR’s three offices within days. It’s clear the team at Envoy has thought about the entire experience, from setup to finish.”</p>
                            <cite class="slate hanging-emdash block">—Bjorn Pave, Senior Director of Information Technology</cite>
                        </blockquote>
                        <a href="case-studies/popsugar/index.html" class="white hover-white btn btn-outline">Read the case study</a>
                    </div>
                </div>
            </div>
        </section>

    </div>
   <!-- <div class="page-section cs-page-sec-611629  nopadding cs-nomargin">
        <script type="text/javascript">if (jQuery("#inline-style-functions-inline-css").length == 0) {
                jQuery("head").append('<style id="inline-style-functions-inline-css" type="text/css"></style>');
            }</script>
        <script type="text/javascript">jQuery("#inline-style-functions-inline-css").append(".cs-page-sec-611629{padding-top: 81px;background: #ffffff;}");</script>
        <!-- Container Start
        <div class="container ">
            <div class="row">
                <div class="section-fullwidth col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                            <div class="icons-boxes-list left">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="icon-boxes modern  left">
                                            <div class="img-holder"><a href="#">
                                                    <figure><img
                                                                src="http://homevillas.chimpgroup.com/demo-v6/wp-content/uploads/Icon-03.png"
                                                                alt="Best Of Emerging Technology"
                                                                data-pagespeed-url-hash="2989496256"
                                                                onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                                    </figure>
                                                </a></div>
                                            <div class="text-holder"><h3 style="color:#444444 !important;"><a href="#"
                                                                                                              style="color:#444444 !important;">Best
                                                        Of Emerging Technology</a></h3>
                                                <p>Choose from professionally designed themes that work across all
                                                    devices. Our real estate website designs</p></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="icon-boxes modern  left">
                                            <div class="img-holder"><a href="#">
                                                    <figure><img
                                                                src="http://homevillas.chimpgroup.com/demo-v6/wp-content/uploads/Icon-02.png"
                                                                alt="Grow Your Business"
                                                                data-pagespeed-url-hash="2694996335"
                                                                onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                                    </figure>
                                                </a></div>
                                            <div class="text-holder"><h3 style="color:#444444 !important;"><a href="#"
                                                                                                              style="color:#444444 !important;">Grow
                                                        Your Business</a></h3>
                                                <p>Choose from professionally designed themes that work across all
                                                    devices. Our real estate website designs</p></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="icon-boxes modern  left">
                                            <div class="img-holder"><a href="#">
                                                    <figure><img
                                                                src="http://homevillas.chimpgroup.com/demo-v6/wp-content/uploads/Icon-01.png"
                                                                alt="Create A Unique Online Presence"
                                                                data-pagespeed-url-hash="2400496414"
                                                                onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                                    </figure>
                                                </a></div>
                                            <div class="text-holder"><h3 style="color:#444444 !important;"><a href="#"
                                                                                                              style="color:#444444 !important;">Create
                                                        A Unique Online Presence</a></h3>
                                                <p>Choose from professionally designed themes that work across all
                                                    devices. Our real estate website designs</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                            <div class="img-frame classic has-border has-shadow">
                                <figure><iframe width="550" height="480" src="https://www.youtube.com/embed/a0T2dzpPrBs" frameborder="0" allowfullscreen></iframe></figure>
                            </div>
                        </div>
                    </div><!-- end section row </div>
            </div>

            <div class="hr"></div>
        </div>

    </div>-->

<section id="awards" class="prc-awards submit-property-one" style="background: #ffffff">

    <div class="container">
        <div class="row">
        <header class="section-header">
            <h3 class="section-title">Awards</h3>

        <div class="prc-awards-carousel-nav carousel-nav">
                <a class="carousel-prev-item prev">
                    <svg xmlns="http://www.w3.org/2000/svg" class="arrow-container" width="32" height="52" viewBox="0 0 32 52">
                        <g class="left-arrow" fill="#07AA9C">
                            <path opacity=".5" d="M31.611 7.646l-6.787-7.057-24.435 25.406 6.787 7.057z"/>
                            <path d="M.389 26.006l6.787-7.058 24.435 25.406-6.787 7.057z"/>
                        </g>
                    </svg>
                </a>
                <a class="carousel-next-item next">
                    <svg xmlns="http://www.w3.org/2000/svg" class="arrow-container" width="32" height="52" viewBox="0 0 32 52">
                        <g class="right-arrow" fill-rule="evenodd" clip-rule="evenodd" fill="#07AA9C">
                            <path d="M.388 44.354l6.788 7.057 24.436-25.406-6.788-7.057-24.436 25.406z"/>
                            <path opacity=".5" d="M31.612 25.994l-6.788 7.058-24.436-25.406 6.788-7.057 24.436 25.405z"/>
                        </g>
                    </svg>
                </a>
            </div>
        </header>
        <div class="col-sm-3 prc-awards-placeholder" style="margin-top: 10px !important;">
            <p style="text-align: left !important;color: #444444">
                The team will deliver value in an honest and ethical manner to all clients, by maintaining the highest moral standards.                    </p>
            <a style="float: left !important;" class="btn-default hidden-sm hidden-md" href="http://localhost/property/property/emali-valley-phase-2/">Read More<i class="fa fa-angle-right"></i></a>
        </div>
        <?php
        while ($awards_query->have_posts()):
            $awards_query->the_post();
        $award_id=get_the_ID();

        ?>
            <div class="col-sm-3 prc-awards-placeholder">
                <div class="image-wrapper">
                    <a href="<?php the_permalink(); ?>"><img src="<?php echo wp_get_attachment_url( get_post_meta(get_the_ID(), 'PRC_WEBaward_image',true));?> " height="100px !important" alt="Icon"></a>
                </div>
                <h3 class="prc-awards-title"><?php the_title(); ?></h3>
                <p>
                    <?php echo '<span style="color:#000 !important;">' .get_post_meta(get_the_ID(), 'PRC_WEBaward_position',true).'</span><br/>' .get_post_meta(get_the_ID(), 'PRC_WEBaward_category',true);  
                    if (get_post_meta(get_the_ID(),'PRC_WEBaward_position',true)=='Winner' || get_post_meta(get_the_ID(),'PRC_WEBaward_position',true)=='Best Company'):
                        $perc='100%';
                    elseif (get_post_meta(get_the_ID(),'PRC_WEBaward_position',true)=='Ist Runners Up'):
                      $perc='80%';
                    else:
                        $perc='52%';
                    endif;
                    ?>
                <div class="star-ratings-sprite"><span style="width:<?php echo $perc;?>" class="star-ratings-sprite rating"></span></div></p>
            </div>
            <?php endwhile;
            ?>





    </div>
    </div>
</section>
<?php
endif;
?>
