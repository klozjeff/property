<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 9/21/2017
 * Time: 5:15 PM
 */
?>
<div id="featured" class="container">
    <div class="featured-properties-three">

        <div class="col-md-6">
            <header class="section-header">
                <h3 class="section-title" style="border-bottom: 0px solid #000 !important;margin-bottom:5px !important; ">PRC Kilimo Biashara</h3>

            </header>
             <?php while ( have_posts() ) : the_post();



                    the_content();



                endwhile; // End of the loop. ?>
        </div>
        <div class="col-md-6">
            <?php //echo do_shortcode("[huge_it_portfolio id='2']"); ?>

        </div>
    </div>
</div>
