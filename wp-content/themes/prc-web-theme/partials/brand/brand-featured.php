<?php
global $prcweb_options,$post;
$number_of_featured_properties = intval( $prcweb_options[ 'prcweb_brand_featured_number_3' ] );
if( !$number_of_featured_properties ) {
    $number_of_featured_properties = 4;
}

$pagename=strtolower($post->post_title);

// Featured properties query arguments
$featured_properties_args = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_featured_properties,
    'tax_query' => array(
        'relation' => 'AND',
        array(
            'key' => 'PRC_WEB_featured',
            'value' => 1,
            'compare' => '=',
            'type'  => 'NUMERIC'
        ),
        array(
            'taxonomy' => 'property-type',
            'field'    => 'slug',
            'terms'    => array($pagename),
            'include_children' => true,
            'operator' => 'IN'
        ),
        array(
            'taxonomy' => 'property-status',
            'field'    => 'slug',
            'terms'    => array('available','sold out'),
            'include_children' => true,
            'operator' => 'IN'
        ),
    )
);

$featured_properties_query = new WP_Query( $featured_properties_args );

if ( $featured_properties_query->have_posts() ) :
?>
<div id="featured" class="container">
<div class="featured-properties-three">

  <?php

        if ( ! empty( $prcweb_options[ 'prcweb_brand_featured_title' ] ) ) {
            ?>
            <header class="section-header">
                <h3 class="section-title"><?php echo esc_html( $prcweb_options[ 'prcweb_brand_featured_title' ] ); ?></h3>

            </header>
            <?php
        }
        ?>
		</div>
		<?php
		
            /*
             * Columns
             */
            $columns_count = intval( $prcweb_options[ 'prcweb_brand_featured_columns' ] );
            if( !$columns_count ) {
                $columns_count = 2;
            }
            $column_class = 'col-md-6';
            if ( 1 == $columns_count ) {
                $column_class = 'col-md-12';
            }
            $properties_count = 1;
			
echo '<div class="row row-odd zero-horizontal-margin">';

            while ( $featured_properties_query->have_posts() ) :
            $featured_properties_query->the_post();

                $featured_property = new inspiry_Property(get_the_ID());

                if ( 0 == ( $properties_count % 2 ) ) {
                    $odd_even_class = 'property-post-even';
                } else {
                    $odd_even_class = 'property-post-odd';
                }

                ?>

                <div class="col-xs-6 custom-col-xs-12">

                    <article class="row property_listing hentry  property-listing-home property-listing-one meta-item-half">

                        <div class="property-thumbnail zero-horizontal-padding col-lg-6">
                            <?php prcweb_thumbnail_featured(); ?>    </div>
                        <!-- .property-thumbnail -->

                        <div class="property-description clearfix col-lg-6">
                            <header class="entry-header">
                                <h3 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
                                <div class="price-and-status">
                                    <span class="price"><?php echo esc_html( $featured_property->get_price_without_postfix()); ?></span>
                                    <!--<?php
                                    $first_status_term = $featured_property->get_taxonomy_first_term( 'property-status', 'all' );
                                    if ( $first_status_term ) {
                                        ?>
                            <a href="<?php the_permalink(); ?>">
                                <span class="property-status-tag"><?php echo esc_html( $first_status_term->name ); ?></span>
                            </a>
                            <?php
                                    }
                                    ?>-->

                                </div>
                            </header>
                            <p><?php prcweb_excerpt( 15 ); ?></p><hr>
                            <!-- .property-meta -->
                            <p>
                            <div class="listing_actions">

                                <div class="share_unit" style="display: none;">
                                    <a href="http://www.facebook.com/sharer.php?u=http://localhost/prc/properties/condo-in-financial-district/&amp;t=Condo+In+Financial+District" target="_blank" class="social_facebook"></a>
                                    <a href="http://twitter.com/home?status=Condo+In+Financial+District+http%3A%2F%2Flocalhost%2Fprc%2Fproperties%2Fcondo-in-financial-district%2F" class="social_tweet" target="_blank"></a>
                                    <a href="http://plus.google.com/share?url=http://localhost/prc/properties/condo-in-financial-district/" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank" class="social_google"></a>
                                    <a href="http://pinterest.com/pin/create/button/?url=http://localhost/prc/properties/condo-in-financial-district/&amp;media=&amp;description=Condo+In+Financial+District" target="_blank" class="social_pinterest"></a>
                                </div>



                                <span class="share_list share_on" data-original-title="share"></span>
                                <span class="icon-fav icon-fav-off" data-original-title="add to favorites" data-postid="3701"></span>

                            </div>
                            <a href="<?php the_permalink(); ?>" class="white hover-white btn btn-outline">More Details </a></p>
                        </div>
                        <!-- .property-description -->


                    </article>
                    <!-- .property-post-odd -->

                </div>
		
		
	    <?php

                if ( $number_of_featured_properties > $properties_count ) {
                    if ( 0 == ( $properties_count % $columns_count ) ) {
                        ?></div><div class="row zero-horizontal-margin"><?php
                    }
                }
                $properties_count++;

            endwhile;
echo '</div>';
            wp_reset_postdata();
            ?>
		

</div>
<?php
endif;
?>