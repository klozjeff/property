<?php
/*
 * Banner Area
 */

global $prcweb_options;
$prcweb_banner_title = null;
$prcweb_banner_sub_title = null;
$revolution_slider_alias = null;
$skip_banner = false;
global $pageName; //$pageName from body.php
$pagename = get_query_var('pagename');


if (  is_page_template( 'page-templates/home.php' ) ||  is_page_template('page-templates/land.php') ||  is_page_template( 'page-templates/housing.php' ) ||  is_page_template( 'page-templates/company.php' ) ||  $pagename=='blog'  || is_single('property')) {      // Home page
    // leave empty

} elseif (is_page_template( 'page-templates/properties-search.php' )) {   // Search page

    if ( $prcweb_options[ 'prcweb_search_module_below_header' ] != 'google-map' ) {
        // initial work for revolution slider
        $revolution_slider_alias = get_post_meta( get_the_ID(), 'PRC_WEB_rev_slider_alias', true );
        if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
            if ( $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
                $skip_banner = true;    // skip revolution slider if header variation is not 1
            }
        } else {
            $prcweb_banner_title = get_the_title();
        }
    } else {
        if ( $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
            $prcweb_banner_title = get_the_title();
        }
    }
} elseif ( is_home() ) {

    $blog_page_id = get_option('page_for_posts');
    if ( $blog_page_id ) {
        $revolution_slider_alias = get_post_meta( $blog_page_id, 'PRC_WEB_rev_slider_alias', true );
        if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
            if ( $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
                $skip_banner = true;    // skip revolution slider if header variation is not 1
            }
        } else {
            $prcweb_banner_title = get_the_title( $blog_page_id );
        }
    }

} elseif ( is_page() || is_singular( 'agent' ) ) {

    // display map is priority and if map is being displayed then revolution slider cannot be displayed
    $display_google_map = get_post_meta( get_the_ID(), 'prcweb_display_google_map', true );
    if ( !$display_google_map ) {

        // initial work for revolution slider
        $revolution_slider_alias = get_post_meta( get_the_ID(), 'PRC_WEB_rev_slider_alias', true );
        if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
            if ( $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
                $skip_banner = true;    // skip revolution slider if header variation is not 1
            }
        } else {
            $prcweb_banner_title = get_the_title();
        }
    }

} elseif ( is_singular( 'post' ) ) {

    $revolution_slider_alias = get_post_meta( get_the_ID(), 'PRC_WEB_rev_slider_alias', true );
    if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
        if ( $prcweb_options[ 'prcweb_header_variation' ] != '1' ) {
            $skip_banner = true;    // skip revolution slider if header variation is not 1
        }
    } else {
        $prcweb_banner_title = apply_filters( 'the_title', get_the_title( get_option( 'page_for_posts' ) ) );
    }

} elseif ( is_single() ) {
    $prcweb_banner_title = get_the_title();

} elseif ( is_search() ) {
    $prcweb_banner_title = sprintf( __( 'Search Results for: %s', 'prcweb' ), get_search_query() );

} elseif ( is_author() ) {
    global $wp_query;
    $current_author = $wp_query->get_queried_object();
    if( !empty( $current_author->display_name ) ) {
        $prcweb_banner_title = $current_author->display_name;
    }

} elseif ( is_archive() ) {
    $prcweb_banner_title = get_the_archive_title();
    $prcweb_banner_sub_title = get_the_archive_description();

}

/*
 * Skip the banner if revolution slider is being displayed
 */
if ( !$skip_banner ) {
    $prcweb_banner_bg_image =prcweb_get_banner_image();
    $prcweb_banner_bg_color = '#FFF';

    $banner_variation_class = '';
    if ( $prcweb_options[ 'prcweb_header_variation' ] == '1' ) {
        $banner_variation_class = 'add-padding-top';
    }
    ?>
    <div class="page-head <?php echo esc_attr( $banner_variation_class ); ?>"
         style="<?php prcweb_generate_background( $prcweb_banner_bg_color, $prcweb_banner_bg_image ); ?>">
      
            <div class="container">
                <div class="page-head-content">
                    <?php
                    if ( !empty( $prcweb_banner_title ) ) {
                        if ( is_single() ) {
                            ?><h2 class="page-title"><?php echo esc_html( $prcweb_banner_title ); ?></h2><?php
                        } else {
                            ?><h1 class="page-title"><?php echo esc_html( $prcweb_banner_title ); ?></h1><?php
                        }
                    }

                    // only for archive pages
                    if ( !empty( $prcweb_banner_sub_title ) ) {
                        ?><p class="page-description"><?php echo esc_html( $prcweb_banner_sub_title ); ?></p><?php
                    }
                    ?>
                </div>
            </div>
          
    </div><!-- .page-head -->
    <?php
}

/*
 * Display revolution slider of all conditions are met
 */
$revolution_slider_alias='slider1';
if ( is_home() || is_singular( 'agent' ) || is_singular( 'post' ) ) {

    if ( function_exists( 'putRevSlider' ) && ( !empty( $revolution_slider_alias ) ) ) {
        echo '<div class="inspiry-revolution-slider">';
        putRevSlider( $revolution_slider_alias );
        echo '</div>';
    }
}

