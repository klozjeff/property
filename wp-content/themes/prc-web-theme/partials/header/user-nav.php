
 <ul class="user-nav">
<?php
global $prcweb_options;
if(is_user_logged_in()):
global $current_user;
 get_currentuserinfo();
  $edit_profile_url = "";
  $my_properties_url = "";
  $favorites_url = "";

         if(!empty($prcweb_options['prcweb_header_email'])):?>
           <li><a href="#"><i class="fa fa-envelope"></i><?php echo esc_html($prcweb_options['prcweb_header_email']);?></a></li>
             <?php endif; if(!empty($prcweb_options['prcweb_header_phone'])):?>
             <li><a href="#"><i class="fa fa-phone"></i><?php echo esc_html($prcweb_options['prcweb_header_phone']);?></a></li>
		   <?php endif;
		    if(!empty($prcweb_options['prcweb_header_address'])):?>
            <li><a href="#"><i class="fa fa-map-marker"></i><?php echo esc_html($prcweb_options['prcweb_header_address']);?></a></li>
			<?php endif;?>
            <li><a class="submit-property-link" href="<?php echo esc_url(home_url('/my_profile')) ?>"><i class="fa fa-user"></i><?php echo $current_user->display_name;?></a></li>
         <li><a href="<?php echo wp_logout_url( esc_url( home_url('/') ) ); ?>"><i class="fa fa-sign-out"></i><?php _e( 'Logout', 'prcweb' ); ?></a></li>
    

<?php
else:

?>
    
            <?php if(!empty($prcweb_options['prcweb_header_email'])):?>
           <li><a href="#"><i class="fa fa-envelope"></i><?php echo esc_html($prcweb_options['prcweb_header_email']);?></a></li>
<?php endif; if(!empty($prcweb_options['prcweb_header_phone'])):?>
    <li><a href="#"><i class="fa fa-phone"></i><?php echo esc_html($prcweb_options['prcweb_header_phone']);?></a></li>

<?php endif;
		    if(!empty($prcweb_options['prcweb_header_address'])):?>
            <li><a href="#"><i class="fa fa-map-marker"></i><?php echo esc_html($prcweb_options['prcweb_header_address']);?></a></li>
			<?php endif;?>
            <li><a class="login-register-link" href="#login-modal" data-toggle="modal"><i class="fa fa-sign-in"></i>Login / Sign up</a></li>
       
<?php 	
endif;
?>
        </ul>