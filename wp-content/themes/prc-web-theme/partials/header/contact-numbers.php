<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 10:35 AM
 */

global $prcweb_options;

if (!empty( $prcweb_options['prcweb_header_phone'] ) ) {
    $contact_icon = 'icon-phone';

    if( $prcweb_options[ 'prcweb_header_variation' ] == '3' ) {
        $contact_icon = 'icon-phone-two';
    }
    ?>
    <div class="contact-number">
        <?php include( get_template_directory() . '/images/svg/' . $contact_icon .  '.svg' ); ?>
        <span class="desktop-version hidden-xs"><?php echo esc_html( $prcweb_options['prcweb_header_phone'] ); ?></span>
        <a class="mobile-version visible-xs-inline-block" href="tel://<?php echo esc_attr( $prcweb_options['prcweb_header_phone'] ); ?>">
		<?php echo esc_html( $prcweb_options['$prcweb_header_phone'] ); ?></a>
    </div><!-- .contact-number -->
    <?php
}
?>