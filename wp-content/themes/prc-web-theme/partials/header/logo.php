
<div id="site-logo" class="site-logo">
                    <div class="logo-inner-wrapper">
					<?php

					global $prcweb_options;
					$prcweb_site_name  = get_bloginfo( 'name' );
                   $prcweb_tag_line   = get_bloginfo ( 'description' );
				   if(!empty($prcweb_options['prcweb_logo']) && !empty( $prcweb_options['prcweb_logo']['url'] )):
				   /*Load Image Logo*/
				    if(is_page_template( 'page-templates/land.php' )):
                        echo  '<a href="'.esc_url( home_url('/') ).'"><img src="'.esc_url( $prcweb_options['prcweb_land_logo']['url'] ).'" alt="'.esc_attr( $prcweb_site_name ).'" style="margin-top: 10px !important;"/></a>';

                    elseif(is_page_template( 'page-templates/housing.php' )):
                       echo  '<a href="'.esc_url( home_url('/') ).'"><img src="'.esc_url( $prcweb_options['prcweb_housing_logo']['url'] ).'" alt="'.esc_attr( $prcweb_site_name ).'" style="width:150px !important"/></a>';
                    elseif(is_page_template( 'page-templates/m-ploti.php' )):
                        echo  '<a href="'.esc_url( home_url('/') ).'"><img src="'.esc_url( $prcweb_options['prcweb_mploti_logo']['url'] ).'" alt="'.esc_attr( $prcweb_site_name ).'" style="margin-top: 10px !important;width:150px !important"/></a>';
                    elseif(is_page_template( 'page-templates/kilimo.php' )):
                        echo  '<a href="'.esc_url( home_url('/') ).'"><img src="'.esc_url( $prcweb_options['prcweb_kilimo_logo']['url'] ).'" alt="'.esc_attr( $prcweb_site_name ).'" style="width:150px !important"/></a>';

                    else:
                       echo  '<a href="'.esc_url( home_url('/') ).'"><img src="'.esc_url( $prcweb_options['prcweb_logo']['url'] ).'" alt="'.esc_attr( $prcweb_site_name ).'"/></a>';
					   endif;
				    else:
					   /** Load Text Based Image**/
					   if ( is_front_page() || is_home() || is_page_template( 'page-templates/home.php' ) ) {
                ?><h1 class="site-title"><a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><?php echo esc_html( $prcweb_site_name ); ?></a></h1><?php
            } else {
                ?><h2 class="site-title"><a href="<?php echo esc_url( home_url('/') ); ?>" rel="home"><?php echo esc_html( $prcweb_site_name ); ?></a></h2><?php
            }
					   endif;
					   
					   ?>
                    </div>
                </div>
