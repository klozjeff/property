<?php
if (is_page_template( 'page-templates/gallery.php' ) || is_page_template( 'page-templates/blog.php' ) || is_page_template( 'page-templates/company.php' ) )
{
   $postion="position:relative !important";
}
else{
    $postion="";
}
?>
<header class="site-header header header-variation-one" style="<?php echo $postion; ?>">
    <div class="header-container">

        <div class="row zero-horizontal-margin">
            <div class="left-column">
			  <?php get_template_part( 'partials/header/logo' ); ?>
                
            </div>
            <!-- .left-column -->
            <div class="right-column">

                <!-- .header-top -->
                <div class="header-bottom clearfix" style="background: #FFFFFF !important;">
                   <?php get_template_part( 'partials/header/menu-blog' ); ?>
                   
                </div>
                <!-- .header-bottom -->
            </div>
            <!-- .right-column -->
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->
</header>
<!-- .site-header -->


