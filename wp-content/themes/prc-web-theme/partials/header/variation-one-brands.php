<header class="site-header header header-variation-one">
    <div class="header-container">

        <div class="row zero-horizontal-margin">
            <!-- .left-column -->
            <div class="header-column-custom">
                <div class="header-top hidden-xs hidden-sm clearfix">

                     <?php get_template_part( 'partials/header/user-nav' ); ?>
                   
                </div>
                <!-- .header-top -->
                <div class="header-bottom clearfix" style="background: #ffffff !important;">
				  <?php get_template_part( 'partials/header/logo' ); ?>
                   <?php get_template_part( 'partials/header/menu' ); ?>
                   
                </div>
                <!-- .header-bottom -->
            </div>
            <!-- .right-column -->
        </div>
        <!-- .row -->
    </div>
    <!-- .container -->
</header>
<!-- .site-header -->


