 <nav id="site-main-nav" class="site-main-nav">
                        <?php     wp_nav_menu( array(
                            'theme_location' => 'primary',
                            'menu'   => 'top-menu',
                            'menu_class' => 'main-menu',
                            'walker' => new PRC_Walker_Nav_Menu()
                        ) );?>
                        <ul class="main-menu">
                           <?php if(is_user_logged_in()):?>
                               <li>
                                   <a href="<?php echo esc_url(home_url('/my-profile'));?>">My Account</a>
                                   <ul class="sub-menu">
                                       <li><a href="<?php echo esc_url(home_url('/account'));?>">Profile</a></li>
                                       <li><a href="<?php echo esc_url(home_url('/my-properties'));?>">Transactions</a></li>
                                       <li><a href="<?php echo esc_url(home_url('/statements'));?>">Statements</a></li>
                                       <li><a href="<?php echo esc_url(home_url('/reports'));?>">Reports</a></li>
                                   </ul>
                               </li>
                            <?php else: ?>
                               <li  class="prc_online_li"><a href="contact.html"><span class="prc_online_widget"> LIVE CHAT | ONLINE BUYING </span></a></li>
                           <?php endif; ?>
     <li><a href="contact.html"><i class="fa fa-search" style="font-size:19px" ></i></a></li>
                        </ul>
                    </nav>