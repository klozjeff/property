/**
 * Created by Web Designer on 6/16/2017.
 */
/*
 Checkout Page Functions
 1.Save Cart Items
 2.Compute Interest
 3.Payment Plan
 */
$ = jQuery;

function computeInterest(cartid)
{
    var original_price = cleanvalue($("#originalprice_"+cartid).text());
    var down_payment = cleanvalue($("#depositamount_"+cartid).val());
    var payment_period = cleanvalue($("#payment_plan_"+cartid).val());

    if(payment_period == "")
        return;

    if(down_payment == "")
    {
        alert("Please fill in a down payment amount.");
        return false;
    }

    if((down_payment/original_price)<0.3)
    {
        alert("Downpayment must be at least 30% of original price.");
        return false;

    }

    var interest = null;
    var monthlyinstallments = null;
    var newprice = null;
    var interest_rate = 0.2;

    var total_interest = null;
    var total_new_price = null;
    var total_deposit = null;

    interest = Math.round(((interest_rate * (original_price - down_payment))/12 ) * payment_period);

    monthlyinstallments = Math.round((interest +  (original_price-down_payment))/payment_period);

    newprice = +original_price + +interest;

    $("#interest_"+cartid).html(numberWithCommas(interest));
    $("#input_interest_"+cartid).val(numberWithCommas(interest));
    $("#installmentamount_"+cartid).html(numberWithCommas(monthlyinstallments));
    $("#installment_amount_"+cartid).val(numberWithCommas(monthlyinstallments));
    $("#newprice_"+cartid).html(numberWithCommas(newprice));
    $("#input_newprice_"+cartid).val(numberWithCommas(newprice));

    $(".interest").each(function() {
        total_interest += parseInt(cleanvalue($(this).text()));
    });

    $("#total_interest").html("KES: "+numberWithCommas(total_interest));


    $(".new_price").each(function() {
        total_new_price += parseInt(cleanvalue($(this).text()));
    });

    $(".deposit").each(function() {
        total_deposit += parseInt(cleanvalue($(this).val()));
    });

    $("#total_new_price").html("KES: "+numberWithCommas(total_new_price));
    $("#total_new_price_field").val(numberWithCommas(total_new_price));
    $("#total_deposit_field").val(numberWithCommas(total_deposit));





}

function cleanvalue(value)
{
    return value.trim().replace(",", "");

}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
$(document).ready(function ($) {
    "use-strict";
    if ( jQuery().validate && jQuery().ajaxSubmit ) {
        var form_loader = $('#ajax-loader');
        var form_message = $('#form-message');
        var errors_container = $( '#form-errors' );
        var ajaxURL='http://localhost/property/wp-admin/admin-ajax.php';
        // Edit User Profile Form
        var save_items = {
            url: ajaxURL,
            type: 'post',
            dataType: 'json',
            timeout: 30000,
            beforeSubmit: function( formData, jqForm, options ){
                form_loader.fadeIn();
                form_message.empty().fadeOut();
                errors_container.empty().fadeOut();
            },
            success: function( response, status, xhr, $form ){
                form_loader.fadeOut();
                if ( response.success ) {
                    form_message.html( response.message).fadeIn();
                    document.location.href = response.redirect;
                } else {

                        errors_container.append( '<li>' + response.message+ '</li>' );

                    errors_container.fadeIn();
                }
            }
        };

        $( '#prcweb-save-items' ).validate({
            submitHandler: function( form ) {
                $( form ).ajaxSubmit( save_items );
            }
        });

    }//End Validate

});
