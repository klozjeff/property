/**
 * Created by Web Designer on 6/12/2017.
 */
jQuery(document).ready(function($){

    var cartWrapper = $('.cd-cart-container');
    //product id - you don't need a counter in your real project but you can use your real product id
    var productId = 0;

    if( cartWrapper.length > 0 ) {        //store jQuery objects
        var cartBody = cartWrapper.find('.body')
        var cartList = cartBody.find('ul').eq(0);
        var cartTotal = cartWrapper.find('.checkout').find('span');
        var cartTrigger = cartWrapper.children('.cd-cart-trigger');
        var cartCount = cartTrigger.children('.count')
        var addToCartBtn = $('.cd-add-to-cart');
        var undo = cartWrapper.find('.undo');
        var undoTimeoutId;

        //add product to cart
        $(document).on('click','.cd-add-to-cart',function(event){
            var sess=$(this);

            var data = {
                action: 'is_user_logged_in'
            };
            var ajaxurl='http://localhost/property/wp-admin/admin-ajax.php';

            jQuery.post(ajaxurl, data, function(response) {
                //  alert(response);
                if(response == 'yes0') {
                   event.preventDefault();
                   addToCart(sess);
                }
                else {
                    jQuery(function($) {
                        $('#login-modal').modal({
                            show: 'true'
                        });
                    });
                }

            });
        });

        //open/close cart
        cartTrigger.on('click', function(event){
            event.preventDefault();
            toggleCart();
        });

        //close cart when clicking on the .cd-cart-container::before (bg layer)
        cartWrapper.on('click', function(event){
            if( $(event.target).is($(this)) ) toggleCart(true);
        });

        //delete an item from the cart
        cartList.on('click', '.delete-item', function(event){
            event.preventDefault();
            removeProduct($(event.target).parents('.product'));
        });

        //update item quantity
        cartList.on('change', 'select', function(event){
            quickUpdateCart();
        });

        //reinsert item deleted from the cart
        undo.on('click', 'a', function(event){
            clearInterval(undoTimeoutId);
            event.preventDefault();
            cartList.find('.deleted').addClass('undo-deleted').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
                $(this).off('webkitAnimationEnd oanimationend msAnimationEnd animationend').removeClass('deleted undo-deleted').removeAttr('style');
                quickUpdateCart();
            });
            undo.removeClass('visible');
        });
    }

    function toggleCart(bool) {
        var cartIsOpen = ( typeof bool === 'undefined' ) ? cartWrapper.hasClass('cart-open') : bool;

        if( cartIsOpen ) {
            cartWrapper.removeClass('cart-open');
            //reset undo
            clearInterval(undoTimeoutId);
            undo.removeClass('visible');
            cartList.find('.deleted').remove();

            setTimeout(function(){
                cartBody.scrollTop(0);
                //check if cart empty to hide it
                if( Number(cartCount.find('li').eq(0).text()) == 0) cartWrapper.addClass('empty');
            }, 500);
        } else {
            cartWrapper.addClass('cart-open');
        }
    }

    function addToCart(trigger) {
        var cartIsEmpty = cartWrapper.hasClass('empty');
        //update cart product list
        addProduct(trigger.data('proid'),trigger.data('property'),trigger.data('price'),trigger.data('name'));
        //update number of items
        updateCartCount(cartIsEmpty);
        //update total price
        updateCartTotal(trigger.data('price'), true);
        //show cart
        cartWrapper.removeClass('empty');
    }

    function addProduct(proid,property,price,name) {
        //Create Sessions
        var sessionData = {
            action: 'shopping_cart_sessions',
            propertyid:proid,
            property  : property,
            price      : price,
            plot:name
        };
        var urlsessions='http://localhost/property/wp-admin/admin-ajax.php'
        jQuery.post(urlsessions, sessionData, function(response) {
             //alert(response);
        });
      //  jQuery.post(urlsessions,sessionData);
        // You can add as many variables as you want (well, within reason)


        //this is just a product placeholder
        //you should insert an item with the selected product info
        //replace productId, productName, price and url with your real product info
        productId = productId + 1;
        var productAdded = $('<li class="product"><div class="product-details"><h3><a href="#0">'+property+'</a></h3><span class="plot">'+name+'</span> <span class="price">'+price+'</span><div class="actions"><a href="#0" class="delete-item"><i class="fa fa-trash" style="color:red"></i></a></div></div></li>');
        cartList.prepend(productAdded);
    }

    function removeProduct(product) {
        clearInterval(undoTimeoutId);
        cartList.find('.deleted').remove();

        var topPosition = product.offset().top - cartBody.children('ul').offset().top ,
            productQuantity = 1,
            productTotPrice = Number(product.find('.price').text().replace('Kshs ', '')) * productQuantity;

        product.css('top', topPosition+'px').addClass('deleted');

        //update items count + total price
        updateCartTotal(productTotPrice, false);
        updateCartCount(true, -productQuantity);
        undo.addClass('visible');

        //wait 3sec before completely remove the item
        undoTimeoutId = setTimeout(function(){
            undo.removeClass('visible');
            cartList.find('.deleted').remove();
        }, 3000);
    }

    function quickUpdateCart() {
        var quantity = 0;
        var price = 0;

        cartList.children('li:not(.deleted)').each(function(){
            var singleQuantity = Number($(this).find('select').val());
            quantity = quantity + singleQuantity;
            price = price + singleQuantity*Number($(this).find('.price').text().replace('Kshs', ''));
        });

        cartTotal.text(price.toFixed(2));
        cartCount.find('li').eq(0).text(quantity);
        cartCount.find('li').eq(1).text(quantity+1);
    }

    function updateCartCount(emptyCart, quantity) {
        if( typeof quantity === 'undefined' ) {
            var actual = Number(cartCount.find('li').eq(0).text()) + 1;
            var next = actual + 1;

            if( emptyCart ) {
                cartCount.find('li').eq(0).text(actual);
                cartCount.find('li').eq(1).text(next);
            } else {
                cartCount.addClass('update-count');

                setTimeout(function() {
                    cartCount.find('li').eq(0).text(actual);
                }, 150);

                setTimeout(function() {
                    cartCount.removeClass('update-count');
                }, 200);

                setTimeout(function() {
                    cartCount.find('li').eq(1).text(next);
                }, 230);
            }
        } else {
            var actual = Number(cartCount.find('li').eq(0).text()) + quantity;
            var next = actual + 1;

            cartCount.find('li').eq(0).text(actual);
            cartCount.find('li').eq(1).text(next);
        }
    }

    function updateCartTotal(price, bool) {
        bool ? cartTotal.text( (Number(cartTotal.text()) + Number(price)).toFixed(2) )  : cartTotal.text( (Number(cartTotal.text()) - Number(price)).toFixed(2) );
    }



    ///Property Repayment Calculator/Tool

});
jQuery(document).ready(function($){
    "use-strict";
function getInterest() {
    var property_price = cleanvalue($("#properyprice_"+1).val());
    var deposit = cleanvalue($("#deposit_"+1).val());
    var repayment = cleanvalue($("#repayment_"+1).val());

    if(repayment == "")
        return;

    if(deposit == "")
    {
        alert("Please fill in a Deposit amount.");
        return false;
    }

    if((deposit/property_price)<0.3)
    {
        alert("Deposit amount must be at least 30% of original/Property price.");
        return false;

    }
    var interest = null;
    var monthlyinstallments = null;
    var newprice = null;
    var interest_rate = 0.2;

    var total_interest = null;
    var total_new_price = null;
    var total_deposit = null;

    interest = Math.round(((interest_rate * (property_price - deposit))/12 ) * repayment);

    monthlyinstallments = Math.round((interest +  (property_price-deposit))/repayment);

    newprice = +property_price + +interest;

    $("#total_interest").html("KES: "+numberWithCommas(interest));


}

function cleanvalue(value)
{
    return value.trim().replace(",", "");

}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
});