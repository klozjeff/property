<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 8:20 AM
 */

/*
 * Index page ( Blog Page )
 */
global $page;
global $pageName; //$pageName from body.php
$pagename = get_query_var('pagename');
if($pagename=='blog'):
    get_header('blog');
    else:
        get_header();
        endif;


//get_template_part( 'partials/header/banner' );
?>
    <div class="page-head add-padding-top" style="background:url(<?php echo esc_url(get_template_directory_uri() . '/images/banner_about.jpg'); ?>) #494c53 no-repeat center top;background-size:cover">



    </div><div class="container-fluid">


    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="start-point"></div>

            <div class="container-fluid container-featured-item" style="background-color: #293f60;">
                <div class="container">
                    <article id="post-3070" class="card">
                        <div class="card__image col-xs-12 col-xsm-4 col-sm-4 col-sm-push-8" style="height: 285px;">
                            <a href="workable-sparkhire-integration/index.html">
                                <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/09/SparkHire.png')"></div>
                            </a>
                        </div>

                        <div class="card__text col-xs-12 col-xsm-8 col-sm-8 col-sm-pull-4">
                            <div class="card__text-content">
                                <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>                    <div class="entry-header">
                                    <h2><a href="workable-sparkhire-integration/index.html" rel="bookmark">Announcing our new integration with Spark&nbsp;Hire</a></h2>                    </div><!-- .entry-header -->

                                <div class="entry-content">
                                    One of the recurring themes at Workable this year has been the steady expansion of our partner services. We're able to offer our customers access to a suite of specialist...                  </div><!-- .entry-content -->
                            </div><!-- .card__text-content -->

                            <div class="card__footer">
                                <div class="card__footer-info col-xs-12 col-sm-9">
                                    <img src="wp-content/authors/heath-51.jpg" class="avatar photo" alt="Heath Allen" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/heath/index.html">Heath Allen</a></span></span>                        |
                                    <span class="posted-on"><time class="entry-date published" datetime="2017-09-12T15:51:14+00:00">September 12, 2017</time><time class="updated" datetime="2017-09-13T09:57:52+00:00">September 13, 2017</time></span>                      </div>
                                <div class="card__footer-more col-sm-3"><a href="workable-sparkhire-integration/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                            </div><!-- .card__footer -->
                        </div>
                    </article>
                </div>
            </div>

            <div class="container">
                <article id="post-3060" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="height: 261px;">
                        <a href="workable-bob-integration/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/08/workable_bob_integration.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="workable-bob-integration/index.html" rel="bookmark">Workable integrates with HR and benefits platform,&nbsp;bob</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                As summer winds down and vacation season comes to a close for many of us, the fall hiring season is about to kick into full gear. In preparation, we’ve been...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/heath-51.jpg" class="avatar photo" alt="Heath Allen" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/heath/index.html">Heath Allen</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published updated" datetime="2017-09-07T14:51:27+00:00">September 7, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="workable-bob-integration/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-841" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(188, 207, 74); height: 254px;">
                        <a href="spreadsheets-and-emails-are-not-hiring-tools/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2013/11/email_preview.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="spreadsheets-and-emails-are-not-hiring-tools/index.html" rel="bookmark">Why are you still using hiring&nbsp;spreadsheets?</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                Hiring great people is the key to building a successful business. Given this, you’d think most companies would use specialist hiring tools to manage such an important task. They bring...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/fiona-61.png" class="avatar photo" alt="Fiona McSweeney" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/fiona/index.html">Fiona McSweeney</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-07-25T10:02:15+00:00">July 25, 2017</time><time class="updated" datetime="2017-08-04T13:55:58+00:00">August 4, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="spreadsheets-and-emails-are-not-hiring-tools/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-3011" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(66, 133, 244); height: 285px;">
                        <a href="workable-google-for-jobs/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/07/google-workable.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="workable-google-for-jobs/index.html" rel="bookmark">Workable makes it easy to get indexed in Google for&nbsp;Jobs</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                At Workable we’re constantly striving to expand our customers’ ability to fill their open jobs quickly and with the right candidates. For many companies, that process starts with getting your...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/heath-51.jpg" class="avatar photo" alt="Heath Allen" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/heath/index.html">Heath Allen</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-07-21T16:37:00+00:00">July 21, 2017</time><time class="updated" datetime="2017-08-04T13:36:38+00:00">August 4, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="workable-google-for-jobs/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-3021" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(87, 177, 173); height: 230px;">
                        <a href="workable-world-tour/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/07/wwt-thumb2.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-in-action/index.html" rel="category tag">Workable in action</a></div>        <div class="entry-header">
                                <h2><a href="workable-world-tour/index.html" rel="bookmark">Workable World Tour: our first event&nbsp;series</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                In my first week at Workable I sat down with Nikos, our CEO, and he told me we needed to “show our customers some love”. He was talking about making...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/whitney-65.png" class="avatar photo" alt="Whitney Young" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/whitney/index.html">Whitney Young</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-07-21T08:55:21+00:00">July 21, 2017</time><time class="updated" datetime="2017-07-27T13:18:19+00:00">July 27, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="workable-world-tour/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-2997" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(97, 196, 206); height: 285px;">
                        <a href="workable-sapling-integration/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/07/sapling_preview.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="workable-sapling-integration/index.html" rel="bookmark">From new hire to engaged employee: our integration with Sapling is&nbsp;live</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                Since the launch of our updated APIs&nbsp;and&nbsp;Developer Partner Program&nbsp;in March, we've been working with other HR technology vendors to integrate more products and services into the Workable ecosystem. We’ve added...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/heath-51.jpg" class="avatar photo" alt="Heath Allen" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/heath/index.html">Heath Allen</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-07-17T10:21:45+00:00">July 17, 2017</time><time class="updated" datetime="2017-07-19T17:05:38+00:00">July 19, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="workable-sapling-integration/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-2968" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(16, 44, 83); height: 254px;">
                        <a href="workable-codility-integration/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/06/codility-integration-featured.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="workable-codility-integration/index.html" rel="bookmark">Announcing our new integration with&nbsp;Codility</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                At Workable, one of the most exciting initiatives this year so far has been the launch of our Developer Partner Program and API. It was our goal to expand the...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/heath-51.jpg" class="avatar photo" alt="Heath Allen" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/heath/index.html">Heath Allen</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-06-13T13:36:26+00:00">June 13, 2017</time><time class="updated" datetime="2017-07-27T13:12:22+00:00">July 27, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="workable-codility-integration/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-2952" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(51, 204, 148); height: 285px;">
                        <a href="boost-job-advertisement-posting/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/06/boost-job-advertisement-featured.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="boost-job-advertisement-posting/index.html" rel="bookmark">Boost your job advertisement exposure through&nbsp;Workable</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                We've made posting job advertisements&nbsp;even easier and more effective using Workable. Find out what we've done and why... Stats over speculation 2017 began with some serious data crunching here at...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/fiona-61.png" class="avatar photo" alt="Fiona McSweeney" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/fiona/index.html">Fiona McSweeney</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-06-07T13:31:55+00:00">June 7, 2017</time><time class="updated" datetime="2017-06-08T07:46:26+00:00">June 8, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="boost-job-advertisement-posting/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-1199" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(89, 193, 193); height: 261px;">
                        <a href="custom-job-application-form/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2014/05/askus.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="custom-job-application-form/index.html" rel="bookmark">How to create a custom employment application to screen job&nbsp;applicants</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                One of the benefits of using recruiting software for job advertising is the huge increase in reach, without a huge increase in effort. In just a few clicks you can...      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/spyros-57.png" class="avatar photo" alt="Spyros Magiatis" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/spyros/index.html">Spyros Magiatis</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-05-31T08:30:59+00:00">May 31, 2017</time><time class="updated" datetime="2017-06-23T20:44:10+00:00">June 23, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="custom-job-application-form/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="container">
                <article id="post-2936" class="card">

                    <div class="card__image col-xs-12 col-xsm-4 col-sm-4" style="background-color: rgb(39, 122, 179); height: 285px;">
                        <a href="workable-criteriacorp-integration/index.html">
                            <div class="card__image-preview" style="background-image: url(' wp-content/uploads/2017/05/criteria_thumbnail.png')"></div>
                        </a>
                    </div>

                    <div class="card__text col-xs-12 col-xsm-8 col-sm-8">
                        <div class="card__text-content">
                            <div class="post-category"><a href="category/workable-features/index.html" rel="category tag">Workable features</a></div>        <div class="entry-header">
                                <h2><a href="workable-criteriacorp-integration/index.html" rel="bookmark">Workable integrates with HireSelect® Pro from&nbsp;CriteriaCorp</a></h2>        </div><!-- .entry-header -->

                            <div class="entry-content">
                                In our last post we announced the launch of our Developer Partner Program and API. This is exciting for us, as we can expand Workable’s functionality with specialist service providers....      </div><!-- .entry-content -->
                        </div><!-- .article-holderright-up -->

                        <div class="card__footer">
                            <div class="card__footer-info col-xs-12 col-sm-9">
                                <img src="wp-content/authors/heath-51.jpg" class="avatar photo" alt="Heath Allen" height="96" width="96"><span class="byline"><span class="author vcard"><a href="author/heath/index.html">Heath Allen</a></span></span>          |
                                <span class="posted-on"><time class="entry-date published" datetime="2017-05-26T13:47:00+00:00">May 26, 2017</time><time class="updated" datetime="2017-05-26T15:27:40+00:00">May 26, 2017</time></span>        </div>
                            <div class="card__footer-more col-sm-3"><a href="workable-criteriacorp-integration/index.html" rel="bookmark" class="small-uppercase--bold">read more<span class="arrow-gray"></span></a></div>
                        </div><!-- .card__footer -->

                    </div>

                </article><!-- #post-## -->
            </div><div class="posts-pagination"><div class="container"><span class="page-numbers current">1</span>
                    <a class="page-numbers" href="page/2/index.html">2</a>
                    <a class="page-numbers" href="page/3/index.html">3</a>
                    <span class="page-numbers dots">…</span>
                    <a class="page-numbers" href="page/7/index.html">7</a>
                    <a class="next page-numbers" href="page/2/index.html">Next »</a></div></div>
            <div class="container">
                <section class="workable-newsletter workable-newsletter--blue-dark aligncenter visible-sm-block">
                    <h4>Subscribe to the Newsletter</h4>
                    <p class="md-text">
                        Hiring, talent, culture, tech and trends in a 5-minute read delivered
                        <br class="visible-md-block visible-lg-block">
                        to your inbox on Thursdays.
                    </p>
                    <div id="mc_embed_signup">
                        <form action="http://workable.us10.list-manage.com/subscribe/post?u=9decee1daa694313bd8ce347c&amp;id=9a06b68196" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate="">
                            <div id="mc_embed_signup_scroll" class="form-group col-xs-12">
                                <div class="mc-field-group col-xs-12 col-sm-9">
                                    <input value="" name="EMAIL" class="required email form-control" placeholder="Type your email address" id="mce-EMAIL" type="email">
                                </div>
                                <input value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn--primary col-xs-12 col-sm-3" type="submit">
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div style="position: absolute; left: -5000px;"><input name="b_9decee1daa694313bd8ce347c_9a06b68196" tabindex="-1" value="" type="text"></div>
                            </div>
                        </form>
                    </div>
                </section> <!-- .workable-newsletter -->
            </div>


        </main><!-- #main -->
    </div><!-- #primary -->

</div>
<?php
/*
 * Footer
 */
get_footer();