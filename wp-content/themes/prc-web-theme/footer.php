<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 8:48 AM
 */
?>
<footer class="site-footer site-footer-one">
    <div class="container">
        <div class="row">
          <!--  <div class="col-lg-3 footer-logo">
                <a href="#">
                    <img class="img-responsive" src="<?=get_template_directory_uri();?>/assets/images/prclogo.png" alt="Footer Logo"/>
                </a>
                <p class="copyright-text">
                    © Copyright 2015 All rights reserved by <a href="#">Property Reality Company</a>
                </p>
            </div>-->
            <div class="col-lg-12 footer-widget-area">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <section class="widget clearfix widget_lc_taxonomy">
                            <h3 class="widget-title">Related Links</h3>
                            <ul>
                                <li>
                                    <a href="#">Land</a>

                                </li>
                                <li>
                                    <a href="#">Housing</a>

                                </li>
                                <li>
                                    <a href="#">M-Ploti</a>

                                </li>
                                <li>
                                    <a href="#">Kilimo</a>

                                </li>
                            </ul>
                        </section>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <section class="widget clearfix widget_recent_entries">
                            <h3 class="widget-title">Quick Links</h3>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Gallery</a></li>
                                <li><a href="#">Blog</a></li>

                            </ul>
                        </section>
                    </div>
                    <div class="clearfix visible-sm"></div>

                    <div class="col-sm-6 col-md-4">
                        <section id="inspiry_social_media_icons-1" class="widget clearfix widget_inspiry_social_media_icons">

                            <?php if ( function_exists("sfp_page_plugin") ) {
                                $args = array(
                                    'url'           => 'http://www.facebook.com/prclimited/',
                                    'width'     => '400',
                                    'height'     => '300',
                                    'show_facepile'=>  true,
                                    'small_header' => true,
                                    'hide_cover'=> false,
                                    'locale'        => 'en_US'
                                );
                                sfp_page_plugin( $args );
                            } ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>


       <!-- <div class="row">
            <div class="col-lg-12 footer-widget-area">

                    <div class="col-sm-12 col-md-12">
                        <section class="widget clearfix widget_lc_taxonomy">
                            <h3 class="widget-title">JOIN INVESTORS LIST</h3>
                            <p style="text-align: center"> You will get updates about PRC products on your Email</p>
                            <form method="post" action="#news-success">
                             <div class="col-md-3" ><input name="newsletter_name" id="name" placeholder="Name" class="form-control fields" type="text"></div>
                                <div class="col-md-3" >  <input name="newsletter_phone" id="phone" placeholder="Phone" class="form-control fields" type="text"></div>
                                <div class="col-md-3" > <input name="newsletter_email" id="email" placeholder="Enter your email" class="form-control fields" type="email"></div>
                                <input name="newsletter_submit" class="col-md-1 btn btn-primary fields" value="Submit" type="submit">
                            </form>
                        </section>
                    </div>



            </div>
        </div>-->

    </div>
    <div class="row copyright">
        <div class="container">
            <div class="col-lg-8"> <p class="copyright-text">
                    Copyright &copy; <?= date('Y');?> PRC&trade; All rights reserved
                </p></div>
            <div class="col-lg-4">
                <div class="social-networks pull-right">
                    <a class="twitter" href="#twitter" target="_blank"><i class="fa fa-twitter fa-lg"></i></a>
                    <a class="facebook" href="#facebook" target="_blank"><i class="fa fa-facebook fa-lg"></i></a>
                    <a class="instagram" href="#instagram" target="_blank"><i class="fa fa-instagram fa-lg"></i></a>
                    <a class="youtube-square" href="#youtube" target="_blank"><i class="fa fa-youtube-square fa-lg"></i></a>

                </div>
            </div>

        </div>
    </div>
</footer>


<?php wp_footer();?>

</body>
</html>