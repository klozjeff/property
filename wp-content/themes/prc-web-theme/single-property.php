<?php
/*
 * Single property page
 */
get_header();

//get_template_part( 'partials/header/banner' );

global $prcweb_options;

// Flag for gallery images in case of 3rd variation
global $gallery_images_exists;
$gallery_images_exists = false;

get_template_part('partials/property/single/slider');
get_template_part('partials/property/single/site-visit');
?>
<section class="single-property">
<div class="container">
    <?php if ( have_posts() ) :
    while ( have_posts() ) :
    the_post();

    global $prcweb_single_property;
        $prcweb_single_property = new Inspiry_Property( get_the_ID() );
     //$prcweb_single_property->get_address();
       $propertyType=$prcweb_single_property->get_taxonomy_first_term('property-type', 'all')->name;
       if($propertyType=='Land'):
    ?>

    <div class="col-md-8">
        <div class="large-9 columns">

  <!-- Title and details start -->
           <?php get_template_part( 'partials/property/single/title' );?>
                    <!-- Title and details end -->

            <?php get_template_part( 'partials/property/single/content' ); ?>



                 </div>



    </div>
    <div class="col-md-4">
        <div id="" class="large-3 columns">
           <?php /*
            * Map
            */
            if ( $prcweb_options[ 'prcweb_property_map' ] ) {
            $address = $prcweb_single_property->get_address();
            $location = $prcweb_single_property->get_location();
            if ( !empty( $address ) && !empty( $location ) ) {
            get_template_part( 'partials/property/single/map' );
            }
            }
            ?>

    <?php get_template_part('partials/property/single/contact-form');?>

</div>
    </div>


    <?php
    elseif($propertyType=='Housing'):
    ?>
        <?php get_template_part( 'partials/property/single/content-housing' ); ?>
    <?php
    endif;

    endwhile;
    endif;
    ?>
</div>
</section>
<?php
get_footer();
?>
<script type="text/javascript">
    jQuery(function($) {
    $(document).ready(function(){

        $('ul.nav li').click(function(){
            var tab_id = $(this).attr('data-tab');


            $('ul.nav li').removeClass('active');
            $('.tab-content').removeClass('active');

            $(this).addClass('active');
            $("#"+tab_id).addClass('active');
        })

    });
        });
</script>
<script>

    function showUser() {
        // Retrieve values from the selects
        var u = document.getElementById('orig').value;
        var g = document.getElementById('deposit').value;
        var h = document.getElementById('rate').value;

        if (u=="" || g == "" || h=="" )
        {
            document.getElementById("result").innerHTML="";
            return;
        }

        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
        } else {// code for IE6, IE5
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                document.getElementById("result").innerHTML=xmlhttp.responseText;
            }
        }

        xmlhttp.open("GET","getDay.php?q="+u+"&w="+g+"&c="+h,true);
        xmlhttp.send();
    }
</script>
<style>
    .tab-content{
        display: none;
        background: none;
        padding: 15px;
    }
    .tab-content.active{
        display: inherit;
    }
</style>
