<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/18/2017
 * Time: 8:08 AM
 */
define('PRCWEB_THEME_VERSION','1.0.0');

if(!function_exists('prcweb_setup')):
    function prcweb_setup(){
        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support( 'post-thumbnails' );

        /*
         * Used on:
         * Single post page and Post archive pages
         * Single property page
         * Gallery pages
         */
        set_post_thumbnail_size( 850, 570, true );

        /*
         * Used on:
         * Home page ( Properties and Featured Properties )
         * Properties list pages ( both list and grid layout )
         */
        add_image_size( 'inspiry-grid-thumbnail', 660,600, true);

        /*
         * Used on:
         * Agent single page
         * Property single page
         */
        add_image_size( 'inspiry-agent-thumbnail', 220, 220, true);

        /*
         * Theme theme uses wp_nav_menu in one location.
         */
        register_nav_menus( array(
            'primary' => __( 'Primary Menu', 'inspiry' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        )   );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
            'image', 'video', 'gallery'
        ) );
    }
    add_action('after_setup_theme','prcweb_setup');

    endif;


/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since 1.0
 */
if ( ! isset ( $content_width) )
    $content_width = 1440;


/**
 * Set google reCAPTCHA display to false by default
 */
if ( ! isset( $google_reCAPTCHA_counter ) ) {
    $google_reCAPTCHA_counter = 1;
}



// load the theme's options
if ( !isset( $redux_owd ) && file_exists( dirname(__FILE__) . '/inc/ReduxFramework/prcweb-redux.php' ) ) {
    require_once( dirname(__FILE__) . '/inc/ReduxFramework/prcweb-redux.php' );
}

/*
 * Meta boxes configuration
 */
require_once ( get_template_directory() . '/inc/meta-boxes/config.php' );


/*
 * TGM plugin activation
 */
require_once ( get_template_directory() . '/inc/tgm/prcweb-required-plugins.php' );
/*
 * Utility functions
 */
require_once ( get_template_directory() . '/inc/util/basic-functions.php' );
require_once ( get_template_directory() . '/inc/util/custom-functions.php' );
require_once ( get_template_directory() . '/inc/util/header-functions.php' );
require_once ( get_template_directory() . '/inc/util/footer-functions.php' );
require_once ( get_template_directory() . '/inc/util/profile-functions.php' );
require_once ( get_template_directory() . '/inc/util/member-functions.php' );
require_once ( get_template_directory() . '/inc/util/facebookoauth.php');
require_once ( get_template_directory() . '/inc/util/sessions-functions.php');
require_once ( get_template_directory() . '/inc/util/pesapalOauth.php');
require_once ( get_template_directory() . '/inc/util/query_functions.php');
require_once ( get_template_directory() . '/inc/util/property-functions.php');
/*
 * Widgets
 */
//include_once ( get_template_directory() . '/inc/widgets/advance-search-widget.php' );
//include_once ( get_template_directory() . '/inc/widgets/featured-properties-widget.php' );
//include_once ( get_template_directory() . '/inc/widgets/facebook-login-widget.php' );

if( !function_exists( 'prcweb_google_fonts' ) ) :
    /**
     * Google fonts enqueue url
     */
    function prcweb_google_fonts() {

        $fonts_url = '';

        /*
         * Translators: If there are characters in your language that are not
         * supported by Varela Round, translate this to 'off'. Do not translate
         * into your own language.
         */

        $varela_round = _x( 'on', 'Varela Round font: on or off', 'inspiry' );

        if ( 'off' !== $varela_round ) {
            $font_families = array();

            if ( 'off' !== $varela_round ) {
                $font_families[] = 'Varela Round';
            }

            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );

            $fonts_url = add_query_arg( $query_args,  '//fonts.googleapis.com/css' );
        }

        return esc_url_raw( $fonts_url );

    }
endif;


if ( ! function_exists( 'prcweb_enqueue_styles' ) ) :
    /**
     * Enqueue required styles for front end
     * @since   1.0.0
     * @return  void
     */
    function prcweb_enqueue_styles() {

        if ( ! is_admin() ) :

            $prceweb_template_directory_uri = get_template_directory_uri();

            // Google Font
            wp_enqueue_style (
                'google-varela-round',
                prcweb_google_fonts(),
                array(),
                PRCWEB_THEME_VERSION
            );


            // flexslider
            wp_enqueue_style(
                'flexslider',
                $prceweb_template_directory_uri . '/js/flexslider/flexslider.css',
                array(),
                '2.4.0'
            );

            // lightslider
            wp_enqueue_style(
                'lightslider',
                $prceweb_template_directory_uri . '/js/lightslider/css/lightslider.min.css',
                array(),
                '1.1.2'
            );

            // owl carousel
            wp_enqueue_style(
                'owl-carousel',
                $prceweb_template_directory_uri . '/js/owl.carousel/owl.carousel.css',
                array(),
                PRCWEB_THEME_VERSION
            );

            // swipebox
            wp_enqueue_style(
                'swipebox',
                $prceweb_template_directory_uri . '/js/swipebox/css/swipebox.min.css',
                array(),
                '1.3.0'
            );

            // select2
            wp_enqueue_style(
                'select2',
                $prceweb_template_directory_uri . '/js/select2/select2.css',
                array(),
                '4.0.0'
            );

            // font awesome
            wp_enqueue_style(
                'font-awesome',
                $prceweb_template_directory_uri . '/css/font-awesome.min.css',
                array(),
                '4.3.0'
            );

            // animate css
            wp_enqueue_style(
                'animate',
                $prceweb_template_directory_uri . '/css/animate.css',
                array(),
                PRCWEB_THEME_VERSION
            );

            // magnific popup css
            wp_enqueue_style(
                'magnific-popup',
                $prceweb_template_directory_uri . '/js/magnific-popup/magnific-popup.css',
                array(),
                '1.0.0'
            );

            // main styles
            wp_enqueue_style(
                'prcweb-main',
                $prceweb_template_directory_uri . '/css/main.css',
                array( 'font-awesome', 'animate' ),
                PRCWEB_THEME_VERSION
            );

            // theme styles
            wp_enqueue_style(
                'prcweb-theme',
                $prceweb_template_directory_uri . '/css/theme.css',
                array( 'prcweb-main' ),
                PRCWEB_THEME_VERSION
            );



            // parent theme style.css
            wp_enqueue_style(
                'prcweb-parent-default',
                get_stylesheet_uri(),
                array ( 'prcweb-main' ),
                PRCWEB_THEME_VERSION
            );

            /* if rtl is enabled
            if ( is_rtl() ) {

                wp_enqueue_style(
                    'prcweb-main-rtl',
                    $prceweb_template_directory_uri . '/css/main-rtl.css',
                    array( 'prcweb-main' ),
                    PRCWEB_THEME_VERSION
                );

                wp_enqueue_style(
                    'prcweb-theme-rtl',
                    $prceweb_template_directory_uri . '/css/theme-rtl.css',
                    array( 'prcweb-main', 'prcweb-theme', 'prcweb-main-rtl' ),
                    PRCWEB_THEME_VERSION
                );
            }*/

            // parent theme css/custom.css
            wp_enqueue_style(
                'prcweb-parent-custom',
                $prceweb_template_directory_uri . '/css/custom.css',
              array( 'font-awesome', 'animate' ),
                PRCWEB_THEME_VERSION
            );

        endif;

    }

endif; // prcweb_enqueue_styles

add_action ( 'wp_enqueue_scripts', 'prcweb_enqueue_styles' );


if ( ! function_exists( 'prcweb_enqueue_scripts' ) ) :
    /**
     * Enqueue required java scripts for front end
     * @since   1.0.0
     * @return  void
     */
    function prcweb_enqueue_scripts() {

        if (!is_admin()) :

            $prcweb_template_directory_uri = get_template_directory_uri();
            /* flex slider
            wp_enqueue_script(
                'boot',
                $prcweb_template_directory_uri . '/css/bootstrap/js/bootstrap.min.js',
                array( 'jquery' ),
                '2.4.0',
                true
            );*/

            // flex slider
            wp_enqueue_script(
                'flexslider',
                $prcweb_template_directory_uri . '/js/flexslider/jquery.flexslider-min.js',
                array( 'jquery' ),
                '2.4.0',
                true
            );

            if ( is_singular( 'property' ) ) {
                // light slider
                wp_enqueue_script(
                    'lightslider',
                    $prcweb_template_directory_uri . '/js/lightslider/js/lightslider.min.js',
                    array( 'jquery' ),
                    '1.1.2',
                    true
                );
            }

            if ( is_singular( 'property' ) || is_page_template( 'page-templates/home.php' ) ) {
                // owl carousel
                wp_enqueue_script(
                    'owl-carousel',
                    $prcweb_template_directory_uri . '/js/owl.carousel/owl.carousel.min.js',
                    array( 'jquery' ),
                    INSPIRY_THEME_VERSION,
                    true
                );
            }

            // swipebox
            wp_enqueue_script(
                'swipebox',
                $prcweb_template_directory_uri . '/js/swipebox/js/jquery.swipebox.min.js',
                array( 'jquery' ),
                '1.3.0',
                true
            );

            // select2
            wp_enqueue_script(
                'select2',
                $prcweb_template_directory_uri . '/js/select2/select2.min.js',
                array( 'jquery' ),
                '4.0.0',
                true
            );

            // hoverIntent
            wp_enqueue_script(
                'hoverIntent',
                $prcweb_template_directory_uri . '/js/jquery.hoverIntent.js',
                array( 'jquery' ),
                '1.8.1',
                true
            );

            // validate
            wp_enqueue_script(
                'validate',
                $prcweb_template_directory_uri . '/js/jquery.validate.min.js',
                array( 'jquery' ),
                '1.13.1',
                true
            );

            // form
            wp_enqueue_script(
                'form',
                $prcweb_template_directory_uri . '/js/jquery.form.js',
                array( 'jquery' ),
                '3.51.0',
                true
            );

            // transition
            wp_enqueue_script(
                'transition',
                $prcweb_template_directory_uri . '/js/transition.js',
                array( 'jquery' ),
                '3.3.1',
                true
            );

            // appear
            wp_enqueue_script(
                'appear',
                $prcweb_template_directory_uri . '/js/jquery.appear.js',
                array( 'jquery' ),
                '0.3.4',
                true
            );

            // modal
            wp_enqueue_script(
                'modal',
                $prcweb_template_directory_uri . '/js/modal.js',
                array( 'jquery' ),
                '3.3.4',
                true
            );

            // mean menu
            wp_enqueue_script(
                'meanmenu',
                $prcweb_template_directory_uri . '/js/meanmenu/jquery.meanmenu.min.js',
                array( 'jquery' ),
                '2.0.8',
                true
            );

            // Placeholder
            wp_enqueue_script(
                'placeholder',
                $prcweb_template_directory_uri . '/js/jquery.placeholder.min.js',
                array( 'jquery' ),
                '2.1.2',
                true
            );

            if ( is_page_template( 'page-templates/2-columns-gallery.php' )
                || is_page_template( 'page-templates/3-columns-gallery.php' )
                || is_page_template( 'page-templates/4-columns-gallery.php' ) ) {

                // isotope
                wp_enqueue_script(
                    'isotope',
                    $prcweb_template_directory_uri . '/js/isotope.pkgd.min.js',
                    array( 'jquery' ),
                    '2.2.0',
                    true
                );
            }

            // jQuery UI
            wp_enqueue_script( 'jquery-ui-core' );
            wp_enqueue_script( 'jquery-ui-autocomplete' );
            wp_enqueue_script( 'jquery-ui-sortable' );

            if( is_singular( 'property' )
                || is_page_template( 'page-templates/contact.php' )
                || is_page_template( 'page-templates/properties-search.php' )
                || is_page_template( 'page-templates/home.php' )
                || is_page_template( 'page-templates/properties-list.php' )
                || is_page_template( 'page-templates/properties-list-with-sidebar.php' )
                || is_page_template( 'page-templates/properties-grid.php' )
                || is_page_template( 'page-templates/properties-grid-with-sidebar.php' )
                || is_page_template( 'page-templates/submit-property.php' )
                || is_tax( 'property-city' )
                || is_tax( 'property-status' )
                || is_tax( 'property-type' )
                || is_tax( 'property-feature' )
                || is_post_type_archive( 'property' ) ) {

                $google_map_arguments = array();

                global $prcweb_options;

                // Get Google Map API Key if available
                if ( isset( $prcweb_options[ 'prcweb_google_map_api_key' ] ) && !empty( $prcweb_options[ 'prcweb_google_map_api_key' ] ) ) {
                    $google_map_arguments[ 'key' ] = urlencode( $prcweb_options[ 'prcweb_google_map_api_key' ] );
                }
                else{
                    $google_map_arguments[ 'key' ]  ='AIzaSyBB0rMrNAevAIRLE1fJqDzw2Ay_dQYkItE';
                }

                // Change the map based on language if enabled from theme options
                if ( $prcweb_options[ 'prcweb_google_map_auto_lang' ] ) {
                    if ( function_exists( 'wpml_object_id_filter' ) ) {
                        $google_map_arguments[ 'language' ] = urlencode( ICL_LANGUAGE_CODE );
                    } else {
                        $google_map_arguments[ 'language' ] = urlencode( get_locale() );
                    }
                }

                $google_map_api_uri = add_query_arg( apply_filters( 'prcweb_google_map_arguments', $google_map_arguments ) ,  '//maps.google.com/maps/api/js' );

                wp_enqueue_script(
                    'google-map-api',
                    esc_url_raw( $google_map_api_uri ),
                    array(),
                    '3.21',
                    false
                );
            }

            if( is_page_template( 'page-templates/properties-search.php' )
                || is_page_template( 'page-templates/home.php' )
                || is_page_template( 'page-templates/properties-list.php' )
                || is_page_template( 'page-templates/properties-list-with-sidebar.php' )
                || is_page_template( 'page-templates/properties-grid.php' )
                || is_page_template( 'page-templates/properties-grid-with-sidebar.php' )
                || is_tax( 'property-city' )
                || is_tax( 'property-status' )
                || is_tax( 'property-type' )
                || is_tax( 'property-feature' )
                || is_post_type_archive( 'property' )
                || is_single( 'property' )) {

                wp_enqueue_script(
                    'google-map-info-box',
                    $prcweb_template_directory_uri . '/js/infobox.js',
                    array( 'google-map-api' ),
                    '1.1.9',
                    false
                );


            }

            // Comment reply script
            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }

            // Property search form script
            wp_enqueue_script(
                'prcweb-search-form',
                $prcweb_template_directory_uri . '/js/inspiry-search-form.js',
                array( 'jquery' ),
                PRCWEB_THEME_VERSION,
                true
            );


            // favorites
            if ( is_singular( 'property' ) || is_page_template( 'page-templates/favorites.php' ) ) {

                wp_enqueue_script(
                    'inspiry-favorites',
                    $prcweb_template_directory_uri . '/js/inspiry-favorites.js',
                    array( 'jquery' ),
                   PRCWEB_THEME_VERSION,
                    true
                );

            }


            // edit profile
            if ( is_page_template( 'page-templates/edit-profile.php' ) ) {

                wp_enqueue_script( 'plupload' );

                wp_enqueue_script(
                    'inspiry-edit-profile',
                    $prcweb_template_directory_uri . '/js/inspiry-edit-profile.js',
                    array( 'jquery', 'plupload' ),
                    PRCWEB_THEME_VERSION,
                    true
                );

                $edit_profile_data = array(
                    'ajaxURL' => admin_url( 'admin-ajax.php' ),
                    'uploadNonce' => wp_create_nonce ( 'prcweb_allow_upload' ),
                    'fileTypeTitle' => __( 'Valid file formats', 'inspiry' ),
                );

                wp_localize_script( 'prcweb-edit-profile', 'editProfile', $edit_profile_data );

            }

            //checkout
            if(is_page_template('page-templates/my-information.php') || is_page_template('page-templates/user-dashboard.php') )
            {
                wp_enqueue_script( 'plupload' );

                wp_enqueue_script(
                    'prcweb-edit-profile',
                    $prcweb_template_directory_uri.'/js/checkout.js',
                    array('jquery','plupload'),
                    PRCWEB_THEME_VERSION,
                    true
                );

                $edit_profile_data = array(
                    'ajaxURL' => admin_url( 'admin-ajax.php' ),
                    'uploadNonce' => wp_create_nonce ( 'prcweb_allow_upload' ),
                    'fileTypeTitle' => __( 'Valid file formats', 'prcweb' ),
                );

                wp_localize_script( 'prcweb-edit-profile', 'editProfile', $edit_profile_data );
            }

            if(is_page_template('page-templates/checkout.php'))
            {
                wp_enqueue_script(
                    'prcweb-checkout',
                    $prcweb_template_directory_uri.'/js/plot-operations.js',
                    true
                );
            }

            // Property submit
            if ( is_page_template( 'page-templates/submit-property.php' ) ) {

                wp_enqueue_script( 'plupload' );
                wp_enqueue_script( 'jquery-ui-sortable' );

                wp_enqueue_script(
                    'prcweb-property-submit',
                    $prcweb_template_directory_uri . '/js/prcweb-property-submit.js',
                    array( 'jquery', 'plupload', 'jquery-ui-sortable' ),
                    PRCWEB_THEME_VERSION,
                    true
                );

                $property_submit_data = array(
                    'ajaxURL' => admin_url( 'admin-ajax.php' ),
                    'uploadNonce' => wp_create_nonce ( 'prcweb_allow_upload' ),
                    'fileTypeTitle' => __( 'Valid file formats', 'inspiry' ),
                );
                wp_localize_script( 'prcweb-property-submit', 'propertySubmit', $property_submit_data );

            }

            // Property Single
            if ( is_singular( 'property' ) ) {

                wp_enqueue_script(
                    'magnific-popup',
                    $prcweb_template_directory_uri . '/js/magnific-popup/jquery.magnific-popup.min.js',
                    array( 'jquery' ),
                    '1.0.0',
                    true
                );

                wp_enqueue_script(
                    'share-js',
                    $prcweb_template_directory_uri . '/js/share.min.js',
                    array( 'jquery' ),
                    PRCWEB_THEME_VERSION,
                    true
                );

                wp_enqueue_script(
                    'property-share',
                    $prcweb_template_directory_uri . '/js/property-share.js',
                    array( 'jquery' ),
                    PRCWEB_THEME_VERSION,
                    true
                );

                wp_enqueue_script(
                    'sly-js',
                    $prcweb_template_directory_uri . '/js/sly.min.js',
                    array( 'jquery' ),
                    PRCWEB_THEME_VERSION,
                    true
                );

                wp_enqueue_script(
                    'property-horizontal-scrolling',
                    $prcweb_template_directory_uri . '/js/property-horizontal-scrolling.js',
                    array( 'jquery' ),
                    PRCWEB_THEME_VERSION,
                    true
                );


                wp_enqueue_script(
                    'google-map-marker-clusterer',
                    $prcweb_template_directory_uri . '/js/markerclusterer.js',

                    true
                );

                wp_enqueue_script(
                    'google-map-geoxml3',
                    $prcweb_template_directory_uri . '/js/geoxml3.js',

                    true
                );

               wp_enqueue_script(
                    'prcweb-cart',
                    $prcweb_template_directory_uri . '/js/cart.js',
                    array( 'jquery' ),
                    PRCWEB_THEME_VERSION,
                    true
                );

            }



            /* Google reCAPTCHA
            if ( prcweb_is_reCAPTCHA_configured() ) {
                global $prcweb_options;
                $recaptcha_src = esc_url_raw( add_query_arg( array( 'render' => 'explicit', 'onload' => 'loadPrcWebReCAPTCHA' ) , '//www.google.com/recaptcha/api.js' ) );
                if ( isset( $prcweb_options['prcweb_reCAPTCHA_language'] ) && !empty( $prcweb_options['prcweb_reCAPTCHA_language'] ) ) {
                    $recaptcha_src =    esc_url_raw( add_query_arg( array( 'hl' => urlencode( $prcweb_options['prcweb_reCAPTCHA_language'] ) ), $recaptcha_src ) );
                }

                // enqueue google reCAPTCHA API
                wp_enqueue_script(
                    'google-recaptcha',
                    $recaptcha_src,
                    array(),
                    PRCWEB_THEME_VERSION,
                    true
                );
            }*/

            // Main js
            wp_enqueue_script(
                'custom',
                $prcweb_template_directory_uri . '/js/custom.js',
                array( 'jquery' ),
                PRCWEB_THEME_VERSION,
                true
            );

        endif;

    }

endif; // prcweb_enqueue_scripts

add_action ( 'wp_enqueue_scripts', 'prcweb_enqueue_scripts' );



if( !function_exists( 'prcweb_theme_sidebars' ) ) :
    /**
     * Register theme sidebars
     *
     * @since 1.0.0
     */
    function prcweb_theme_sidebars( ) {

        // Location: Default Sidebar
        register_sidebar(array(
            'name'=>__('Default Sidebar','prcweb'),
            'id' => 'default-sidebar',
            'description' => __('Widget area for default sidebar on news and post pages.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Location: Sidebar Pages
        register_sidebar(array(
            'name'=>__('Pages Sidebar','prcweb'),
            'id' => 'default-page-sidebar',
            'description' => __('Widget area for default page template sidebar.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Location: Sidebar Properties List
        register_sidebar(array(
            'name'=>__('Properties List Sidebar','prcweb'),
            'id' => 'properties-list',
            'description' => __('Widget area for properties list template with sidebar support.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Location: Sidebar Properties Grid
        register_sidebar(array(
            'name'=>__('Properties Grid Sidebar','prcweb'),
            'id' => 'properties-grid',
            'description' => __( 'Widget area for properties grid template with sidebar support.', 'prcweb' ),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Location: Sidebar Property
        register_sidebar(array(
            'name'=>__('Property Sidebar','prcweb'),
            'id' => 'property-sidebar',
            'description' => __('Widget area for property detail page sidebar.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // *** FOR FUTURE ***
        // Location: Property Search Template
        /*register_sidebar( array(
            'name' => __('Property Search Sidebar','inspiry'),
            'id' => 'property-search-sidebar',
            'description' => __('Widget area for property search template with sidebar.','inspiry'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));
        */

        // Location: Sidebar dsIDX
        register_sidebar(array(
            'name'=>__('dsIDXpress Sidebar','prcweb'),
            'id' => 'dsidx-sidebar',
            'description' => __('Widget area for dsIDX related pages.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Location: Footer First Column
        register_sidebar(array(
            'name'=>__('Footer First Column','prcweb'),
            'id' => 'footer-first-column',
            'description' => __('Widget area for first column in footer.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ));

        // Location: Footer Second Column
        register_sidebar(array(
            'name'=>__('Footer Second Column','prcweb'),
            'id' => 'footer-second-column',
            'description' => __('Widget area for second column in footer.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Footer Third Column
        register_sidebar(array(
            'name'=>__('Footer Third Column','prcweb'),
            'id' => 'footer-third-column',
            'description' => __('Widget area for third column in footer.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // Location: Footer Fourth Column
        register_sidebar(array(
            'name'=>__('Footer Fourth Column','prcweb'),
            'id' => 'footer-fourth-column',
            'description' => __('Widget area for fourth column in footer.','prcweb'),
            'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>'
        ));

        // *** FOR FUTURE ***
        // Location: Home Search Area
        /*
        register_sidebar(array(
            'name'=>__('Home Search Area','inspiry'),
            'id'=> 'home-search-area',
            'description'=>__('Widget area for only IDX Search Widget. Using this area means you want to display IDX search form instead of default search form.','inspiry'),
            'before_widget' => '<section id="home-idx-search" class="clearfix %2$s">',
            'after_widget' => '</section>',
            'before_title' => '<h3 class="home-widget-label">',
            'after_title' => '</h3>'
        ));
        */

    }

    add_action( 'widgets_init', 'prcweb_theme_sidebars' );

endif;
if(!function_exists('ajax_check_user_logged_in')):
function ajax_check_user_logged_in() {
    echo is_user_logged_in()?'yes':'no';
}
endif;
add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');
/**
 * Custom walker class.
 */
class PRC_Walker_Nav_Menu extends Walker_Nav_Menu {

    /**
     * Starts the list before the elements are added.
     *
     * Adds classes to the unordered list sub-menus.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // Depth-dependent classes.
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );

        // Build HTML for output.
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }

    /**
     * Start the element output.
     *
     * Adds main/sub-classes to the list items and links.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // Depth-dependent classes.
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        // Build HTML.
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // Link attributes.
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        // Build HTML output and pass through the proper filter.
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

