<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/12/2017
 * Time: 12:59 PM
 */

//var_dump($all_properties);

?>
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/prc-web-manager/lib/assets/css/jquery-ui.css" rel="stylesheet" />
<link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/prc-web-manager/lib/assets/bower_components/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<script src="<?php echo WP_PLUGIN_URL; ?>/prc-web-manager/lib/assets/bower_components/bootstrap/js/bootstrap.min.js"></script>
<div class="content-w">
        <div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <div class="element-header">Properties
            <a class="pull-right" href="<?php echo admin_url('admin.php?page=prc_properties_create'); ?>"><button  class="sub-btn btn btn-success btn-sm pull-right">
               <i class="os-icon os-icon-pencil-1"></i> Add New         </button></a>
            </div>

                <div class=" table-responsive">
                    <table id="data-table" width="100%" class="table table-bordered table-striped table-lightfont">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>Land Reference</th>
                            <th>Size</th>
                            <th>Plot No</th>
                            <th>Price</th>
                            <th>Published</th>
                            <th>Latitude</th>
                            <th>Category</th>
                            <th>Location</th>
                            <th>Country</th>
                            <th>Allow Installments</th>
                            <th>Facilities</th>
                            <th>Actions</th>

                        </tr>
                        </thead>

                        <tbody>
                        <?php
if(!empty($all_properties)):
    foreach ($all_properties as $property):

                        echo '<tr>
  <td><input type="checkbox" name="property[]" value="'.$property->id.'"> </td>
                            <td>'.$property->name.'</td>
                            <td>'.$property->land_reference.'</td>
                            <td>'.$property->size.'</td>
                            <td>'.$property->plot_no.'</td>
                            <td>'.$property->price.'</td>
                            <td>'.$property->published.'</td>
                            <td>'.$property->latitude.'</td>
                            <td>'.$property->category.'</td>
                            <td>'.$property->location.'</td>
                            <td>'.$property->country.'</td>
                             <td>'.$property->allow_installments.'</td>
                              <td></td>
                            <td class="row-actions">
                            <a href="'.admin_url('admin.php?page=prc_properties_view&id='.$property->id).'"><i class="os-icon os-icon-pencil-2" title="Edit"></i></a>
                            <a href="#"><i class="os-icon os-icon-link-3" title="Visit Page"></i></a>
                            <a class="danger" href="#" ><i class="os-icon os-icon-database-remove" title="Archive" ></i></a></td>
                        </tr>';
endforeach;
endif;
    ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>  </div>

<script src="<?php echo WP_PLUGIN_URL; ?>/prc-web-manager/lib/assets/bower_components/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo WP_PLUGIN_URL; ?>/prc-web-manager/lib/assets/bower_components/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#data-table').DataTable();

    });
</script>