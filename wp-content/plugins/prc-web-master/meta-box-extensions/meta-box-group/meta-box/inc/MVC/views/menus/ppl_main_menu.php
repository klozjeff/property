<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 4/26/2017
 * Time: 2:36 PM
 */
function prc_properties_menu()
{
    add_menu_page( 'PRC Properties', 'PRC Properties', 'manage_options', 'properties-dashboard', array(
        __CLASS__,
        'prc_properties_view_path'
    ), plugins_url('lib/assets/images/menu-icons/analytics-logo.png', __FILE__),'2.2.9');

    add_submenu_page( 'properties-dashboard', 'PRC Properties' . ' Dashboard', ' Dashboard', 'manage_options', 'properties-dashboard', array(
        __CLASS__,
        'prc_properties_view_path'
    ));

    add_submenu_page( 'properties-dashboard', 'PRC Properties' . ' Settings', '<b style="color:#f9845b">Settings</b>', 'manage_options', 'properties-settings', array(
        __CLASS__,
        'prc_properties_view_path'
    ));

}









