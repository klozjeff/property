<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/15/2017
 * Time: 2:39 PM
 */
define('ROOTDIR', plugin_dir_path(__FILE__));
class LoadView
{

    function __construct()
    {
    }
    public function view ($file_name,$domain=null, $data = null)
    {

        if (is_array($data))
        {
            extract($data);

        }

        include ROOTDIR.'lib/includes/views/properties/'.$file_name.'.php';

    }

}