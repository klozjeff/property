<?php

/**
 * Fired during plugin activation
 *
 * @link       http://themeforest.net/user/InspiryThemes
 * @since      1.0.0
 *
 * @package    Inspiry_Real_Estate
 * @subpackage Inspiry_Real_Estate/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Inspiry_Real_Estate
 * @subpackage Inspiry_Real_Estate/includes
 * @author     M Saqib Sarwar <saqib@inspirythemes.com>
 */
class Inspiry_Real_Estate_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
 global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $table = array(
            'properties' => $wpdb->prefix.'properties',
            'property_items' => $wpdb->prefix.'property_items'
        );
        $sql="CREATE TABLE IF NOT EXISTS".$table['properties']."(
		`id` int(11) UNSIGNED NOT NULL auto_increment,
  asset_id int(10) UNSIGNED NOT NULL DEFAULT 0,
  name varchar(255) NOT NULL,
  reference text NOT NULL,
  status int(1) NOT NULL DEFAULT 0,
  highlight text NOT NULL,
  description text NOT NULL,
  land_reference varchar(255) NOT NULL,
  size varchar(255) NOT NULL,
  plot_no varchar(255) NOT NULL,
  price double NOT NULL,
  ordering int(11) NOT NULL,
  published tinyint(1) NOT NULL,
  created_by int(11) NOT NULL,
  latitude varchar(255) NOT NULL,
  longitude varchar(255) NOT NULL,
  kml text NOT NULL,
  video_url varchar(150) NOT NULL,
  category varchar(255) NOT NULL,
  location varchar(255) NOT NULL,
  country varchar(255) NOT NULL,
  allow_installments varchar(255) NOT NULL,
  installments text NULL,
  amenities text NOT NULL,
  location_overview text NOT NULL,
  forecast text NOT NULL,
  main_image varchar(255) NOT NULL,
  is_multiple tinyint(2) NOT NULL DEFAULT 0,
  featured tinyint(2) NOT NULL DEFAULT 0,
  special_offer tinyint(2) NOT NULL DEFAULT 0,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
   PRIMARY KEY  (id))";
        dbDelta($sql);
        $sql="CREATE TABLE IF NOT EXISTS".$table['property_items']."( 
		id int(255) NOT NULL auto_increment,
  project_id int(255) NOT NULL,
  name varchar(10) NOT NULL,
  coordinates varchar(500) NOT NULL,
  description varchar(20) NOT NULL,
  styleUrl varchar(20) NOT NULL,
  price double NOT NULL,
  size varchar(10) NOT NULL,
  status int(1) NOT NULL,
  published tinyint(2) NOT NULL DEFAULT 1,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  booked_at datetime NOT NULL,
  bought_at datetime NOT NULL,
   PRIMARY KEY  (id))";
        dbDelta($sql);
	}

}
