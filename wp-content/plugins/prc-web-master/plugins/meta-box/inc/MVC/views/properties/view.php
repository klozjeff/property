<?php
/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/15/2017
 * Time: 4:51 PM
 */
print_r($single_property);
?>
<form id="formValidate" method="POST" action="" enctype="multipart/form-data">
    <div class="breadcrumb subhead" xmlns="http://www.w3.org/1999/html">
        <a class="pull-right" href="<?php echo admin_url('admin.php?page=prc_properties_create'); ?>"><button  class="sub-btn btn btn-primary btn-sm pull-right">
                Save    </button></a>
        <a class="pull-right" href="<?php echo admin_url('admin.php?page=prc_properties_create'); ?>"><button  class="sub-btn btn btn-success btn-sm pull-right">
                Save & Close </button></a>
        <a class="pull-right" href="<?php echo admin_url('admin.php?page=prc_properties_list'); ?>"><button  class="sub-btn btn btn-danger btn-sm pull-right">
                Close </button></a>
    </div>
    <div class="content-w">

        <div class="content-i">
            <div class="content-box">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="element-wrapper">
                            <div class="element-box">
                                <h5 class="form-header">Add New Property</h5>
                                <?php if (isset($message)): ?><div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>
                                <div class="form-desc"></div>
                                <div class="form-group"><label for=""> Property Title</label><input
                                        class="form-control" data-error="Property Title can't be empty"
                                        placeholder="Enter Property Title" name="property_land_name"  id="property_land_name" required="required" type="text">
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                                <div class="wp-core-ui wp-editor-wrap tmce-active has-dfw form-group"><label>Description</label>
                                    <!--   <textarea class="form-control" rows="3" name="property_land_description" id="property_land_description">--->
                                    <?php wp_editor( $property_land_description, 'property_land_description',array('textarea_name' => 'property_land_description' ) );?>
                                    </textarea></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group"><label for=""> Land Reference</label><input class="form-control" type="text" name="property_land_reference" value="" />
                                            <div class="help-block form-text text-muted form-control-feedback">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label for="">Size in Acres</label><input class="form-control" type="text"  name="property_land_size" value="" />
                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group"><label for=""> Price</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">Kshs</div>
                                                <input class="form-control" name="property_land_price" placeholder=""
                                                       type="text"></div></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group"><label for=""> Allow Installments</label><select
                                                name="property_allow_install"  class="form-control">
                                                <option value="Yes">Yes</option>
                                                <option value="No">No</option>

                                            </select></div>
                                    </div>
                                </div>
                                <div class="form-group"><label>Installments</label>
                                    <!-- <textarea class="form-control" name="property_install_desc" rows="3"></textarea>-->
                                    <?php wp_editor( $property_install_desc, 'property_install_desc',array('textarea_name' => 'property_install_desc' ) );?>

                                </div>
                                <div class="form-group"><label for=""> Plot No</label><input class="form-control" type="text" size="50" name="property_land_plot_no" value="" />
                                    <div class="help-block form-text with-errors form-control-feedback"></div>
                                </div>
                                <fieldset class="form-group">
                                    <legend><span>More Details</span></legend>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input class="form-control" value="1" name="isPublished" type="checkbox">  <label for=""> Published</label>
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group"><input class="form-control" name="isFeatured" value="1"  type="checkbox"> <label for="">Featured</label>
                                                <div class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for=""> Latitude</label><input class="form-control" placeholder="Enter the middle position of the plots" type="text" name="property_land_lat" id="property_land_lat">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for="">Longitude</label>

                                                <input value="" class="form-control" name="property_land_long" id="property_land_long" value="" type="text"></div>

                                        </div>
                                    </div>
                                    <div class="form-group"><label for="">KLM File</label><input
                                            class="form-control" type="file" name="property_klm_file" id="property_klm_file"></div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for=""> Category</label>
                                                <input class="form-control" value="" name="property_land_category" id="property_land_category" value="" type="text"></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for=""> Location</label>  <input value="" class="form-control" name="property_land_location" id="property_land_location" value="" type="text"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for=""> Country</label>
                                                <select name="property_land_country" id="property_land_country" class="form-control">
                                                    <option value="Kenya">Kenya</option>
                                                    <option value="Uganda">Uganda</option>
                                                    <option value="Tanzania">Tanzania</option>
                                                </select>	</div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for="">Status</label>
                                                <select  name="property_land_status" id="property_land_status" class="form-control">
                                                    <option value="current">Available</option>
                                                    <option value="sold">Sold Out</option>
                                                    <option value="upcoming">Upcoming</option>
                                                </select>	</div>
                                        </div>
                                    </div>


                                    <div class="form-group"><label> Highlight</label>
                                        <?php wp_editor( $property_land_highlights, 'property_land_highlights',array('textarea_name' => 'property_land_highlights' ) );?>

                                    </div>

                                    <div class="form-group"><label> Amenities</label>
                                        <?php wp_editor( $property_land_amenities, 'property_land_amenities',array('textarea_name' => 'property_land_amenities' ) );?>

                                    </div>

                                    <div class="form-group"><label> Location Overview</label>
                                        <?php wp_editor( $property_land_overview, 'property_land_overview',array('textarea_name' => 'property_land_overview' ) );?>

                                    </div>

                                    <div class="form-group"><label> Property Forecast</label>
                                        <?php wp_editor( $property_land_forecast, 'property_land_forecast',array('textarea_name' => 'property_land_forecast' ) );?>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group"><label for=""> Video URL</label>
                                                <input class="form-control" type="text" name="property_video_url"  id="property_video_url" value=""  /></div>
                                        </div>

                                    </div>
                                </fieldset>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-panel">

                <div id="postimagediv" class="element-wrapper postbox ">
                    <h6 class="element-header">Featured Image</h6>
                    <div class="inside">
                        <div class="">
                            <img id="book_image" class="book_image"  style="max-width:100%;" />
                            <input id="image-url" type="hidden" name="imageurl" />
                            <p>
                                <a title="<?php esc_attr_e( 'Set Main Property image' ) ?>" href="#" id="set-property-image" class="inputfile"><?php _e( 'Set Main Property image' ) ?></a>
                            </p>
                            <script type="text/javascript">
                                jQuery(document).ready(function($){
                                    var mediaUploader;
                                    $('#set-property-image').click(function(e) {
                                        e.preventDefault();
                                        // If the uploader object has already been created, reopen the dialog
                                        if (mediaUploader) {
                                            mediaUploader.open();
                                            return;
                                        }
                                        // Extend the wp.media object
                                        mediaUploader = wp.media.frames.file_frame = wp.media({
                                            title: 'Choose Image',
                                            button: {
                                                text: 'Choose Image'
                                            }, multiple: false });
                                        // When a file is selected, grab the URL and set it as the text field's value
                                        mediaUploader.on('select', function() {
                                            var attachment = mediaUploader.state().get('selection').first().toJSON();
                                            $('#image-url').val(attachment.url);
                                            $('img.book_image').prop('src', attachment.url);
                                        });
                                        // Open the uploader dialog
                                        mediaUploader.open();
                                    });
                                });
                            </script>



                        </div>   </div>

                </div>
                <div class="form-buttons-w">
                    <button class="btn btn-primary" name="insert" type="submit"> Submit</button>
                </div>


            </div>

        </div>
</form>