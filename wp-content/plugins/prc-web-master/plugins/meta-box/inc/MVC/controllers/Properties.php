<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/15/2017
 * Time: 11:43 AM
 */
define('ROOTDIR', plugin_dir_path(__FILE__));

class Properties
{
     protected  $properties_model;
     protected $load;
    function __construct()
    {

        require_once(ROOTDIR . 'lib/includes/models/properties_model.php');
        require_once(ROOTDIR . 'lib/includes/core/LoadView.php');
        $this->properties_model = new properties_model();
        $this->load=new LoadView();
    }


    public function prc_properties_create()
    {
        //insert
        if (isset($_POST['insert'])) {
            $video_url = $_POST['property_video_file'];
            if (!empty($_FILES['property_klm_file']['name'])):
                $this->properties_model->uploadKMLFile($_FILES['property_klm_file']);
            endif;
            $ppData=array(
                'asset_id'=>'001',
                'name' => $_POST["property_land_name"],
                'description' => $_POST["property_land_description"],
                'land_reference'=>$_POST["property_land_reference"],
                'status'=> $_POST['property_land_status'],
                'size'=>$_POST["property_land_size"],
                'plot_no'=>$_POST["property_land_plot_no"],
                'price'=>$_POST["property_land_price"],
                'published'=>$_POST['isPublished'],
                'featured'=>$_POST['isFeatured'],
                'category'=>$_POST["property_land_category"],
                'location'=>$_POST["property_land_location"],
                'country'=>$_POST['property_land_country'],
                'allow_installments'=>$_POST["property_allow_install"],
                'installments'=>$_POST["property_install_desc"],
                'latitude'=>$_POST['property_land_lat'],
                'longitude'=>$_POST['property_land_long'],
                'highlight'=>$_POST['property_land_highlights'],
                'amenities'=>$_POST['property_land_amenities'],
                'location_overview'=>$_POST['property_land_overview'],
                'forecast'=> $_POST['property_land_forecast'],
                'kml'=>$_POST['property_klm_file'],
                'main_image'=>$_POST['imageurl'],
                'created_by'=>1);
            $saveProperty=$this->properties_model->saveData('properties',$ppData);
            if($saveProperty):
            $message=$_FILES['property_klm_file']['name'];
            else:
                $message="Error Saving Data";
            endif;
        }
         $data['control']='properties';
         $this->load->view('create',$data);
    }
    public function prc_properties_list()
    {
        $properties=$this->properties_model->getData('properties');
        $data['all_properties']=$properties;
        $this->load->view('index','',$data);
    }
    public function prc_properties_view()
    {

        $property=$this->properties_model->getSingleData('properties',$_GET['id']);
        $data['single_property']=$property;
        $this->load->view('view',$data);
    }









}
new Properties();