<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/15/2017
 * Time: 11:38 AM
 */
define('ROOTDIR', plugin_dir_path(__FILE__));
define('WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/uploads');
class properties_model
{
    protected $table_name;
    protected $db;

    function __construct()
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . "properties";
    }

    function uploadImage()
    {


    }

    function uploadKMLFile($file)
    {
        $supported_types = array('application/kml');
        $arr_file_type = wp_check_filetype(basename($file['name']));
        $uploaded_type = $arr_file_type['type'];
        if (in_array($uploaded_type, $supported_types)):
            $upload = wp_upload_bits($file['name'], null, file_get_contents($file['tmp_name']));
            if (isset($upload['error']) && $upload['error'] != 0) {
                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);

            } else {
                $this->readKMLFile($file);
                return true;

            }

        endif;
    }

   function readKMLFile($file){
        $kml=$file['name'];
        $updir='';
        $completeurl = WP_CONTENT_DIR."/uploads/2017/05/".$kml;
        $xml = simplexml_load_file($completeurl);
        $placemarks = $xml->Document->Folder;
        foreach ($placemarks->Placemark as $placemark){
            //allow values starting with A to be imported
            $placemark->numeric = str_replace('A', '', $placemark->name);
            if(is_numeric((string)$placemark->numeric)){
                $plot_data=array(
                    'name'=>$placemark->name,
                    'price'=>'299000',
                    'size'=>'1/8Acre',
                    'coordinates'=>$placemark->Point->coordinates,
                    'description'=>$placemark->description,
                    'styleUrl'=>$placemark->styleUrl,
                    'project_id'=>1,
                    'status'=>1,
                    'published'=>1,
                    'created_at'=> date('Y-m-d'));
                    $this->SaveData('property_items',$plot_data);
            }
        }

    }

   /* function savePlot($data)
    {
        $date = JFactory::getDate();
        //$date=date('Y-m-d H:i:s', $date);
        $db = JFactory::getDBO();
        $query="INSERT INTO #__property_item (id,name,price,size,coordinates,description,project_id,created_at,status,published) VALUES( '','".$data['name']."','".$data['price']."','".$data['size']."','".$data['coordinates']."','".$data['description']."','".$data['project_id']."','".$date."','1','1')";
        $db->setQuery($query);
        $db->query();
        $id = $db->insertid();
        if($id)$msg="Plot ".$data['name']."  Saved."; else $msg="Error Saving Plot";
        return $msg;

    }*/


    function saveData($table, $data)
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . $table;
        $count = array_values(array_fill(0, sizeof($data), '%s'));
        $save = $wpdb->insert($this->table_name, $data, $count);
        if ($save):
            $lastID = $wpdb->insert_id;
            return true;

        else:
            return false;
        endif;

    }

    function getData($table)
    {
        global $wpdb;
        $this->table_name = $wpdb->prefix . $table;
        $details=$wpdb->get_results("SELECT * from $this->table_name");
        return $details;

    }

    function getSingleData($table,$RowID)

    {
        if($RowID):
            global $wpdb;
            $this->table_name = $wpdb->prefix . $table;
            $property=$wpdb->get_row($wpdb->prepare("SELECT * from $this->table_name where id=%s", $RowID));
            return $property;
        endif;
    }

}
new properties_model();