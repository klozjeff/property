<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 8/25/2017
 * Time: 9:14 AM
 */
class PrcWeb_Press_Post_Type
{
  public function register_press_post_type()
  {
      $labels=array(
          'name'                => _x( 'Press', 'Post Type General Name', 'prcweb-real-estate' ),
          'singular_name'       => _x( 'Press', 'Post Type Singular Name', 'prcweb-real-estate' ),
          'menu_name'           => __( 'Press Release', 'prcweb-real-estate' ),
          'name_admin_bar'      => __( 'Press', 'prcweb-real-estate' ),
          'parent_item_colon'   => __( 'Parent Press:', 'prcweb-real-estate'),
          'all_items'           => __( 'All Press', 'prcweb-real-estate' ),
          'add_new_item'        => __( 'Add New Press', 'prcweb-real-estate' ),
          'add_new'             => __( 'Add New', 'prcweb-real-estate' ),
          'new_item'            => __( 'New Press', 'prcweb-real-estate' ),
          'edit_item'           => __( 'Edit Press', 'prcweb-real-estate' ),
          'update_item'         => __( 'Update Press', 'prcweb-real-estate' ),
          'view_item'           => __( 'View Press', 'prcweb-real-estate' ),
          'search_items'        => __( 'Search Press', 'prcweb-real-estate' ),
          'not_found'           => __( 'Not found', 'prcweb-real-estate' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'prcweb-real-estate' ),
      );

      $args = array(
          'label'               => __( 'Press', 'prcweb-real-estate' ),
          'description'         => __( 'Press', 'prcweb-real-estate' ),
          'labels'              => $labels,
          'supports'            => array( 'title','editor'),
          'hierarchical'        => false,
          'public'              => false,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'menu_position'       => 6,
          'menu_icon'           => 'dashicons-megaphone',
          'show_in_admin_bar'   => false,
          'can_export'          => true,
          'has_archive'         => false,
          'rewrite'            => array( 'slug' => 'press','hierarchical' => TRUE, 'with_front' => FALSE),
          'capability_type'     => 'post',
      );

      register_post_type( 'Press', $args );
  }

  public function register_release_category_taxonomy(){

      $labels = array(
          'name'                       => _x( 'Release Categories', 'Taxonomy General Name', 'prcweb-real-estate' ),
          'singular_name'              => _x( 'Release Category', 'Taxonomy Singular Name', 'prcweb-real-estate' ),
          'menu_name'                  => __( 'Release Categories', 'prcweb-real-estate' ),
          'all_items'                  => __( 'All Release Categories', 'prcweb-real-estate' ),
          'parent_item'                => __( 'Parent Release Category', 'prcweb-real-estate' ),
          'parent_item_colon'          => __( 'Parent Release Category:', 'prcweb-real-estate' ),
          'new_item_name'              => __( 'New Release Category Name', 'prcweb-real-estate' ),
          'add_new_item'               => __( 'Add New Release Category', 'prcweb-real-estate' ),
          'edit_item'                  => __( 'Edit Release Category', 'prcweb-real-estate' ),
          'update_item'                => __( 'Update Release Category', 'prcweb-real-estate' ),
          'view_item'                  => __( 'View Release Category', 'prcweb-real-estate' ),
          'separate_items_with_commas' => __( 'Separate Release Categories with commas', 'prcweb-real-estate' ),
          'add_or_remove_items'        => __( 'Add or remove Release Categories', 'prcweb-real-estate' ),
          'choose_from_most_used'      => __( 'Choose from the most used', 'prcweb-real-estate' ),
          'popular_items'              => __( 'Popular Release Categories', 'prcweb-real-estate' ),
          'search_items'               => __( 'Search Release Categories', 'prcweb-real-estate' ),
          'not_found'                  => __( 'Not Found', 'prcweb-real-estate' ),
      );

      $rewrite = array(
          'slug'                       => apply_filters( 'prcweb_press_category_slug', __( 'press-category', 'prcweb-real-estate' ) ),
          'with_front'                 => true,
          'hierarchical'               => true,
      );

      $args = array(
          'labels'                     => $labels,
          'hierarchical'               => true,
          'public'                     => true,
          'show_ui'                    => true,
          'show_admin_column'          => true,
          'show_in_nav_menus'          => true,
          'show_tagcloud'              => true,
          'rewrite'                    => $rewrite,
      );

      register_taxonomy( 'press-category', array( 'press' ), $args );

  }

    public function register_media_type_taxonomy(){

        $labels = array(
            'name'                       => _x( 'Media Types', 'Taxonomy General Name', 'prcweb-real-estate' ),
            'singular_name'              => _x( 'Media Type', 'Taxonomy Singular Name', 'prcweb-real-estate' ),
            'menu_name'                  => __( 'Media Types', 'prcweb-real-estate' ),
            'all_items'                  => __( 'All Media Types', 'prcweb-real-estate' ),
            'parent_item'                => __( 'Parent Media Type', 'prcweb-real-estate' ),
            'parent_item_colon'          => __( 'Parent Media Type:', 'prcweb-real-estate' ),
            'new_item_name'              => __( 'New Media Type Name', 'prcweb-real-estate' ),
            'add_new_item'               => __( 'Add New Media Type', 'prcweb-real-estate' ),
            'edit_item'                  => __( 'Edit Media Type', 'prcweb-real-estate' ),
            'update_item'                => __( 'Update Media Type', 'prcweb-real-estate' ),
            'view_item'                  => __( 'View Media Type', 'prcweb-real-estate' ),
            'separate_items_with_commas' => __( 'Separate Media Types with commas', 'prcweb-real-estate' ),
            'add_or_remove_items'        => __( 'Add or remove Media Types', 'prcweb-real-estate' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'prcweb-real-estate' ),
            'popular_items'              => __( 'Popular Media Types', 'prcweb-real-estate' ),
            'search_items'               => __( 'Search Media Types', 'prcweb-real-estate' ),
            'not_found'                  => __( 'Not Found', 'prcweb-real-estate' ),
        );

        $rewrite = array(
            'slug'                       => apply_filters( 'prcweb_media_type_slug', __( 'media-type', 'prcweb-real-estate' ) ),
            'with_front'                 => true,
            'hierarchical'               => true,
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );

        register_taxonomy( 'media-type', array( 'press' ), $args );

    }

    public function register_meta_boxes ( $meta_boxes ){

        $prefix = 'PRC_WEB_';

        // Partners Meta Box
        $meta_boxes[] = array(
            'id' => 'press-meta-box',
            'title' => __('Press Details', 'prcweb-real-estate'),
            'pages' => array( 'press' ),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('Press Release Sub-headline', 'prcweb-real-estate'),
                    'id' => "{$prefix}sub_headline",
                    'desc' => __('Sub-headline for this press release ', 'prcweb-real-estate'),
                    'type' => 'text',
                ),
                array(
                    'name' => __('Link', 'prcweb-real-estate'),
                    'id' => "{$prefix}source_link",
                    'desc' => __('Ex:Link for applicants to submit their applications.Brightermonday,Fuzu or any other platform', 'prcweb-real-estate'),
                    'type' => 'text',
                ),
                array(
                    'name' => __('Source', 'prcweb-real-estate'),
                    'id' => "{$prefix}press_source",
                    'desc' => __('Name of the press release/News source eg Daily Nation,The Star', 'prcweb-real-estate'),
                    'type' => 'text',
                ),

            )
        );

        // apply a filter before returning meta boxes
        $meta_boxes = apply_filters( 'job_meta_boxes', $meta_boxes );

        return $meta_boxes;
    }
}