<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 8/25/2017
 * Time: 9:14 AM
 */
class PrcWeb_Team_Post_Type
{
  public function register_team_post_type()
  {
      $labels=array(
          'name'                => _x( 'Team', 'Post Type General Name', 'prcweb-real-estate' ),
          'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'prcweb-real-estate' ),
          'menu_name'           => __( 'Team/Governance', 'prcweb-real-estate' ),
          'name_admin_bar'      => __( 'Team', 'prcweb-real-estate' ),
          'parent_item_colon'   => __( 'Parent Job:', 'prcweb-real-estate' ),
          'all_items'           => __( 'All Team', 'prcweb-real-estate' ),
          'add_new_item'        => __( 'Add New Team', 'prcweb-real-estate' ),
          'add_new'             => __( 'Add New', 'prcweb-real-estate' ),
          'new_item'            => __( 'New Team', 'prcweb-real-estate' ),
          'edit_item'           => __( 'Edit Team', 'prcweb-real-estate' ),
          'update_item'         => __( 'Update Team', 'prcweb-real-estate' ),
          'view_item'           => __( 'View Team', 'prcweb-real-estate' ),
          'search_items'        => __( 'Search Team', 'prcweb-real-estate' ),
          'not_found'           => __( 'Not found', 'prcweb-real-estate' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'prcweb-real-estate' ),
      );

      $args = array(
          'label'               => __( 'Team', 'prcweb-real-estate' ),
          'description'         => __( 'Team', 'prcweb-real-estate' ),
          'labels'              => $labels,
          'supports'            => array( 'title','editor','thumbnail'),
          'hierarchical'        => false,
          'public'              => false,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'menu_position'       => 6,
          'menu_icon'           => 'dashicons-businessman',
          'show_in_admin_bar'   => false,
          'can_export'          => true,
          'has_archive'         => false,
          'rewrite'            => array( 'slug' => 'Team','hierarchical' => TRUE, 'with_front' => FALSE),
          'capability_type'     => 'post',
      );

      register_post_type( 'Team', $args );
  }


    public function register_meta_boxes ( $meta_boxes ){

        $prefix = 'PRC_WEB_';

        // Partners Meta Box
        $meta_boxes[] = array(
            'id' => 'team-meta-box',
            'title' => __('Team/Governance Member Details', 'prcweb-real-estate'),
            'pages' => array( 'team' ),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('Title', 'prcweb-real-estate'),
                    'id' => "{$prefix}team_title",
                    'desc' => __('Ex:Director HR ', 'prcweb-real-estate'),
                    'type' => 'text',
                ),


            )
        );

        // apply a filter before returning meta boxes
        $meta_boxes = apply_filters( 'team_meta_boxes', $meta_boxes );

        return $meta_boxes;
    }
}