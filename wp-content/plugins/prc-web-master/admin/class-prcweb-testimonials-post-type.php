<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/24/2017
 * Time: 10:00 AM
 */
class Prcweb_Testimonials_Post_Type
{
    public function register_testimonial_post_type()
    {

        $labels = array(
            'name'                => _x( 'Testimonials', 'Post Type General Name', 'prcweb-real-estate' ),
            'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'prcweb-real-estate' ),
            'menu_name'           => __( 'Testimonials', 'prcweb-real-estate' ),
            'name_admin_bar'      => __( 'Testimonial', 'prcweb-real-estate' ),
            'parent_item_colon'   => __( 'Parent Testimonial:', 'prcweb-real-estate' ),
            'all_items'           => __( 'All Testimonials', 'prcweb-real-estate' ),
            'add_new_item'        => __( 'Add New Testimonial', 'prcweb-real-estate' ),
            'add_new'             => __( 'Add New', 'prcweb-real-estate' ),
            'new_item'            => __( 'New Testimonial', 'prcweb-real-estate' ),
            'edit_item'           => __( 'Edit Testimonial', 'prcweb-real-estate' ),
            'update_item'         => __( 'Update Testimonial', 'prcweb-real-estate' ),
            'view_item'           => __( 'View Testimonial', 'prcweb-real-estate' ),
            'search_items'        => __( 'Search Testimonial', 'prcweb-real-estate' ),
            'not_found'           => __( 'Not found', 'prcweb-real-estate' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'prcweb-real-estate' ),
        );

        $rewrite = array(
            'slug'                => apply_filters( 'prcweb_testimonial_slug', __( 'Testimonial', 'prcweb-real-estate' ) ),
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => false,
        );

        $args = array(
            'label'               => __( 'Testimonial', 'prcweb-real-estate' ),
            'description'         => __( 'Real Estate Testimonial', 'prcweb-real-estate' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'thumbnail',),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'menu_position'       => 2,
            'menu_icon'           => 'dashicons-testimonial',
            'show_in_admin_bar'   => true,
            'show_in_nav_menus'   => true,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );

        register_post_type( 'Testimonial', $args );
    }
    /**
     * Register custom columns
     *
     * @param   array   $defaults
     * @since   1.0.0
     * @return  array   $defaults
     */
    public function register_custom_column_titles ( $defaults ) {

        $new_columns = array(
            "thumb"     => __( 'Photo', 'prcweb-real-estate' ),
            "email"     => __( 'Email', 'prcweb-real-estate' ),
            "mobile"    => __( 'Mobile', 'prcweb-real-estate'),
        );

        $last_columns = array();

        if ( count( $defaults ) > 2 ) {
            $last_columns = array_splice( $defaults, 2, 1 );
        }

        $defaults = array_merge( $defaults, $new_columns );
        $defaults = array_merge( $defaults, $last_columns );

        return $defaults;
    }

    /**
     * Display custom column for Testimonials
     *
     * @access  public
     * @param   string $column_name
     * @since   1.0.0
     * @return  void
     */
    public function display_custom_column ( $column_name ) {
        global $post;

        switch ( $column_name ) {

            case 'thumb':
                if ( has_post_thumbnail ( $post->ID ) ) {
                    ?>
                    <a href="<?php the_permalink(); ?>" target="_blank">
                        <?php the_post_thumbnail( array( 130, 130 ) );?>
                    </a>
                    <?php
                } else {
                    _e ( 'No Image', 'prcweb-real-estate' );
                }
                break;

            case 'email':
                $Testimonial_email = is_email( get_post_meta ( $post->ID, 'PRC_WEB_testimony_email', true ) );
                if ( $Testimonial_email ) {
                    echo $Testimonial_email;
                } else {
                    _e ( 'NA', 'prcweb-real-estate' );
                }
                break;

            case 'mobile':
                $mobile_number = get_post_meta ( $post->ID, 'PRC_WEB_mobile_number', true );
                if ( !empty( $mobile_number ) ) {
                    echo $mobile_number;
                } else {
                    _e ( 'NA', 'prcweb-real-estate' );
                }
                break;

            default:
                break;
        }
    }
    /**
     * Register meta boxes related to Testimony post type
     *
     * @param   array   $meta_boxes
     * @since   1.0.0
     * @return  array   $meta_boxes
     */
    public function register_meta_boxes ( $meta_boxes ){

        $prefix = 'PRC_WEB_';

        // Testimonial Meta Box
        $meta_boxes[] = array(
            'id'        => 'Testimonial-meta-box',
            'title'     => __('Testimonial Details', 'prcweb-real-estate'),
            'pages'     => array( 'testimonial' ),
            'context'   => 'normal',
            'priority'  => 'high',

            'fields'    => array(
                array(
                    'id' => "{$prefix}client_name",
                    'name' => __('Name', 'prcweb-real-estate'),
                    'desc' => __('Example Value: John Doe', 'prcweb-real-estate'),
                    'type' => 'text',
                    'std' => "",
                    'columns' => 12,

                ),
                array(
                    'name'  => __( 'Title', 'prcweb-real-estate' ),
                    'id'    => "{$prefix}client_title",
                    'desc' => __('Example Value:Senior Developer', 'prcweb-real-estate'),
                    'type'  => 'text',
                    'columns'=>6,
                ),
                array(
                    'name'  => __( 'Company', 'prcweb-real-estate' ),
                    'id'    => "{$prefix}client_company",
                    'desc' => __('Example Value:PRC Group', 'prcweb-real-estate'),
                    'type'  => 'text',
                    'columns'=>6,
                ),
                array(
                    'name'  => __( 'Email Address', 'prcweb-real-estate' ),
                    'id'    => "{$prefix}testimonial_email",
                    'desc'  => __( "Email Address:test@mail.com.", "prcweb-real-estate" ),
                    'type'  => 'email',
                    'columns' => 6,
                ),

                array(
                    'id' => "{$prefix}related_to",
                    'name' => __('In Reference To', 'prcweb-real-estate'),
                    'desc' => __('', 'prcweb-real-estate'),
                    'type' => 'radio',
                    'std' =>'',
                    'options' => array(
                        1 => __('PRC Land ', 'prcweb-real-estate'),
                        2 => __('PRC Housing', 'prcweb-real-estate'),
                        3 => __('General', 'prcweb-real-estate')
                    ),
                    'columns' => 6,
                ),

            )
        );

        // apply a filter before returning meta boxes
        $meta_boxes = apply_filters( 'Testimonial_meta_boxes', $meta_boxes );

        return $meta_boxes;

    }

}