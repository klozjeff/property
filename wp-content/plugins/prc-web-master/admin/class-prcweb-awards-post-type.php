<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 5/24/2017
 * Time: 8:56 AM
 */
class Prcweb_Awards_Post_Type
{
public function register_awards_post_type(){
    $labels = array(
        'name'                => _x( 'Awards', 'Post Type General Name', 'prcweb-real-estate' ),
        'singular_name'       => _x( 'Award', 'Post Type Singular Name', 'prcweb-real-estate' ),
        'menu_name'           => __( 'Awards', 'prcweb-real-estate' ),
        'name_admin_bar'      => __( 'Awards', 'prcweb-real-estate' ),
        'parent_item_colon'   => __( 'Parent Award:', 'prcweb-real-estate' ),
        'all_items'           => __( 'All Awards', 'prcweb-real-estate' ),
        'add_new_item'        => __( 'Add New Award', 'prcweb-real-estate' ),
        'add_new'             => __( 'Add New', 'prcweb-real-estate' ),
        'new_item'            => __( 'New Award', 'prcweb-real-estate' ),
        'edit_item'           => __( 'Edit Awards', 'prcweb-real-estate' ),
        'update_item'         => __( 'Update Award', 'prcweb-real-estate' ),
        'view_item'           => __( 'View Award', 'prcweb-real-estate' ),
        'search_items'        => __( 'Search Award', 'prcweb-real-estate' ),
        'not_found'           => __( 'Not found', 'prcweb-real-estate' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'prcweb-real-estate' ),
    );

    $args = array(
        'label'               => __( 'Awards', 'prcweb-real-estate' ),
        'description'         => __( 'Awards', 'prcweb-real-estate' ),
        'labels'              => $labels,
        'supports'            => array( 'title'),
        'hierarchical'        => false,
        'public'              => false,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-awards',
        'show_in_admin_bar'   => false,
        'can_export'          => true,
        'has_archive'         => false,
        'rewrite'             => false,
        'capability_type'     => 'post',
    );

    register_post_type( 'Awards', $args );
}
    /**
     * Register custom columns
     *
     * @param   array   $defaults
     * @since   1.0.0
     * @return  array   $defaults
     */
    public function register_custom_column_titles ( $defaults ) {

        $new_columns = array(
            "thumb"     => __( 'Image', 'prcweb-real-estate' ),
        );

        $last_columns = array();

        if ( count( $defaults ) > 2 ) {
            $last_columns = array_splice( $defaults, 2, 1 );
        }

        $defaults = array_merge( $defaults, $new_columns );
        $defaults = array_merge( $defaults, $last_columns );

        return $defaults;
    }

    /**
     * Display custom column for Awards
     *
     * @access  public
     * @param   string $column_name
     * @since   1.0.0
     * @return  void
     */
    public function display_custom_column ( $column_name ) {
        global $post;

        switch ( $column_name ) {

            case 'thumb':
                if ( has_post_thumbnail ( $post->ID ) ) {
                    the_post_thumbnail( array( 150, 150 ) );
                } else {
                    _e ( 'No Image', 'prcweb-real-estate' );
                }
                break;

            default:
                break;
        }
    }
    /**
     * Register meta boxes related to partner post type
     *
     * @param   array   $meta_boxes
     * @since   1.0.0
     * @return  array   $meta_boxes
     */
    public function register_meta_boxes ( $meta_boxes ){

        $prefix = 'PRC_WEB';

        // Partners Meta Box
        $meta_boxes[] = array(
            'id' => 'awards-meta-box',
            'title' => __('Award Information', 'prcweb-real-estate'),
            'pages' => array( 'awards' ),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('Award Category', 'prcweb-real-estate'),
                    'id' => "{$prefix}award_category",
                    'desc' => __('Ex:Advisors and Consultants for Research', 'prcweb-real-estate'),
                    'type' => 'text',
                ),
                array(
                    'name' => __('Award Position Attained', 'prcweb-real-estate'),
                    'id' => "{$prefix}award_position",
                    'desc' => __('Ex:1st Place,Winner,Runners Up', 'prcweb-real-estate'),
                    'type' => 'text',
                ),
                array(
                    'name' => __('Is published ?', 'prcweb-real-estate'),
                    'id' => "{$prefix}isPublished",
                    'type' => 'radio',
                    'options' => array(
                        1 => __('Yes ', 'prcweb-real-estate'),
                        0 => __('No', 'prcweb-real-estate')
                    )
                ),
                array(
                    'name' => __('Award Image', 'prcweb-real-estate'),
                    'id' => "{$prefix}award_image",
                    'desc' => __('Provide Award Base Image', 'prcweb-real-estate'),
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                ),
            )
        );

        // apply a filter before returning meta boxes
        $meta_boxes = apply_filters( 'award_meta_boxes', $meta_boxes );

        return $meta_boxes;
    }
}