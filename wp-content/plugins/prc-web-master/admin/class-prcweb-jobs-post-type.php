<?php

/**
 * Created by PhpStorm.
 * User: Web Designer
 * Date: 8/25/2017
 * Time: 9:14 AM
 */
class PrcWeb_Jobs_Post_Type
{
  public function register_jobs_post_type()
  {
      $labels=array(
          'name'                => _x( 'Jobs', 'Post Type General Name', 'prcweb-real-estate' ),
          'singular_name'       => _x( 'Job', 'Post Type Singular Name', 'prcweb-real-estate' ),
          'menu_name'           => __( 'Job Board', 'prcweb-real-estate' ),
          'name_admin_bar'      => __( 'Jobs', 'prcweb-real-estate' ),
          'parent_item_colon'   => __( 'Parent Job:', 'prcweb-real-estate' ),
          'all_items'           => __( 'All Jobs', 'prcweb-real-estate' ),
          'add_new_item'        => __( 'Add New Job', 'prcweb-real-estate' ),
          'add_new'             => __( 'Add New', 'prcweb-real-estate' ),
          'new_item'            => __( 'New Job', 'prcweb-real-estate' ),
          'edit_item'           => __( 'Edit Jobs', 'prcweb-real-estate' ),
          'update_item'         => __( 'Update Job', 'prcweb-real-estate' ),
          'view_item'           => __( 'View Job', 'prcweb-real-estate' ),
          'search_items'        => __( 'Search Job', 'prcweb-real-estate' ),
          'not_found'           => __( 'Not found', 'prcweb-real-estate' ),
          'not_found_in_trash'  => __( 'Not found in Trash', 'prcweb-real-estate' ),
      );

      $args = array(
          'label'               => __( 'Jobs', 'prcweb-real-estate' ),
          'description'         => __( 'Jobs', 'prcweb-real-estate' ),
          'labels'              => $labels,
          'supports'            => array( 'title','editor'),
          'hierarchical'        => false,
          'public'              => false,
          'show_ui'             => true,
          'show_in_menu'        => true,
          'menu_position'       => 6,
          'menu_icon'           => 'dashicons-welcome-learn-more',
          'show_in_admin_bar'   => false,
          'can_export'          => true,
          'has_archive'         => false,
          'rewrite'            => array( 'slug' => 'jobs','hierarchical' => TRUE, 'with_front' => FALSE),
          'capability_type'     => 'post',
      );

      register_post_type( 'Jobs', $args );
  }

  public function register_job_category_taxonomy(){

      $labels = array(
          'name'                       => _x( 'Job Categories', 'Taxonomy General Name', 'prcweb-real-estate' ),
          'singular_name'              => _x( 'Job Category', 'Taxonomy Singular Name', 'prcweb-real-estate' ),
          'menu_name'                  => __( 'Job Categories', 'prcweb-real-estate' ),
          'all_items'                  => __( 'All Job Categories', 'prcweb-real-estate' ),
          'parent_item'                => __( 'Parent Job Category', 'prcweb-real-estate' ),
          'parent_item_colon'          => __( 'Parent Job Category:', 'prcweb-real-estate' ),
          'new_item_name'              => __( 'New Job Category Name', 'prcweb-real-estate' ),
          'add_new_item'               => __( 'Add New Job Category', 'prcweb-real-estate' ),
          'edit_item'                  => __( 'Edit Job Category', 'prcweb-real-estate' ),
          'update_item'                => __( 'Update Job Category', 'prcweb-real-estate' ),
          'view_item'                  => __( 'View Job Category', 'prcweb-real-estate' ),
          'separate_items_with_commas' => __( 'Separate Job Categories with commas', 'prcweb-real-estate' ),
          'add_or_remove_items'        => __( 'Add or remove Job Categories', 'prcweb-real-estate' ),
          'choose_from_most_used'      => __( 'Choose from the most used', 'prcweb-real-estate' ),
          'popular_items'              => __( 'Popular Job Categories', 'prcweb-real-estate' ),
          'search_items'               => __( 'Search Job Categories', 'prcweb-real-estate' ),
          'not_found'                  => __( 'Not Found', 'prcweb-real-estate' ),
      );

      $rewrite = array(
          'slug'                       => apply_filters( 'prcweb_job_category_slug', __( 'job-category', 'prcweb-real-estate' ) ),
          'with_front'                 => true,
          'hierarchical'               => true,
      );

      $args = array(
          'labels'                     => $labels,
          'hierarchical'               => true,
          'public'                     => true,
          'show_ui'                    => true,
          'show_admin_column'          => true,
          'show_in_nav_menus'          => true,
          'show_tagcloud'              => true,
          'rewrite'                    => $rewrite,
      );

      register_taxonomy( 'job-category', array( 'jobs' ), $args );

  }

    public function register_job_type_taxonomy(){

        $labels = array(
            'name'                       => _x( 'Job Types', 'Taxonomy General Name', 'prcweb-real-estate' ),
            'singular_name'              => _x( 'Job Type', 'Taxonomy Singular Name', 'prcweb-real-estate' ),
            'menu_name'                  => __( 'Job Types', 'prcweb-real-estate' ),
            'all_items'                  => __( 'All Job Types', 'prcweb-real-estate' ),
            'parent_item'                => __( 'Parent Job Type', 'prcweb-real-estate' ),
            'parent_item_colon'          => __( 'Parent Job Type:', 'prcweb-real-estate' ),
            'new_item_name'              => __( 'New Job Type Name', 'prcweb-real-estate' ),
            'add_new_item'               => __( 'Add New Job Type', 'prcweb-real-estate' ),
            'edit_item'                  => __( 'Edit Job Type', 'prcweb-real-estate' ),
            'update_item'                => __( 'Update Job Type', 'prcweb-real-estate' ),
            'view_item'                  => __( 'View Job Type', 'prcweb-real-estate' ),
            'separate_items_with_commas' => __( 'Separate Job Types with commas', 'prcweb-real-estate' ),
            'add_or_remove_items'        => __( 'Add or remove Job Types', 'prcweb-real-estate' ),
            'choose_from_most_used'      => __( 'Choose from the most used', 'prcweb-real-estate' ),
            'popular_items'              => __( 'Popular Job Types', 'prcweb-real-estate' ),
            'search_items'               => __( 'Search Job Types', 'prcweb-real-estate' ),
            'not_found'                  => __( 'Not Found', 'prcweb-real-estate' ),
        );

        $rewrite = array(
            'slug'                       => apply_filters( 'prcweb_job_type_slug', __( 'job-type', 'prcweb-real-estate' ) ),
            'with_front'                 => true,
            'hierarchical'               => true,
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );

        register_taxonomy( 'job-type', array( 'jobs' ), $args );

    }

    public function register_meta_boxes ( $meta_boxes ){

        $prefix = 'PRC_WEB';

        // Partners Meta Box
        $meta_boxes[] = array(
            'id' => 'jobs-meta-box',
            'title' => __('Job Details', 'prcweb-real-estate'),
            'pages' => array( 'jobs' ),
            'context' => 'normal',
            'priority' => 'high',
            'fields' => array(
                array(
                    'name' => __('Salary Range', 'prcweb-real-estate'),
                    'id' => "{$prefix}salary_range",
                    'desc' => __('Ex:25k-45k,Min 25k or 25k ', 'prcweb-real-estate'),
                    'type' => 'text',
                ),
                array(
                    'name' => __('Application Link', 'prcweb-real-estate'),
                    'id' => "{$prefix}application_link",
                    'desc' => __('Ex:Link for applicants to submit their applications.Brightermonday,Fuzu or any other platform', 'prcweb-real-estate'),
                    'type' => 'text',
                ),
                array(
                    'name' => __('Is Filled?-if the job is already taken', 'prcweb-real-estate'),
                    'id' => "{$prefix}isFilled",
                    'type' => 'radio',
                    'options' => array(
                        1 => __('Yes ', 'prcweb-real-estate'),
                        0 => __('No', 'prcweb-real-estate')
                    )
                ),

            )
        );

        // apply a filter before returning meta boxes
        $meta_boxes = apply_filters( 'job_meta_boxes', $meta_boxes );

        return $meta_boxes;
    }
}