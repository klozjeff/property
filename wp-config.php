<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'property');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('ALLOW_UNFILTERED_UPLOADS', true);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&DQEPv+Mf|T*T?Fv|k;r3l?TPGg;mGlq~evvREb5Oc$1f[OCD[WZ2gJ>WoE3:8So');
define('SECURE_AUTH_KEY',  '7QN4s.7ri-B>|yBUBJ,GfB5*Y&`PkzcxG;5SLPQ@ZCq?[a-rxOQR11<S)Vds Hrp');
define('LOGGED_IN_KEY',    '^+I9ofg8%j@[CsVm1OZx4f,!VqnE%9*G~?^kq ~0T|ORC_BJ`:Iyl3^I;;!HeZHY');
define('NONCE_KEY',        '1brbDhTMOS&JZ+&g:q!Wl+7j--t{-`qdBX~SX6(_tqw>C%^t,,5-PR1??{|lb!Df');
define('AUTH_SALT',        '80^_3_ON)GH2E#exv0S{R/;+uXkkv5M}:=C ~9xw/A }z~fnh;_L->/?;R|6&PPR');
define('SECURE_AUTH_SALT', '16h7h#KT;E&8I4yWC(u_*2TT/ TEX<$Bl3KH;1#c&CY$t4h)$badd2M&,<mwQn~6');
define('LOGGED_IN_SALT',   '+QaSdf2Gg]ZS-uKa<$#[(r^O~xHc cp.Zbm[MW*JUY#QJ84!zWV/eW3^)&ZYCjz*');
define('NONCE_SALT',       'R(N!zQde:aeIIy:u3x8(,K{7}[DCYQ?NF/4C#L=$/E%U3t0snxGD<fA>+y$(/GeN');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'prc_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '1024M' );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
